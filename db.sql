# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.32)
# Database: work_saga_penjurian
# Generation Time: 2020-12-29 12:01:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table anggota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `anggota`;

CREATE TABLE `anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `jabatan` enum('ketua','anggota','pembimbing') NOT NULL DEFAULT 'anggota',
  `peran` text COMMENT 'peranan dalam tim',
  `agama` enum('islam','kristen','katolik','hindu','budha','konghucu') NOT NULL DEFAULT 'islam',
  `nik` varchar(20) NOT NULL,
  `alamat` text NOT NULL COMMENT 'alamat sesuai KTP',
  `provinsi` int(11) NOT NULL,
  `foto` varchar(20) NOT NULL,
  `kabupaten` int(11) NOT NULL,
  `place_of_birth` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '1=Laki-laki, 2=Perempuan',
  `country` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `anggota_user_id_foreign` (`user_id`),
  CONSTRAINT `anggota_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `anggota` WRITE;
/*!40000 ALTER TABLE `anggota` DISABLE KEYS */;

INSERT INTO `anggota` (`id`, `user_id`, `name`, `jabatan`, `peran`, `agama`, `nik`, `alamat`, `provinsi`, `foto`, `kabupaten`, `place_of_birth`, `date_of_birth`, `gender`, `country`, `phone`, `email`, `created_at`, `updated_at`)
VALUES
	(1,4,'Tio Tamara','ketua','Ketua','islam','12345','Somodaran Banyuraden Gamping Sleman YK',34,'foto-1.jpeg',3404,'Sleman','0000-00-00',1,'ID','087838883764','tiotamara@gmail.com','2020-12-29 18:48:59','2020-12-29 18:59:23');

/*!40000 ALTER TABLE `anggota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text,
  `foto` text,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `submisi` varchar(39) DEFAULT NULL,
  `bimbingan` varchar(39) DEFAULT NULL,
  `penjurian_makalah` varchar(39) DEFAULT NULL,
  `penjurian_grand_final` varchar(39) DEFAULT NULL,
  `tutorial` text,
  `embed_survey_peserta` text,
  `embed_survey_juri` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `judul`, `deskripsi`, `foto`, `tanggal_mulai`, `tanggal_selesai`, `submisi`, `bimbingan`, `penjurian_makalah`, `penjurian_grand_final`, `tutorial`, `embed_survey_peserta`, `embed_survey_juri`, `created_at`, `updated_at`)
VALUES
	(1,'AIP 2021',NULL,NULL,'2021-05-01','2021-06-12','2020-05-01 00:00:00_2020-05-01 00:00:00','2020-05-02 00:00:00_2020-05-02 00:00:00','2020-06-01 00:00:00_2020-06-04 00:00:00','2020-06-05 00:00:00_2020-06-06 00:00:00','<p>tes</p>','Embed Survey Peserta','Embed Survey Juri','2020-05-13 00:35:46','2020-06-01 12:01:44'),
	(5,'Anugerah Inovasi & Penelitian 2020',NULL,NULL,'2020-07-01','2020-12-31','2020-08-02 19:07:04_2020-08-03 19:07:04','2020-08-02 19:07:04_2020-08-31 19:07:04','2020-08-03 19:07:04_2020-08-31 19:07:04','2020-09-01 19:07:04_2020-09-01 19:07:04',NULL,NULL,NULL,'2020-06-01 11:49:38','2020-08-02 19:08:39');

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table juri
# ------------------------------------------------------------

DROP TABLE IF EXISTS `juri`;

CREATE TABLE `juri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `organisasi` varchar(20) NOT NULL,
  `no_rek` int(11) NOT NULL,
  `bank` varchar(10) NOT NULL,
  `place_of_birth` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '1=Laki-laki, 2=Perempuan',
  `country` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL,
  `npwp` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `juri_user_id_foreign` (`user_id`),
  CONSTRAINT `juri_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `juri` WRITE;
/*!40000 ALTER TABLE `juri` DISABLE KEYS */;

INSERT INTO `juri` (`id`, `user_id`, `name`, `jabatan`, `organisasi`, `no_rek`, `bank`, `place_of_birth`, `date_of_birth`, `gender`, `country`, `phone`, `email`, `npwp`, `foto`, `created_at`, `updated_at`)
VALUES
	(1,3,'Juri Tio Tamara','Jabatan','Organisasi',12345,'Bca','Yogyakarta','2020-03-21',2,'Indonesia','087838883764','tiotamara@gmail.com','12345','foto-juri-3.jpg','2020-05-05 22:40:19','2020-05-29 21:28:16');

/*!40000 ALTER TABLE `juri` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table juri_kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `juri_kategori`;

CREATE TABLE `juri_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jurikategori_user_id_foreign` (`user_id`),
  KEY `jurikategori_kategori_id_foreign` (`kategori_id`),
  CONSTRAINT `jurikategori_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`),
  CONSTRAINT `jurikategori_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `juri_kategori` WRITE;
/*!40000 ALTER TABLE `juri_kategori` DISABLE KEYS */;

INSERT INTO `juri_kategori` (`id`, `user_id`, `kategori_id`, `created_at`, `updated_at`)
VALUES
	(1,3,1,'2020-05-26 19:55:38',NULL),
	(2,3,2,'2020-05-26 19:55:38',NULL);

/*!40000 ALTER TABLE `juri_kategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kabupaten
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kabupaten`;

CREATE TABLE `kabupaten` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regencies_province_id_index` (`province_id`),
  CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinsi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kabupaten` WRITE;
/*!40000 ALTER TABLE `kabupaten` DISABLE KEYS */;

INSERT INTO `kabupaten` (`id`, `province_id`, `name`)
VALUES
	('1101','11','KABUPATEN SIMEULUE'),
	('1102','11','KABUPATEN ACEH SINGKIL'),
	('1103','11','KABUPATEN ACEH SELATAN'),
	('1104','11','KABUPATEN ACEH TENGGARA'),
	('1105','11','KABUPATEN ACEH TIMUR'),
	('1106','11','KABUPATEN ACEH TENGAH'),
	('1107','11','KABUPATEN ACEH BARAT'),
	('1108','11','KABUPATEN ACEH BESAR'),
	('1109','11','KABUPATEN PIDIE'),
	('1110','11','KABUPATEN BIREUEN'),
	('1111','11','KABUPATEN ACEH UTARA'),
	('1112','11','KABUPATEN ACEH BARAT DAYA'),
	('1113','11','KABUPATEN GAYO LUES'),
	('1114','11','KABUPATEN ACEH TAMIANG'),
	('1115','11','KABUPATEN NAGAN RAYA'),
	('1116','11','KABUPATEN ACEH JAYA'),
	('1117','11','KABUPATEN BENER MERIAH'),
	('1118','11','KABUPATEN PIDIE JAYA'),
	('1171','11','KOTA BANDA ACEH'),
	('1172','11','KOTA SABANG'),
	('1173','11','KOTA LANGSA'),
	('1174','11','KOTA LHOKSEUMAWE'),
	('1175','11','KOTA SUBULUSSALAM'),
	('1201','12','KABUPATEN NIAS'),
	('1202','12','KABUPATEN MANDAILING NATAL'),
	('1203','12','KABUPATEN TAPANULI SELATAN'),
	('1204','12','KABUPATEN TAPANULI TENGAH'),
	('1205','12','KABUPATEN TAPANULI UTARA'),
	('1206','12','KABUPATEN TOBA SAMOSIR'),
	('1207','12','KABUPATEN LABUHAN BATU'),
	('1208','12','KABUPATEN ASAHAN'),
	('1209','12','KABUPATEN SIMALUNGUN'),
	('1210','12','KABUPATEN DAIRI'),
	('1211','12','KABUPATEN KARO'),
	('1212','12','KABUPATEN DELI SERDANG'),
	('1213','12','KABUPATEN LANGKAT'),
	('1214','12','KABUPATEN NIAS SELATAN'),
	('1215','12','KABUPATEN HUMBANG HASUNDUTAN'),
	('1216','12','KABUPATEN PAKPAK BHARAT'),
	('1217','12','KABUPATEN SAMOSIR'),
	('1218','12','KABUPATEN SERDANG BEDAGAI'),
	('1219','12','KABUPATEN BATU BARA'),
	('1220','12','KABUPATEN PADANG LAWAS UTARA'),
	('1221','12','KABUPATEN PADANG LAWAS'),
	('1222','12','KABUPATEN LABUHAN BATU SELATAN'),
	('1223','12','KABUPATEN LABUHAN BATU UTARA'),
	('1224','12','KABUPATEN NIAS UTARA'),
	('1225','12','KABUPATEN NIAS BARAT'),
	('1271','12','KOTA SIBOLGA'),
	('1272','12','KOTA TANJUNG BALAI'),
	('1273','12','KOTA PEMATANG SIANTAR'),
	('1274','12','KOTA TEBING TINGGI'),
	('1275','12','KOTA MEDAN'),
	('1276','12','KOTA BINJAI'),
	('1277','12','KOTA PADANGSIDIMPUAN'),
	('1278','12','KOTA GUNUNGSITOLI'),
	('1301','13','KABUPATEN KEPULAUAN MENTAWAI'),
	('1302','13','KABUPATEN PESISIR SELATAN'),
	('1303','13','KABUPATEN SOLOK'),
	('1304','13','KABUPATEN SIJUNJUNG'),
	('1305','13','KABUPATEN TANAH DATAR'),
	('1306','13','KABUPATEN PADANG PARIAMAN'),
	('1307','13','KABUPATEN AGAM'),
	('1308','13','KABUPATEN LIMA PULUH KOTA'),
	('1309','13','KABUPATEN PASAMAN'),
	('1310','13','KABUPATEN SOLOK SELATAN'),
	('1311','13','KABUPATEN DHARMASRAYA'),
	('1312','13','KABUPATEN PASAMAN BARAT'),
	('1371','13','KOTA PADANG'),
	('1372','13','KOTA SOLOK'),
	('1373','13','KOTA SAWAH LUNTO'),
	('1374','13','KOTA PADANG PANJANG'),
	('1375','13','KOTA BUKITTINGGI'),
	('1376','13','KOTA PAYAKUMBUH'),
	('1377','13','KOTA PARIAMAN'),
	('1401','14','KABUPATEN KUANTAN SINGINGI'),
	('1402','14','KABUPATEN INDRAGIRI HULU'),
	('1403','14','KABUPATEN INDRAGIRI HILIR'),
	('1404','14','KABUPATEN PELALAWAN'),
	('1405','14','KABUPATEN S I A K'),
	('1406','14','KABUPATEN KAMPAR'),
	('1407','14','KABUPATEN ROKAN HULU'),
	('1408','14','KABUPATEN BENGKALIS'),
	('1409','14','KABUPATEN ROKAN HILIR'),
	('1410','14','KABUPATEN KEPULAUAN MERANTI'),
	('1471','14','KOTA PEKANBARU'),
	('1473','14','KOTA D U M A I'),
	('1501','15','KABUPATEN KERINCI'),
	('1502','15','KABUPATEN MERANGIN'),
	('1503','15','KABUPATEN SAROLANGUN'),
	('1504','15','KABUPATEN BATANG HARI'),
	('1505','15','KABUPATEN MUARO JAMBI'),
	('1506','15','KABUPATEN TANJUNG JABUNG TIMUR'),
	('1507','15','KABUPATEN TANJUNG JABUNG BARAT'),
	('1508','15','KABUPATEN TEBO'),
	('1509','15','KABUPATEN BUNGO'),
	('1571','15','KOTA JAMBI'),
	('1572','15','KOTA SUNGAI PENUH'),
	('1601','16','KABUPATEN OGAN KOMERING ULU'),
	('1602','16','KABUPATEN OGAN KOMERING ILIR'),
	('1603','16','KABUPATEN MUARA ENIM'),
	('1604','16','KABUPATEN LAHAT'),
	('1605','16','KABUPATEN MUSI RAWAS'),
	('1606','16','KABUPATEN MUSI BANYUASIN'),
	('1607','16','KABUPATEN BANYU ASIN'),
	('1608','16','KABUPATEN OGAN KOMERING ULU SELATAN'),
	('1609','16','KABUPATEN OGAN KOMERING ULU TIMUR'),
	('1610','16','KABUPATEN OGAN ILIR'),
	('1611','16','KABUPATEN EMPAT LAWANG'),
	('1612','16','KABUPATEN PENUKAL ABAB LEMATANG ILIR'),
	('1613','16','KABUPATEN MUSI RAWAS UTARA'),
	('1671','16','KOTA PALEMBANG'),
	('1672','16','KOTA PRABUMULIH'),
	('1673','16','KOTA PAGAR ALAM'),
	('1674','16','KOTA LUBUKLINGGAU'),
	('1701','17','KABUPATEN BENGKULU SELATAN'),
	('1702','17','KABUPATEN REJANG LEBONG'),
	('1703','17','KABUPATEN BENGKULU UTARA'),
	('1704','17','KABUPATEN KAUR'),
	('1705','17','KABUPATEN SELUMA'),
	('1706','17','KABUPATEN MUKOMUKO'),
	('1707','17','KABUPATEN LEBONG'),
	('1708','17','KABUPATEN KEPAHIANG'),
	('1709','17','KABUPATEN BENGKULU TENGAH'),
	('1771','17','KOTA BENGKULU'),
	('1801','18','KABUPATEN LAMPUNG BARAT'),
	('1802','18','KABUPATEN TANGGAMUS'),
	('1803','18','KABUPATEN LAMPUNG SELATAN'),
	('1804','18','KABUPATEN LAMPUNG TIMUR'),
	('1805','18','KABUPATEN LAMPUNG TENGAH'),
	('1806','18','KABUPATEN LAMPUNG UTARA'),
	('1807','18','KABUPATEN WAY KANAN'),
	('1808','18','KABUPATEN TULANGBAWANG'),
	('1809','18','KABUPATEN PESAWARAN'),
	('1810','18','KABUPATEN PRINGSEWU'),
	('1811','18','KABUPATEN MESUJI'),
	('1812','18','KABUPATEN TULANG BAWANG BARAT'),
	('1813','18','KABUPATEN PESISIR BARAT'),
	('1871','18','KOTA BANDAR LAMPUNG'),
	('1872','18','KOTA METRO'),
	('1901','19','KABUPATEN BANGKA'),
	('1902','19','KABUPATEN BELITUNG'),
	('1903','19','KABUPATEN BANGKA BARAT'),
	('1904','19','KABUPATEN BANGKA TENGAH'),
	('1905','19','KABUPATEN BANGKA SELATAN'),
	('1906','19','KABUPATEN BELITUNG TIMUR'),
	('1971','19','KOTA PANGKAL PINANG'),
	('2101','21','KABUPATEN KARIMUN'),
	('2102','21','KABUPATEN BINTAN'),
	('2103','21','KABUPATEN NATUNA'),
	('2104','21','KABUPATEN LINGGA'),
	('2105','21','KABUPATEN KEPULAUAN ANAMBAS'),
	('2171','21','KOTA B A T A M'),
	('2172','21','KOTA TANJUNG PINANG'),
	('3101','31','KABUPATEN KEPULAUAN SERIBU'),
	('3171','31','KOTA JAKARTA SELATAN'),
	('3172','31','KOTA JAKARTA TIMUR'),
	('3173','31','KOTA JAKARTA PUSAT'),
	('3174','31','KOTA JAKARTA BARAT'),
	('3175','31','KOTA JAKARTA UTARA'),
	('3201','32','KABUPATEN BOGOR'),
	('3202','32','KABUPATEN SUKABUMI'),
	('3203','32','KABUPATEN CIANJUR'),
	('3204','32','KABUPATEN BANDUNG'),
	('3205','32','KABUPATEN GARUT'),
	('3206','32','KABUPATEN TASIKMALAYA'),
	('3207','32','KABUPATEN CIAMIS'),
	('3208','32','KABUPATEN KUNINGAN'),
	('3209','32','KABUPATEN CIREBON'),
	('3210','32','KABUPATEN MAJALENGKA'),
	('3211','32','KABUPATEN SUMEDANG'),
	('3212','32','KABUPATEN INDRAMAYU'),
	('3213','32','KABUPATEN SUBANG'),
	('3214','32','KABUPATEN PURWAKARTA'),
	('3215','32','KABUPATEN KARAWANG'),
	('3216','32','KABUPATEN BEKASI'),
	('3217','32','KABUPATEN BANDUNG BARAT'),
	('3218','32','KABUPATEN PANGANDARAN'),
	('3271','32','KOTA BOGOR'),
	('3272','32','KOTA SUKABUMI'),
	('3273','32','KOTA BANDUNG'),
	('3274','32','KOTA CIREBON'),
	('3275','32','KOTA BEKASI'),
	('3276','32','KOTA DEPOK'),
	('3277','32','KOTA CIMAHI'),
	('3278','32','KOTA TASIKMALAYA'),
	('3279','32','KOTA BANJAR'),
	('3301','33','KABUPATEN CILACAP'),
	('3302','33','KABUPATEN BANYUMAS'),
	('3303','33','KABUPATEN PURBALINGGA'),
	('3304','33','KABUPATEN BANJARNEGARA'),
	('3305','33','KABUPATEN KEBUMEN'),
	('3306','33','KABUPATEN PURWOREJO'),
	('3307','33','KABUPATEN WONOSOBO'),
	('3308','33','KABUPATEN MAGELANG'),
	('3309','33','KABUPATEN BOYOLALI'),
	('3310','33','KABUPATEN KLATEN'),
	('3311','33','KABUPATEN SUKOHARJO'),
	('3312','33','KABUPATEN WONOGIRI'),
	('3313','33','KABUPATEN KARANGANYAR'),
	('3314','33','KABUPATEN SRAGEN'),
	('3315','33','KABUPATEN GROBOGAN'),
	('3316','33','KABUPATEN BLORA'),
	('3317','33','KABUPATEN REMBANG'),
	('3318','33','KABUPATEN PATI'),
	('3319','33','KABUPATEN KUDUS'),
	('3320','33','KABUPATEN JEPARA'),
	('3321','33','KABUPATEN DEMAK'),
	('3322','33','KABUPATEN SEMARANG'),
	('3323','33','KABUPATEN TEMANGGUNG'),
	('3324','33','KABUPATEN KENDAL'),
	('3325','33','KABUPATEN BATANG'),
	('3326','33','KABUPATEN PEKALONGAN'),
	('3327','33','KABUPATEN PEMALANG'),
	('3328','33','KABUPATEN TEGAL'),
	('3329','33','KABUPATEN BREBES'),
	('3371','33','KOTA MAGELANG'),
	('3372','33','KOTA SURAKARTA'),
	('3373','33','KOTA SALATIGA'),
	('3374','33','KOTA SEMARANG'),
	('3375','33','KOTA PEKALONGAN'),
	('3376','33','KOTA TEGAL'),
	('3401','34','KABUPATEN KULON PROGO'),
	('3402','34','KABUPATEN BANTUL'),
	('3403','34','KABUPATEN GUNUNG KIDUL'),
	('3404','34','KABUPATEN SLEMAN'),
	('3471','34','KOTA YOGYAKARTA'),
	('3501','35','KABUPATEN PACITAN'),
	('3502','35','KABUPATEN PONOROGO'),
	('3503','35','KABUPATEN TRENGGALEK'),
	('3504','35','KABUPATEN TULUNGAGUNG'),
	('3505','35','KABUPATEN BLITAR'),
	('3506','35','KABUPATEN KEDIRI'),
	('3507','35','KABUPATEN MALANG'),
	('3508','35','KABUPATEN LUMAJANG'),
	('3509','35','KABUPATEN JEMBER'),
	('3510','35','KABUPATEN BANYUWANGI'),
	('3511','35','KABUPATEN BONDOWOSO'),
	('3512','35','KABUPATEN SITUBONDO'),
	('3513','35','KABUPATEN PROBOLINGGO'),
	('3514','35','KABUPATEN PASURUAN'),
	('3515','35','KABUPATEN SIDOARJO'),
	('3516','35','KABUPATEN MOJOKERTO'),
	('3517','35','KABUPATEN JOMBANG'),
	('3518','35','KABUPATEN NGANJUK'),
	('3519','35','KABUPATEN MADIUN'),
	('3520','35','KABUPATEN MAGETAN'),
	('3521','35','KABUPATEN NGAWI'),
	('3522','35','KABUPATEN BOJONEGORO'),
	('3523','35','KABUPATEN TUBAN'),
	('3524','35','KABUPATEN LAMONGAN'),
	('3525','35','KABUPATEN GRESIK'),
	('3526','35','KABUPATEN BANGKALAN'),
	('3527','35','KABUPATEN SAMPANG'),
	('3528','35','KABUPATEN PAMEKASAN'),
	('3529','35','KABUPATEN SUMENEP'),
	('3571','35','KOTA KEDIRI'),
	('3572','35','KOTA BLITAR'),
	('3573','35','KOTA MALANG'),
	('3574','35','KOTA PROBOLINGGO'),
	('3575','35','KOTA PASURUAN'),
	('3576','35','KOTA MOJOKERTO'),
	('3577','35','KOTA MADIUN'),
	('3578','35','KOTA SURABAYA'),
	('3579','35','KOTA BATU'),
	('3601','36','KABUPATEN PANDEGLANG'),
	('3602','36','KABUPATEN LEBAK'),
	('3603','36','KABUPATEN TANGERANG'),
	('3604','36','KABUPATEN SERANG'),
	('3671','36','KOTA TANGERANG'),
	('3672','36','KOTA CILEGON'),
	('3673','36','KOTA SERANG'),
	('3674','36','KOTA TANGERANG SELATAN'),
	('5101','51','KABUPATEN JEMBRANA'),
	('5102','51','KABUPATEN TABANAN'),
	('5103','51','KABUPATEN BADUNG'),
	('5104','51','KABUPATEN GIANYAR'),
	('5105','51','KABUPATEN KLUNGKUNG'),
	('5106','51','KABUPATEN BANGLI'),
	('5107','51','KABUPATEN KARANG ASEM'),
	('5108','51','KABUPATEN BULELENG'),
	('5171','51','KOTA DENPASAR'),
	('5201','52','KABUPATEN LOMBOK BARAT'),
	('5202','52','KABUPATEN LOMBOK TENGAH'),
	('5203','52','KABUPATEN LOMBOK TIMUR'),
	('5204','52','KABUPATEN SUMBAWA'),
	('5205','52','KABUPATEN DOMPU'),
	('5206','52','KABUPATEN BIMA'),
	('5207','52','KABUPATEN SUMBAWA BARAT'),
	('5208','52','KABUPATEN LOMBOK UTARA'),
	('5271','52','KOTA MATARAM'),
	('5272','52','KOTA BIMA'),
	('5301','53','KABUPATEN SUMBA BARAT'),
	('5302','53','KABUPATEN SUMBA TIMUR'),
	('5303','53','KABUPATEN KUPANG'),
	('5304','53','KABUPATEN TIMOR TENGAH SELATAN'),
	('5305','53','KABUPATEN TIMOR TENGAH UTARA'),
	('5306','53','KABUPATEN BELU'),
	('5307','53','KABUPATEN ALOR'),
	('5308','53','KABUPATEN LEMBATA'),
	('5309','53','KABUPATEN FLORES TIMUR'),
	('5310','53','KABUPATEN SIKKA'),
	('5311','53','KABUPATEN ENDE'),
	('5312','53','KABUPATEN NGADA'),
	('5313','53','KABUPATEN MANGGARAI'),
	('5314','53','KABUPATEN ROTE NDAO'),
	('5315','53','KABUPATEN MANGGARAI BARAT'),
	('5316','53','KABUPATEN SUMBA TENGAH'),
	('5317','53','KABUPATEN SUMBA BARAT DAYA'),
	('5318','53','KABUPATEN NAGEKEO'),
	('5319','53','KABUPATEN MANGGARAI TIMUR'),
	('5320','53','KABUPATEN SABU RAIJUA'),
	('5321','53','KABUPATEN MALAKA'),
	('5371','53','KOTA KUPANG'),
	('6101','61','KABUPATEN SAMBAS'),
	('6102','61','KABUPATEN BENGKAYANG'),
	('6103','61','KABUPATEN LANDAK'),
	('6104','61','KABUPATEN MEMPAWAH'),
	('6105','61','KABUPATEN SANGGAU'),
	('6106','61','KABUPATEN KETAPANG'),
	('6107','61','KABUPATEN SINTANG'),
	('6108','61','KABUPATEN KAPUAS HULU'),
	('6109','61','KABUPATEN SEKADAU'),
	('6110','61','KABUPATEN MELAWI'),
	('6111','61','KABUPATEN KAYONG UTARA'),
	('6112','61','KABUPATEN KUBU RAYA'),
	('6171','61','KOTA PONTIANAK'),
	('6172','61','KOTA SINGKAWANG'),
	('6201','62','KABUPATEN KOTAWARINGIN BARAT'),
	('6202','62','KABUPATEN KOTAWARINGIN TIMUR'),
	('6203','62','KABUPATEN KAPUAS'),
	('6204','62','KABUPATEN BARITO SELATAN'),
	('6205','62','KABUPATEN BARITO UTARA'),
	('6206','62','KABUPATEN SUKAMARA'),
	('6207','62','KABUPATEN LAMANDAU'),
	('6208','62','KABUPATEN SERUYAN'),
	('6209','62','KABUPATEN KATINGAN'),
	('6210','62','KABUPATEN PULANG PISAU'),
	('6211','62','KABUPATEN GUNUNG MAS'),
	('6212','62','KABUPATEN BARITO TIMUR'),
	('6213','62','KABUPATEN MURUNG RAYA'),
	('6271','62','KOTA PALANGKA RAYA'),
	('6301','63','KABUPATEN TANAH LAUT'),
	('6302','63','KABUPATEN KOTA BARU'),
	('6303','63','KABUPATEN BANJAR'),
	('6304','63','KABUPATEN BARITO KUALA'),
	('6305','63','KABUPATEN TAPIN'),
	('6306','63','KABUPATEN HULU SUNGAI SELATAN'),
	('6307','63','KABUPATEN HULU SUNGAI TENGAH'),
	('6308','63','KABUPATEN HULU SUNGAI UTARA'),
	('6309','63','KABUPATEN TABALONG'),
	('6310','63','KABUPATEN TANAH BUMBU'),
	('6311','63','KABUPATEN BALANGAN'),
	('6371','63','KOTA BANJARMASIN'),
	('6372','63','KOTA BANJAR BARU'),
	('6401','64','KABUPATEN PASER'),
	('6402','64','KABUPATEN KUTAI BARAT'),
	('6403','64','KABUPATEN KUTAI KARTANEGARA'),
	('6404','64','KABUPATEN KUTAI TIMUR'),
	('6405','64','KABUPATEN BERAU'),
	('6409','64','KABUPATEN PENAJAM PASER UTARA'),
	('6411','64','KABUPATEN MAHAKAM HULU'),
	('6471','64','KOTA BALIKPAPAN'),
	('6472','64','KOTA SAMARINDA'),
	('6474','64','KOTA BONTANG'),
	('6501','65','KABUPATEN MALINAU'),
	('6502','65','KABUPATEN BULUNGAN'),
	('6503','65','KABUPATEN TANA TIDUNG'),
	('6504','65','KABUPATEN NUNUKAN'),
	('6571','65','KOTA TARAKAN'),
	('7101','71','KABUPATEN BOLAANG MONGONDOW'),
	('7102','71','KABUPATEN MINAHASA'),
	('7103','71','KABUPATEN KEPULAUAN SANGIHE'),
	('7104','71','KABUPATEN KEPULAUAN TALAUD'),
	('7105','71','KABUPATEN MINAHASA SELATAN'),
	('7106','71','KABUPATEN MINAHASA UTARA'),
	('7107','71','KABUPATEN BOLAANG MONGONDOW UTARA'),
	('7108','71','KABUPATEN SIAU TAGULANDANG BIARO'),
	('7109','71','KABUPATEN MINAHASA TENGGARA'),
	('7110','71','KABUPATEN BOLAANG MONGONDOW SELATAN'),
	('7111','71','KABUPATEN BOLAANG MONGONDOW TIMUR'),
	('7171','71','KOTA MANADO'),
	('7172','71','KOTA BITUNG'),
	('7173','71','KOTA TOMOHON'),
	('7174','71','KOTA KOTAMOBAGU'),
	('7201','72','KABUPATEN BANGGAI KEPULAUAN'),
	('7202','72','KABUPATEN BANGGAI'),
	('7203','72','KABUPATEN MOROWALI'),
	('7204','72','KABUPATEN POSO'),
	('7205','72','KABUPATEN DONGGALA'),
	('7206','72','KABUPATEN TOLI-TOLI'),
	('7207','72','KABUPATEN BUOL'),
	('7208','72','KABUPATEN PARIGI MOUTONG'),
	('7209','72','KABUPATEN TOJO UNA-UNA'),
	('7210','72','KABUPATEN SIGI'),
	('7211','72','KABUPATEN BANGGAI LAUT'),
	('7212','72','KABUPATEN MOROWALI UTARA'),
	('7271','72','KOTA PALU'),
	('7301','73','KABUPATEN KEPULAUAN SELAYAR'),
	('7302','73','KABUPATEN BULUKUMBA'),
	('7303','73','KABUPATEN BANTAENG'),
	('7304','73','KABUPATEN JENEPONTO'),
	('7305','73','KABUPATEN TAKALAR'),
	('7306','73','KABUPATEN GOWA'),
	('7307','73','KABUPATEN SINJAI'),
	('7308','73','KABUPATEN MAROS'),
	('7309','73','KABUPATEN PANGKAJENE DAN KEPULAUAN'),
	('7310','73','KABUPATEN BARRU'),
	('7311','73','KABUPATEN BONE'),
	('7312','73','KABUPATEN SOPPENG'),
	('7313','73','KABUPATEN WAJO'),
	('7314','73','KABUPATEN SIDENRENG RAPPANG'),
	('7315','73','KABUPATEN PINRANG'),
	('7316','73','KABUPATEN ENREKANG'),
	('7317','73','KABUPATEN LUWU'),
	('7318','73','KABUPATEN TANA TORAJA'),
	('7322','73','KABUPATEN LUWU UTARA'),
	('7325','73','KABUPATEN LUWU TIMUR'),
	('7326','73','KABUPATEN TORAJA UTARA'),
	('7371','73','KOTA MAKASSAR'),
	('7372','73','KOTA PAREPARE'),
	('7373','73','KOTA PALOPO'),
	('7401','74','KABUPATEN BUTON'),
	('7402','74','KABUPATEN MUNA'),
	('7403','74','KABUPATEN KONAWE'),
	('7404','74','KABUPATEN KOLAKA'),
	('7405','74','KABUPATEN KONAWE SELATAN'),
	('7406','74','KABUPATEN BOMBANA'),
	('7407','74','KABUPATEN WAKATOBI'),
	('7408','74','KABUPATEN KOLAKA UTARA'),
	('7409','74','KABUPATEN BUTON UTARA'),
	('7410','74','KABUPATEN KONAWE UTARA'),
	('7411','74','KABUPATEN KOLAKA TIMUR'),
	('7412','74','KABUPATEN KONAWE KEPULAUAN'),
	('7413','74','KABUPATEN MUNA BARAT'),
	('7414','74','KABUPATEN BUTON TENGAH'),
	('7415','74','KABUPATEN BUTON SELATAN'),
	('7471','74','KOTA KENDARI'),
	('7472','74','KOTA BAUBAU'),
	('7501','75','KABUPATEN BOALEMO'),
	('7502','75','KABUPATEN GORONTALO'),
	('7503','75','KABUPATEN POHUWATO'),
	('7504','75','KABUPATEN BONE BOLANGO'),
	('7505','75','KABUPATEN GORONTALO UTARA'),
	('7571','75','KOTA GORONTALO'),
	('7601','76','KABUPATEN MAJENE'),
	('7602','76','KABUPATEN POLEWALI MANDAR'),
	('7603','76','KABUPATEN MAMASA'),
	('7604','76','KABUPATEN MAMUJU'),
	('7605','76','KABUPATEN MAMUJU UTARA'),
	('7606','76','KABUPATEN MAMUJU TENGAH'),
	('8101','81','KABUPATEN MALUKU TENGGARA BARAT'),
	('8102','81','KABUPATEN MALUKU TENGGARA'),
	('8103','81','KABUPATEN MALUKU TENGAH'),
	('8104','81','KABUPATEN BURU'),
	('8105','81','KABUPATEN KEPULAUAN ARU'),
	('8106','81','KABUPATEN SERAM BAGIAN BARAT'),
	('8107','81','KABUPATEN SERAM BAGIAN TIMUR'),
	('8108','81','KABUPATEN MALUKU BARAT DAYA'),
	('8109','81','KABUPATEN BURU SELATAN'),
	('8171','81','KOTA AMBON'),
	('8172','81','KOTA TUAL'),
	('8201','82','KABUPATEN HALMAHERA BARAT'),
	('8202','82','KABUPATEN HALMAHERA TENGAH'),
	('8203','82','KABUPATEN KEPULAUAN SULA'),
	('8204','82','KABUPATEN HALMAHERA SELATAN'),
	('8205','82','KABUPATEN HALMAHERA UTARA'),
	('8206','82','KABUPATEN HALMAHERA TIMUR'),
	('8207','82','KABUPATEN PULAU MOROTAI'),
	('8208','82','KABUPATEN PULAU TALIABU'),
	('8271','82','KOTA TERNATE'),
	('8272','82','KOTA TIDORE KEPULAUAN'),
	('9101','91','KABUPATEN FAKFAK'),
	('9102','91','KABUPATEN KAIMANA'),
	('9103','91','KABUPATEN TELUK WONDAMA'),
	('9104','91','KABUPATEN TELUK BINTUNI'),
	('9105','91','KABUPATEN MANOKWARI'),
	('9106','91','KABUPATEN SORONG SELATAN'),
	('9107','91','KABUPATEN SORONG'),
	('9108','91','KABUPATEN RAJA AMPAT'),
	('9109','91','KABUPATEN TAMBRAUW'),
	('9110','91','KABUPATEN MAYBRAT'),
	('9111','91','KABUPATEN MANOKWARI SELATAN'),
	('9112','91','KABUPATEN PEGUNUNGAN ARFAK'),
	('9171','91','KOTA SORONG'),
	('9401','94','KABUPATEN MERAUKE'),
	('9402','94','KABUPATEN JAYAWIJAYA'),
	('9403','94','KABUPATEN JAYAPURA'),
	('9404','94','KABUPATEN NABIRE'),
	('9408','94','KABUPATEN KEPULAUAN YAPEN'),
	('9409','94','KABUPATEN BIAK NUMFOR'),
	('9410','94','KABUPATEN PANIAI'),
	('9411','94','KABUPATEN PUNCAK JAYA'),
	('9412','94','KABUPATEN MIMIKA'),
	('9413','94','KABUPATEN BOVEN DIGOEL'),
	('9414','94','KABUPATEN MAPPI'),
	('9415','94','KABUPATEN ASMAT'),
	('9416','94','KABUPATEN YAHUKIMO'),
	('9417','94','KABUPATEN PEGUNUNGAN BINTANG'),
	('9418','94','KABUPATEN TOLIKARA'),
	('9419','94','KABUPATEN SARMI'),
	('9420','94','KABUPATEN KEEROM'),
	('9426','94','KABUPATEN WAROPEN'),
	('9427','94','KABUPATEN SUPIORI'),
	('9428','94','KABUPATEN MAMBERAMO RAYA'),
	('9429','94','KABUPATEN NDUGA'),
	('9430','94','KABUPATEN LANNY JAYA'),
	('9431','94','KABUPATEN MAMBERAMO TENGAH'),
	('9432','94','KABUPATEN YALIMO'),
	('9433','94','KABUPATEN PUNCAK'),
	('9434','94','KABUPATEN DOGIYAI'),
	('9435','94','KABUPATEN INTAN JAYA'),
	('9436','94','KABUPATEN DEIYAI'),
	('9471','94','KOTA JAYAPURA');

/*!40000 ALTER TABLE `kabupaten` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_kategori_id_foreign` (`event_id`),
  CONSTRAINT `event_kategori_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;

INSERT INTO `kategori` (`id`, `event_id`, `nama`, `deskripsi`, `created_at`, `updated_at`)
VALUES
	(1,1,'Inovasi SMP',NULL,'2020-05-13 00:34:07',NULL),
	(2,1,'Inovasi SMA/SMK',NULL,'2020-05-13 00:34:07',NULL),
	(3,1,'Penelitian SMP','Penelitian SMP','2020-05-28 00:04:45','2020-08-02 18:48:00'),
	(4,5,'Inovasi SMP','Inovasi SMP','2020-08-02 23:14:54','2020-08-03 08:28:17'),
	(5,5,'Inovasi SMA/SMK','Inovasi SMA/SMK','2020-08-02 23:15:12',NULL),
	(6,5,'Inovasi Mahasiswa','Inovasi Mahasiswa','2020-08-02 23:15:32',NULL),
	(7,5,'Inovasi Umum','Inovasi Umum','2020-08-02 23:15:46',NULL),
	(8,5,'Penelitian SMP','Penelitian SMP','2020-08-02 23:16:02',NULL),
	(9,5,'Penelitian SMA/SMK','Penelitian SMA/SMK','2020-08-02 23:16:13','2020-08-02 23:55:21'),
	(10,5,'Penelitian Mahasiswa','Penelitian Mahasiswa','2020-08-02 23:16:23',NULL),
	(11,5,'Penelitian Umum','Penelitian Umum','2020-08-02 23:16:32',NULL);

/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kategori_sub
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori_sub`;

CREATE TABLE `kategori_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_kategori_id_foreign` (`kategori_id`),
  CONSTRAINT `sub_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `kategori_sub` WRITE;
/*!40000 ALTER TABLE `kategori_sub` DISABLE KEYS */;

INSERT INTO `kategori_sub` (`id`, `kategori_id`, `nama`, `deskripsi`, `created_at`, `updated_at`)
VALUES
	(1,1,'SMP NEGERI 1 YOGYAKARTA',NULL,'2020-05-13 00:56:30',NULL),
	(2,2,'SMK N 3 YOGYAKARTA','','2020-05-13 00:56:30','2020-05-28 21:55:21'),
	(3,9,'SMA 1 Yogyakarta','SMA 1 Yogyakarta','2020-08-02 23:21:47',NULL),
	(4,9,'SMA 2 Yogyakarta','SMA 2 Yogyakarta','2020-08-02 23:22:05',NULL),
	(5,9,'SMA 3 Yogyakarta','SMA 3 Yogyakarta','2020-08-02 23:22:28',NULL),
	(6,11,'SMA 4 Yogyakarta','SMA 4 Yogyakarta','2020-08-02 23:46:45',NULL),
	(7,9,'SMA 4 YK','SMA 4 YK','2020-08-02 23:55:59',NULL),
	(8,5,'SMA 4 YK','SMA 4 YK','2020-08-02 23:56:43',NULL),
	(9,11,'Umum','Penelitian Umum','2020-08-03 00:07:07',NULL),
	(10,7,'Umum','Inovasi Umum','2020-08-03 00:07:20',NULL),
	(11,10,'Universitas Gadjah Mada','Universitas Gadjah Mada','2020-08-03 00:07:44',NULL),
	(12,6,'Universitas Gadjah Mada','Universitas Gadjah Mada','2020-08-03 00:07:56',NULL),
	(13,8,'SMP 5 Yogyakarta','SMP 5 Yogyakarta','2020-08-03 00:11:03',NULL),
	(14,4,'SMP 5 Yogyakarta','SMP 5 Yogyakarta','2020-08-03 00:11:20',NULL),
	(15,4,'SMP 7 Yogyakarta','SMP 7 Yogyakarta','2020-08-03 08:28:08',NULL),
	(16,8,'SMP 7 Yogyakarta','SMP 7 Yogyakarta','2020-08-03 08:28:58',NULL);

/*!40000 ALTER TABLE `kategori_sub` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table nilai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nilai`;

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tim_id` int(11) NOT NULL COMMENT 'user id',
  `juri_id` int(11) NOT NULL COMMENT 'user id',
  `ide` int(11) NOT NULL,
  `metode` int(11) NOT NULL,
  `manfaat` int(11) NOT NULL,
  `display` int(11) NOT NULL,
  `presentasi` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nilai_juri_id_foreign` (`juri_id`),
  KEY `nilai_tim_id_foreign` (`tim_id`),
  CONSTRAINT `nilai_juri_id_foreign` FOREIGN KEY (`juri_id`) REFERENCES `user` (`id`),
  CONSTRAINT `nilai_tim_id_foreign` FOREIGN KEY (`tim_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_key` varchar(50) NOT NULL,
  `option_value` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;

INSERT INTO `options` (`id`, `option_key`, `option_value`, `created_at`, `updated_at`)
VALUES
	(1,'race_tutorial','Tutorial Lomba Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2020-03-19 00:19:25','0000-00-00 00:00:00'),
	(2,'limit','10','2020-03-20 00:19:25','0000-00-00 00:00:00'),
	(3,'value_input_tutorial','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(4,'website_title','AIP 2021','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(5,'website_role_admin','1','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(6,'website_role_panitia','2','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(7,'website_role_juri','3','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(8,'website_role_peserta','4','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(9,'website_table_option','options','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(10,'website_table_anggota','anggota','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(11,'website_table_juri','juri','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(12,'website_table_pengumuman','pengumuman','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(13,'website_table_role','role','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(14,'website_table_user','user','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(15,'website_table_user_meta','user_meta','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(16,'website_comment_table_not_found','Maaf, Terjadi Kesalahan, Harap Hubungi Admin','2020-03-21 00:19:25','0000-00-00 00:00:00'),
	(17,'website_table_provinsi','provinsi','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(18,'website_table_kabupaten','kabupaten','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(19,'website_table_sekolah','sekolah','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(20,'website_table_nilai','nilai','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(21,'website_table_kategori','kategori','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(22,'website_table_kategori_sub','kategori_sub','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(23,'website_table_tim','tim','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(24,'website_table_event','event','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(25,'website_table_juri_kategori','juri_kategori','2020-05-04 00:00:00','2020-05-04 00:00:00'),
	(26,'website_function_not_found','Ehh sorry ya guys','2020-05-27 00:00:00','2020-05-27 00:00:00');

/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pengumuman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pengumuman`;

CREATE TABLE `pengumuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=tidak aktif, 1=aktif',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pengumuman_event_id_foreign` (`event_id`),
  CONSTRAINT `pengumuman_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pengumuman` WRITE;
/*!40000 ALTER TABLE `pengumuman` DISABLE KEYS */;

INSERT INTO `pengumuman` (`id`, `event_id`, `title`, `content`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,'Pemberitahuan Covid 19','Covid 19 - Dikarenakan virus ini semakin berbahaya, dalam 2 minggu kedepan kerjanya dirumah saja',1,'2020-03-19 00:00:00',NULL);

/*!40000 ALTER TABLE `pengumuman` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table provinsi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `provinsi`;

CREATE TABLE `provinsi` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `provinsi` WRITE;
/*!40000 ALTER TABLE `provinsi` DISABLE KEYS */;

INSERT INTO `provinsi` (`id`, `name`)
VALUES
	('11','ACEH'),
	('12','SUMATERA UTARA'),
	('13','SUMATERA BARAT'),
	('14','RIAU'),
	('15','JAMBI'),
	('16','SUMATERA SELATAN'),
	('17','BENGKULU'),
	('18','LAMPUNG'),
	('19','KEPULAUAN BANGKA BELITUNG'),
	('21','KEPULAUAN RIAU'),
	('31','DKI JAKARTA'),
	('32','JAWA BARAT'),
	('33','JAWA TENGAH'),
	('34','DI YOGYAKARTA'),
	('35','JAWA TIMUR'),
	('36','BANTEN'),
	('51','BALI'),
	('52','NUSA TENGGARA BARAT'),
	('53','NUSA TENGGARA TIMUR'),
	('61','KALIMANTAN BARAT'),
	('62','KALIMANTAN TENGAH'),
	('63','KALIMANTAN SELATAN'),
	('64','KALIMANTAN TIMUR'),
	('65','KALIMANTAN UTARA'),
	('71','SULAWESI UTARA'),
	('72','SULAWESI TENGAH'),
	('73','SULAWESI SELATAN'),
	('74','SULAWESI TENGGARA'),
	('75','GORONTALO'),
	('76','SULAWESI BARAT'),
	('81','MALUKU'),
	('82','MALUKU UTARA'),
	('91','PAPUA BARAT'),
	('94','PAPUA');

/*!40000 ALTER TABLE `provinsi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;

INSERT INTO `role` (`id`, `slug`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Admin','2020-03-15 23:18:47',NULL),
	(2,'panitia','Panitia','2020-03-15 23:18:47',NULL),
	(3,'juri','Juri','2020-03-15 23:19:19',NULL),
	(4,'peserta','Peserta','2020-03-15 23:19:19',NULL);

/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sekolah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sekolah`;

CREATE TABLE `sekolah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sekolah` WRITE;
/*!40000 ALTER TABLE `sekolah` DISABLE KEYS */;

INSERT INTO `sekolah` (`id`, `name`)
VALUES
	(1,'SMP NEGERI 1 YOGYAKARTA'),
	(2,'SMP NEGERI 2 YOGYAKARTA'),
	(3,'SMP NEGERI 3 YOGYAKARTA'),
	(4,'SMP NEGERI 4 YOGYAKARTA'),
	(5,'SMP NEGERI 5 YOGYAKARTA'),
	(6,'SMP NEGERI 6 YOGYAKARTA'),
	(7,'SMP NEGERI 7 YOGYAKARTA'),
	(8,'SMP NEGERI 8 YOGYAKARTA'),
	(9,'SMP NEGERI 9 YOGYAKARTA'),
	(10,'SMP NEGERI 10 YOGYAKARTA'),
	(11,'SMP NEGERI 11 YOGYAKARTA'),
	(12,'SMP NEGERI 12 YOGYAKARTA'),
	(13,'SMP NEGERI 13 YOGYAKARTA'),
	(14,'SMP NEGERI 14 YOGYAKARTA'),
	(15,'SMP NEGERI 15 YOGYAKARTA'),
	(16,'SMP NEGERI 16 YOGYAKARTA'),
	(17,'SMP 17 1 YOGYAKARTA'),
	(18,'SMP 17 2 YOGYAKARTA'),
	(19,'SMP BHINNEKA TUNGGAL IKA YOGYAKARTA'),
	(20,'SMP BOPKRI 1 YOGYAKARTA'),
	(21,'SMP BOPKRI 10 YOGYAKARTA'),
	(22,'SMP BOPKRI 2 YOGYAKARTA'),
	(23,'SMP BOPKRI 3 YOGYAKARTA'),
	(24,'SMP BOPKRI 5 YOGYAKARTA'),
	(25,'SMP BUDI LUHUR'),
	(26,'SMP BUDYA WACANA'),
	(27,'SMP GOTONG ROYONG YOGYAKARTA'),
	(28,'SMP INSTITUT INDONESIA YOGYAKARTA'),
	(29,'SMP ISLAM TERPADU BINA ANAK SHOLEH'),
	(30,'SMP ISLAM YOGYAKARTA'),
	(31,'SMP IT ABU BAKAR YOGYAKARTA'),
	(32,'SMP IT AL KHAIRAAT'),
	(33,'SMP IT MASJID SYUHADA YOGYAKARTA'),
	(34,'SMP JOANNES BOSCO'),
	(35,'SMP KANISIUS GAYAM YOGYAKARTA'),
	(36,'SMP KRISTEN KALAM KUDUS'),
	(37,'SMP MARIA IMMACULATA MARSUDIRINI'),
	(38,'SMP MUHAMMADIYAH 1 YOGYAKARTA'),
	(39,'SMP MUHAMMADIYAH 10 YOGYAKARTA'),
	(40,'SMP MUHAMMADIYAH 2 YOGYAKARTA'),
	(41,'SMP MUHAMMADIYAH 3 YOGYAKARTA'),
	(42,'SMP MUHAMMADIYAH 4 YOGYAKARTA'),
	(43,'SMP MUHAMMADIYAH 5 YOGYAKARTA'),
	(44,'SMP MUHAMMADIYAH 6 YOGYAKARTA'),
	(45,'SMP MUHAMMADIYAH 7 YOGYAKARTA'),
	(46,'SMP MUHAMMADIYAH 8 YOGYAKARTA'),
	(47,'SMP MUHAMMADIYAH 9 YOGYAKARTA'),
	(48,'SMP PANGUDI LUHUR 1 YOGYAKARTA'),
	(49,'SMP PEMBANGUNAN MAARIF YOGYAKARTA'),
	(50,'SMP PERAK YOGYAKARTA'),
	(51,'SMP PERINTIS YOGYAKARTA'),
	(52,'SMP PIRI 1 YOGYAKARTA'),
	(53,'SMP PIRI 2 YOGYAKARTA'),
	(54,'SMP STELLA DUCE 1'),
	(55,'SMP STELLA DUCE 2 YOGYAKARTA'),
	(56,'SMP TAMAN DEWASA IBU PAWIYATAN TAMANSISWA'),
	(57,'SMP TAMAN DEWASA JETIS'),
	(58,'SMP TAMAN DEWASA KUMENDAMAN YOGYAKARTA'),
	(59,'SMAN 1 YOGYAKARTA'),
	(60,'SMAN 10 YOGYAKARTA'),
	(61,'SMAN 11 YOGYAKARTA'),
	(62,'SMAN 2 YOGYAKARTA'),
	(63,'SMAN 3 YOGYAKARTA'),
	(64,'SMAN 4 YOGYAKARTA'),
	(65,'SMAN 5 YOGYAKARTA'),
	(66,'SMAN 6 YOGYAKARTA'),
	(67,'SMAN 7 YOGYAKARTA'),
	(68,'SMAN 8 YOGYAKARTA'),
	(69,'SMAN 9 YOGYAKARTA'),
	(70,'SMA 17 Yogyakarta'),
	(71,'SMAS BERBUDI'),
	(72,'SMAS BHINNEKA TUNGGAL IKA'),
	(73,'SMAS BOPKRI 1'),
	(74,'SMAS BOPKRI 2'),
	(75,'SMAS BUDI LUHUR'),
	(76,'SMAS BUDYA WACANA'),
	(77,'SMAS GADJAH MADA'),
	(78,'SMAS GOTONG ROYONG'),
	(79,'SMAS IT ABU BAKAR'),
	(80,'SMAS IT BINA ANAK SHOLEH'),
	(81,'SMAS KATOLIK SANG TIMUR'),
	(82,'SMAS MA ARIF YOGYAKARTA'),
	(83,'SMAS MUHAMMADIYAH 1 YOGYAKARTA'),
	(84,'SMAS MUHAMMADIYAH 2 YOGYAKARTA'),
	(85,'SMAS MUHAMMADIYAH 3 YOGYAKARTA'),
	(86,'SMAS MUHAMMADIYAH 4 YOGYAKARTA'),
	(87,'SMAS MUHAMMADIYAH 5 YOGYAKARTA'),
	(88,'SMAS MUHAMMADIYAH 6 YOGYAKARTA'),
	(89,'SMAS MUHAMMADIYAH 7 YOGYAKARTA'),
	(90,'SMAS PANGUDI LUHUR'),
	(91,'SMAS PERAK'),
	(92,'SMAS PIRI 1 YOGYAKARTA'),
	(93,'SMAS SANTA MARIA'),
	(94,'SMAS SANTO THOMAS'),
	(95,'SMAS STELLA DUCE 1'),
	(96,'SMAS STELLA DUCE 2'),
	(97,'SMAS SULTAN AGUNG'),
	(98,'SMAS TAMAN MADYA IP'),
	(99,'SMAS TAMAN MADYA JETIS'),
	(100,'SMKN 1 YOGYAKARTA'),
	(101,'SMKN 2 YOGYAKARTA'),
	(102,'SMKN 3 YOGYAKARTA'),
	(103,'SMKN 4 YOGYAKARTA'),
	(104,'SMKN 5 YOGYAKARTA'),
	(105,'SMKN 6 YOGYAKARTA'),
	(106,'SMKN 7 YOGYAKARTA'),
	(107,'SMK SMTI'),
	(108,'SMKS BERBUDI YOGYAKARTA'),
	(109,'SMKS BOPKRI 1 YOGYAKARTA'),
	(110,'SMKS BOPKRI 2 YOGYAKARTA'),
	(111,'SMKS IBU PAWIYATAN TAMANSISWA'),
	(112,'SMKS INDONESIA YOGYAKARTA'),
	(113,'SMKS INSAN MULIA'),
	(114,'SMKS ISLAM YOGYAKARTA'),
	(115,'SMKS KESEHATAN CIPTA BHAKTI HUSADA YOGYAKARTA'),
	(116,'SMKS KOPERASI YOGYAKARTA'),
	(117,'SMKS MAARIF 1 YOGYAKARTA'),
	(118,'SMKS MARSUDI LUHUR I YOGYAKARTA'),
	(119,'SMKS MUHAMMADIYAH 1 YOGYAKARTA'),
	(120,'SMKS MUHAMMADIYAH 2 YOGYAKARTA'),
	(121,'SMKS MUHAMMADIYAH 3 YOGYAKARTA'),
	(122,'SMKS MUHAMMADIYAH 4 YOGYAKARTA'),
	(123,'SMKS PEMBANGUNAN'),
	(124,'SMKS PERINDUSTRIAN YOGYAKARTA'),
	(125,'SMKS PERKEBUNAN MM 52'),
	(126,'SMKS PIRI 1 YOGYAKARTA'),
	(127,'SMKS PIRI 2 YOGYAKARTA'),
	(128,'SMKS PIRI 3 YOGYAKARTA'),
	(129,'SMKS TAMANSISWA JETIS');

/*!40000 ALTER TABLE `sekolah` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tim
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tim`;

CREATE TABLE `tim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `kode` varchar(30) NOT NULL COMMENT '(judul)-(event 3)(kategori 3)-(kode 4)',
  `nama` varchar(30) NOT NULL,
  `inovasi` text,
  `abstract` text,
  `provinsi_id` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kategori_id` int(11) NOT NULL,
  `kategori_sub_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tim_user_id_foreign` (`user_id`),
  KEY `tim_provinsi_id_foreign` (`provinsi_id`),
  KEY `tim_kategori_id_foreign` (`kategori_id`),
  KEY `tim_kategori_sub_id_foreign` (`kategori_sub_id`),
  CONSTRAINT `tim_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`),
  CONSTRAINT `tim_kategori_sub_id_foreign` FOREIGN KEY (`kategori_sub_id`) REFERENCES `kategori_sub` (`id`),
  CONSTRAINT `tim_provinsi_id_foreign` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`),
  CONSTRAINT `tim_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tim` WRITE;
/*!40000 ALTER TABLE `tim` DISABLE KEYS */;

INSERT INTO `tim` (`id`, `user_id`, `kode`, `nama`, `inovasi`, `abstract`, `provinsi_id`, `kategori_id`, `kategori_sub_id`, `created_at`, `updated_at`)
VALUES
	(1,4,'AIP-001001-0001','Tim Tio Tamara','Programming','Google Language','34',2,2,'2020-05-14 00:33:47','2020-12-29 18:49:07');

/*!40000 ALTER TABLE `tim` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_role_id_foreign` (`role_id`),
  KEY `user_event_id_foreign` (`event_id`),
  CONSTRAINT `user_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `email`, `password`, `role_id`, `event_id`, `created_at`, `updated_at`)
VALUES
	(1,'admin','admin@admin.com','9102574b11ac0fb08c54359e0e9fc2c9',1,1,'2020-05-13 00:48:59',NULL),
	(2,'panitia','panitia@panitia.com','d32b1673837a6a45f795c13ea67ec79e',2,1,'2020-05-13 00:48:59',NULL),
	(3,'juri','juri@juri.com','2f40bde8529e99bf8648a0a5718d0650',3,1,'2020-05-13 00:48:59',NULL),
	(4,'peserta','peserta@peserta.com','129451d83a60351a82516cb836842c68',4,1,'2020-05-13 00:48:59',NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_meta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_meta`;

CREATE TABLE `user_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `meta_value` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meta_user_id_foreign` (`user_id`),
  CONSTRAINT `meta_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_meta` WRITE;
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;

INSERT INTO `user_meta` (`id`, `user_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`)
VALUES
	(1,4,'team_makalah_1','4_makalah1.pdf','2020-05-20 00:13:59',NULL),
	(2,4,'team_lampiran_1','4_lampiran1.jpg','2020-05-20 00:13:59',NULL),
	(3,4,'team_upload_makalah_count','2','2020-05-20 00:13:59',NULL);

/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
