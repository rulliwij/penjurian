<?php
if (!function_exists('asset')) {
    function asset($url)
    {
        return base_url('assets/gentelella/' . $url);
    }
}

function set_view($view, $data=[])
{
    $ci =& get_instance();
    $data['content'] = $ci->load->view($view, $data, TRUE);
    $ci->load->view('base', $data);
}

if (!function_exists('dd')) {
    function dd($data)
    {
        echo '<pre>';
        print_r($data);
        die;
    }
}

if (!function_exists('role_slug')) {
    function role_slug($role_id)
    {
        $slug = '';
        if ($role_id == "1") {
            $slug = "admin";
        }else if ($role_id == "2") {
            $slug = "panitia";
        }else if ($role_id == "3") {
            $slug = "juri";
        }else if ($role_id == "4") {
            $slug = "peserta";
        }
        return $slug;
    }
}

if (!function_exists('get_current_user_id')) {
    function get_current_user_id()
    {
        $ci=& get_instance();
        return $ci->session->userdata('id');
    }
}

if (!function_exists('get_current_event_id')) {
    function get_current_event_id()
    {
        $ci=& get_instance();
        return $ci->session->userdata('event_id');
    }
}

if (!function_exists('get_user_meta')) {
    function get_user_meta($user_id,$key, $loss=FALSE)
    {
        $ci=& get_instance();
        $table = get_table('website_table_user_meta');
        if (!$table) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
        $sql = "
                SELECT * FROM $table 
                WHERE user_id = $user_id 
                    AND meta_key = '$key'
                "; 
        $query = $ci->db->query($sql);
        if ($loss) {
            $row = $query->row()->meta_value ?? FALSE;
        }else{
            $row = $query->row();
        }
        return $row;
    }
}

if (!function_exists('add_user_meta')) {
    function add_user_meta($user_id,$key,$value)
    {
        $ci=& get_instance();
        $table = get_table('website_table_user_meta');
        if (!$table) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
        $data = array(
            'user_id' => $user_id,
            'meta_key' => $key,
            'meta_value' => $value,
            'created_at' => date('Y-m-d H:i:s')
        );

        return $ci->db->insert($table, $data);
    }
}

if (!function_exists('update_user_meta')) {
    function update_user_meta($user_id,$key,$value)
    {
        $ci=& get_instance();
        $table = get_table('website_table_user_meta');
        if (!$table) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
        $data = array(
            'meta_value' => $value,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $ci->db->where('user_id', $user_id);
        $ci->db->where('meta_key', $key);
        return $ci->db->update($table, $data);
    }
}

if (!function_exists('get_gender_label')) {
    function get_gender_label($value)
    {
        if ($value == '1') {
            $result = 'Laki-laki';
        }else{
            $result = 'Perempuan';
        }
        return $result;
    }
}

if (!function_exists('get_status_label')) {
    function get_status_label($value)
    {
        if ($value == '0') {
            $result = 'Tidak Aktif';
        }else{
            $result = 'Aktif';
        }
        return $result;
    }
}

if (!function_exists('get_permalink_materi')) {
    function get_permalink_materi($filename)
    {
        if (!$filename) {
            return FALSE;
        }
        $path = base_url('uploads/materi/'.$filename);
        return $path;
    }
}

if (!function_exists('get_permalink_profile')) {
    function get_permalink_profile($filename)
    {
        if (!$filename) {
            return FALSE;
        }
        $path = base_url('uploads/profile/'.$filename);
        return $path;
    }
}

if (!function_exists('get_option')) {
    function get_option($key,$loss=FALSE)
    {
        $ci=& get_instance();
        $table = "options";
        if (!$table) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
        $sql = "
                SELECT * FROM $table 
                WHERE option_key = '$key'
            ";
        $query = $ci->db->query($sql);
        if ($loss) {
            $row = $query->row()->option_value ?? FALSE;
        }else{
            $row = $query->row();
        }
        return $row;
    }
}

if (!function_exists('get_table')) {
    function get_table($key)
    {
        return get_option($key)->option_value ?? FALSE;
    }
}

if (!function_exists('saveDataHelper')) {
    function saveDataHelper($table, $data)
    {
        $ci=& get_instance();
        return $ci->db->insert($table, $data);
    }
}

if (!function_exists('updateDataHelper')) {
    function updateDataHelper($table, $where, $data)
    {
        $ci=& get_instance();
        if (!$where) {
            return FALSE;
        }
        $ci->db->where($where);
        return $ci->db->update($table, $data);
    }
}

if (!function_exists('generateTimCode')) {
    function generateTimCode($id,$kategori_id=1) {
        $ci=& get_instance();
        return "AIP-".sprintf("%'.03d", $ci->session->userdata('event_id')).sprintf("%'.03d", $kategori_id).'-'.sprintf("%'.04d", $id);
    }
}

if (!function_exists('getData')) {
    function getData($table, $where, $row=FALSE)
    {
        $ci=& get_instance();
        if ($where) {
            $ci->db->where($where);
        }
        return $row ? $ci->db->get($table)->row_array() : $ci->db->get($table)->result_array();
    }
}

if (!function_exists('get_current_event_year')) {
    function get_current_event_year()
    {
        $ci=& get_instance();
        $tableEvent = get_table('website_table_event');
        return $ci->db->get_where($tableEvent,['YEAR(tanggal_mulai)' => date('Y')])->row_array();
    }
}