<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_peserta extends CI_Model
{	
    function __construct(){
        parent::__construct();
        $this->tableUser = get_table('website_table_user');
        $this->tableAnggota = get_table('website_table_anggota');
        $this->tablePengumuman = get_table('website_table_pengumuman');
        $this->tableProvinsi = get_table('website_table_provinsi');
        $this->tableKabupaten = get_table('website_table_kabupaten');
        $this->tableKategori = get_table('website_table_kategori');
        $this->tableKategoriSub = get_table('website_table_kategori_sub');
        $this->tableTim = get_table('website_table_tim');
        $this->tableEvent = get_table('website_table_event');
        if (!$this->tableUser || !$this->tableAnggota || !$this->tablePengumuman || !$this->tableProvinsi || !$this->tableKabupaten || !$this->tableKategori || !$this->tableKategoriSub || !$this->tableTim || !$this->tableEvent) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
    }
	public function _uploadFile($nameFile='materi',$ext="docx|pdf|doc", $debug=false)
	{
        $config = [];
        $config['upload_path']   = './uploads/materi/';
        $config['allowed_types'] = $ext;
        $config['file_name']     = get_current_user_id()."_".$nameFile;
        $config['overwrite']     = true;
        $config['max_size']      = 2048; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$this->load->library('upload', $config);
        if ($debug) {
            dd($config);
        }
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($nameFile)){
            $data = array(
                'error' => TRUE,
                'data' => $this->upload->display_errors()
            );
        }else{
            $data = array(
                'error' => FALSE,
                'data' => $this->upload->data()
            );
        }
        return $data;
	}

    public function _uploadFileDinamis($nameFile='materi',$ext="docx|pdf|doc", $filename='', $path='./uploads/profile/')
    {
        if (!$filename) {
            $filename = get_current_user_id()."_".$nameFile;
        }
        $config = [];
        $config['upload_path']   = $path;
        $config['allowed_types'] = $ext;
        $config['file_name']     = $filename;
        $config['overwrite']     = true;
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($nameFile)){
            $data = array(
                'error' => TRUE,
                'data' => $this->upload->display_errors()
            );
        }else{
            $data = array(
                'error' => FALSE,
                'data' => $this->upload->data()
            );
        }
        return $data;
    }

	// private function _deleteFile($id)
	// {
	// 	$product = $this->getById($id);
	// 	if ($product->image != "default.jpg") {
	// 		$filename = explode(".", $product->image)[0];
	// 		return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
	// 	}
	// }

    public function getDataAnggota()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get_where($this->tableAnggota, ['user_id' => get_current_user_id()]);
    }

    public function getDataKetua()
    {
        return $this->db->get_where($this->tableAnggota, ['jabatan' => 'ketua','user_id' => get_current_user_id()])->row();
    }

    public function getDataAnggotaByID($id)
    {
        return $this->db->get_where($this->tableAnggota,['id' => $id]);
    }

    public function insertDataAnggota($data)
    {
        return $this->db->insert($this->tableAnggota, $data);
    }

    public function updateDataAnggota($data)
    {
        return $this->db->update($this->tableAnggota, $data);
    }

    public function deleteDataAnggota($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->tableAnggota);
    }

    public function getDataAll()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get_where($this->tablePengumuman, ['status' => "1"])->num_rows();
    }
    
    public function getData($limit,$page)
    {
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $page);
        return $this->db->get_where($this->tablePengumuman, ['status' => "1"]);
    }

    public function getDataProvinsi()
    {
        $this->db->order_by("name", "asc");
        return $this->db->get($this->tableProvinsi)->result_array();
    }

    public function getDataKategori()
    {
        $this->db->where("event_id", get_current_event_id());
        $this->db->order_by("nama", "asc");
        return $this->db->get($this->tableKategori)->result_array();
    }

    public function getDataKabupaten($id_provinsi)
    {
        $this->db->order_by("name", "asc");
        return $this->db->get_where($this->tableKabupaten,['province_id' => $id_provinsi])->result_array();
    }

    public function getDataTim()
    {
        $this->db->order_by("nama", "asc");
        return $this->db->get_where($this->tableTim,['user_id' => get_current_user_id()])->row_array();
    }

    public function getDataTimByID($tim_id='')
    {
        
    }

    public function getDataTimByUserID($user_id='')
    {
        if (!$user_id) {
            return FALSE;
        }
        $this->db->where("user_id", $user_id);
        $exist = $this->db->get($this->tableTim)->row_array();
        return $exist;
    }

    public function getNextPrimaryKey($table='')
    {
        $table = $table ? $table : $this->tableAnggota;
        $this->db->select_max('id');
        $result= $this->db->get($table)->row_array();
        return $result['id'] ?? 1;
    }

    public function getDataInstansiByID($kategori_id)
    {
        $this->db->order_by("nama", "asc");
        return $this->db->get_where($this->tableKategoriSub,['kategori_id' => $kategori_id])->result_array();
    }

    function saveTim($data)
    {
        if (!$data) {
            return FALSE;
        }
        $user_id = get_current_user_id();
        $dataTim = $this->getDataTimByUserID($user_id);
        if ($dataTim) {
            // update
            $data['updated_at'] = date('Y-m-d H:i:s');
            $save = updateDataHelper($this->tableTim, ['user_id' => $user_id], $data);
        }else{
            // create
            $data['user_id'] = $user_id;
            $data['kode'] = generateTimCode($this->getNextPrimaryKey($this->tableTim));
            $data['created_at'] = date('Y-m-d H:i:s');
            $save = saveDataHelper($this->tableTim, $data);
        }
        if (!$save) {
            return FALSE;
        }
        return TRUE;
    }

    public function getDataEventByID()
    {
        return $this->db->get_where($this->tableEvent,['id' => $this->session->userdata('event_id')])->row_array();
    }
}
