<?php
class M_login extends CI_Model{	
	function __construct(){
	    parent::__construct();
	    $this->tableUser = get_table('website_table_user');
	    $this->tableJuri = get_table('website_table_juri');
	    $this->tableTim = get_table('website_table_tim');
	    $this->tableEvent = get_table('website_table_event');
	    if (!$this->tableUser || !$this->tableJuri || !$this->tableTim || !$this->tableEvent) {
	    	die(get_option('website_comment_table_not_found', TRUE));
	    }
	}

	function cek_login($username,$password){
		$where = array(
			'username' => $username,
			'password' => md5($password)
		);
		return $this->db->get_where($this->tableUser,$where);
	}

	public function getDataJuriByUserID($user_id)
	{
	    return $this->db->get_where($this->tableJuri, ['user_id' => $user_id])->row_array();
	}

	public function getDataPesertaByUserID($user_id)
	{
	    return $this->db->get_where($this->tableTim, ['user_id' => $user_id])->row_array();
	}

	public function get_current_event_year()
	{
	    return $this->db->get_where($this->tableEvent,['YEAR(tanggal_mulai)' => date('Y')])->row_array();
	}

	public function insertDataUser($data)
	{
	    return $this->db->insert($this->tableUser, $data);
	}

	function getUserByJssID($jssID = false){
		if (!$jssID) {
			return false;
		}
		$where = array(
			'jss_id' => $jssID
		);
		return $this->db->get_where($this->tableUser,$where);
	}
	
	function registerUserJSS($dataJSS = false){
		if (!$dataJSS) {
			return false;
		}

		$eventID = get_current_event_year()['id'] ?? 1;
		$rolePeserta = get_option('website_role_peserta', TRUE) ?? 4;
		$data = array(
			'jss_id' => $dataJSS->id_upik,
			'username' => $dataJSS->username,
			'email' => $dataJSS->email,
			'password' => md5($dataJSS->username),
			'role_id' => $rolePeserta,
			'event_id' => $eventID,
		);
		return $this->db->insert($this->tableUser, $data);
	}
}