<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_juri extends CI_Model
{	
    function __construct(){
        parent::__construct();
        $this->tableUser = get_table('website_table_user');
        $this->tableJuri = get_table('website_table_juri');
        $this->tablePengumuman = get_table('website_table_pengumuman');
        $this->tableSekolah = get_table('website_table_sekolah');
        $this->tableAnggota = get_table('website_table_anggota');
        $this->tableNilai = get_table('website_table_nilai');
        $this->tableTim = get_table('website_table_tim');
        $this->tableEvent = get_table('website_table_event');
        $this->tableKategori = get_table('website_table_kategori');
        $this->tableKategoriSub = get_table('website_table_kategori_sub');
        $this->tableJuriKategori = get_table('website_table_juri_kategori');
        if (!$this->tableUser || !$this->tableJuri || !$this->tablePengumuman || !$this->tableSekolah || !$this->tableAnggota || !$this->tableNilai || !$this->tableTim || !$this->tableEvent  || !$this->tableKategori  || !$this->tableJuriKategori || !$this->tableKategoriSub) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
    }

    public function getDataProfile()
    {
        $result = [];
        $this->db->select("*");
        $this->db->where("user_id", get_current_user_id());
        $data = $this->db->get($this->tableJuri);
        $data = ($data->num_rows() > 0) ? $data->row_array() : [];
        if ($data) {
            $this->db->select("
                $this->tableKategori.id, 
                $this->tableKategori.nama, 
                (SELECT COUNT(id) FROM $this->tableTim WHERE $this->tableJuriKategori.kategori_id = $this->tableTim.kategori_id) AS total
            ");
            $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableJuriKategori.'.kategori_id');
            $this->db->group_by('kategori_id');
            $this->db->where("user_id", $data['user_id']);
            $kategori = $this->db->get($this->tableJuriKategori);
            $kategori = ($kategori->num_rows() > 0) ? $kategori->result_array() : [];
            $data['kategori'] = $kategori;
        }
        return $data;
    }

    public function getDataPengumumanAll()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get_where($this->tablePengumuman, ['status' => "1"])->num_rows();
    }
    
    public function getDataPengumuman($limit,$page)
    {
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $page);
        return $this->db->get_where($this->tablePengumuman, ['status' => "1"]);
    }

    public function getDataPesertaAll()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get_where($this->tableUser, ['role_id' => get_option('website_role_peserta',TRUE)])->num_rows();
    }
    
    public function getDataPeserta()
    {
        $this->db->select('kategori_id');
        $kategoriJuri = $this->db->get_where($this->tableJuriKategori,['user_id' => get_current_user_id()])->result_array();
        if (!$kategoriJuri) {
            return FALSE;
        }
        $kategoriJuriID = [];
        foreach ($kategoriJuri as $key => $value) {
            $kategoriJuriID[] = $value['kategori_id'];
        }
        $this->db->select("$this->tableUser.*");
        $this->db->join($this->tableTim, $this->tableTim.'.user_id = '.$this->tableUser.'.id');
        $this->db->where(['role_id' => get_option('website_role_peserta',TRUE)]);
        $this->db->where_in("$this->tableTim.kategori_id", $kategoriJuriID);
        $this->db->order_by("$this->tableUser.id", "desc");
        $data = $this->db->get($this->tableUser);
        $results = [];
        if ($data->num_rows() > 0) {
            $results = $data->result();
            foreach ($results as $key => $value) {
                $nilai = $this->getNilai($value->id);
                $tim = $this->getTimByUserID($value->id);
                $makalah = ((20/100)*$nilai['ide']) + ((15/100)*$nilai['metode']) + ((40/100)*$nilai['manfaat']);
                $grandFinal = ((10/100)*$nilai['display']) + ((15/100)*$nilai['presentasi']);
                $nilai['makalah'] = $makalah;
                $nilai['grand_final'] = $grandFinal;
                $results[$key]->nilai = $nilai;
                $results[$key]->tim = $tim;
            }
        }
        return $results;
    }

    public function getDataTimByUserID($user_id)
    {
        $this->db->select("
            $this->tableTim.*,
            $this->tableKategori.nama AS kategori_nama, 
            $this->tableKategoriSub.nama AS kategori_sub_nama,
            (SELECT COUNT($this->tableAnggota.user_id) FROM $this->tableAnggota WHERE $this->tableAnggota.user_id = $this->tableTim.user_id) AS jumlah_tim
        ");
        $this->db->order_by($this->tableTim.".id", "desc");
        $this->db->where([
            $this->tableUser.".event_id" => $this->session->userdata('event_id'),
            $this->tableTim.".user_id" => $user_id
        ]);
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableTim.'.user_id');
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        $this->db->join($this->tableKategoriSub, $this->tableKategoriSub.'.id = '.$this->tableTim.'.kategori_sub_id');
        return $this->db->get($this->tableTim)->row_array();
    }

    public function getDataCategoryByID($id)
    {
        if (!$id) {
            return FALSE;
        }
        $label = '';
        switch ($id) {
            case 1:
                $label = 'Inovasi Pelajar SMP & SMA';
                break;
            case 2:
                $label = 'Inovasi Mahasiswa & Umum';
                break;
            case 3:
                $label = 'Penelitian Pelajar SMP & SMA';
                break;
            case 4:
                $label = 'Penelitian Mahasiswa & Umum';
                break;
            default:
                $label = 'belum diketahui';
                break;
        }
        return $label;
    }

    public function getDataSekolahByID($id)
    {
        if (!$id) {
            return FALSE;
        }
        return $this->db->get_where($this->tableSekolah,['id' => $id])->row_array();
    }

    public function getDataAnggotaByUserID($id)
    {
        if (!$id) {
            return FALSE;
        }
        return $this->db->query("
            SELECT * FROM $this->tableAnggota 
            WHERE user_id = $id 
            ORDER BY FIELD(jabatan, 'ketua','pembimbing','anggota')"
        )->result_array();
    }

    public function _uploadFileDinamis($nameFile='materi',$ext="docx|pdf|doc", $filename='', $path='./uploads/profile/')
    {
        if (!$filename) {
            $filename = get_current_user_id()."_".$nameFile;
        }
        $config = [];
        $config['upload_path']   = $path;
        $config['allowed_types'] = $ext;
        $config['file_name']     = $filename;
        $config['overwrite']     = true;
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($nameFile)){
            $data = array(
                'error' => TRUE,
                'data' => $this->upload->display_errors()
            );
        }else{
            $data = array(
                'error' => FALSE,
                'data' => $this->upload->data()
            );
        }
        return $data;
    }

    public function getNilai($tim_id)
    {
        return $this->db->get_where($this->tableNilai, ['tim_id' => $tim_id,'juri_id' => get_current_user_id()])->row_array();
    }

    public function getTimByUserID($tim_id)
    {
        return $this->db->get_where($this->tableTim, ['user_id' => $tim_id])->row_array();
    }

    public function getDataEventByID()
    {
        return $this->db->get_where($this->tableEvent,['id' => $this->session->userdata('event_id')])->row_array();
    }

    public function getDataJuri()
    {
        $result = [];
        $this->db->select("
            $this->tableJuri.*,
            $this->tableJuri.email AS email_publik,
            $this->tableUser.username,
            $this->tableUser.email
        ");
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableJuri.'.user_id');
        $this->db->where("$this->tableJuri.user_id", get_current_user_id());
        $data = $this->db->get($this->tableJuri);
        $data = ($data->num_rows() > 0) ? $data->row_array() : [];
        if ($data) {
            $this->db->select("
                $this->tableKategori.id, 
                $this->tableKategori.nama, 
                (SELECT COUNT(id) FROM $this->tableTim WHERE $this->tableJuriKategori.kategori_id = $this->tableTim.kategori_id) AS total,
                (
                    SELECT COUNT($this->tableNilai.id) FROM $this->tableTim 
                    JOIN $this->tableNilai ON $this->tableNilai.tim_id = $this->tableTim.user_id 
                    WHERE $this->tableTim.user_id = $this->tableNilai.tim_id 
                    AND $this->tableNilai.juri_id = $this->tableJuriKategori.user_id
                    AND $this->tableJuriKategori.kategori_id = $this->tableTim.kategori_id
                ) 
                AS total_dinilai,
            ");
            $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableJuriKategori.'.kategori_id');
            $this->db->group_by('kategori_id');
            $this->db->where("user_id", $data['user_id']);
            $kategori = $this->db->get($this->tableJuriKategori);
            $kategori = ($kategori->num_rows() > 0) ? $kategori->result_array() : [];
            $data['kategori'] = $kategori;
        }
        return $data;
    }
}
