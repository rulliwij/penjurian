<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_panitia extends CI_Model
{	
    function __construct(){
        parent::__construct();
        $this->tableJuri = get_table('website_table_juri');
        $this->tableUser = get_table('website_table_user');
        $this->tablePengumuman = get_table('website_table_pengumuman');
        $this->tableEvent = get_table('website_table_event');
        $this->tableTim = get_table('website_table_tim');
        $this->tableKategori = get_table('website_table_kategori');
        $this->tableKategoriSub = get_table('website_table_kategori_sub');
        $this->tableAnggota = get_table('website_table_anggota');
        $this->tableJuriKategori = get_table('website_table_juri_kategori');
        $this->tableNilai = get_table('website_table_nilai');
        if (!$this->tableJuri || !$this->tablePengumuman || !$this->tableEvent || !$this->tableTim || !$this->tableKategori || !$this->tableKategoriSub || !$this->tableAnggota || !$this->tableJuriKategori || !$this->tableNilai) {
            die(get_option('website_comment_table_not_found', TRUE));
        }
        $this->roleAnggota = array('ketua','anggota','pembimbing');
        $this->roleIdPeserta = get_option('website_role_peserta',TRUE);
    }

    public function getDataProfile()
    {
        $data = $this->db->get_where($this->tableJuri, ['user_id' => get_current_user_id()]);
        if ($data->num_rows() > 0) {
            return $data->row();
        }
        return FALSE;
    }

    public function getDataPengumumanAll()
    {
        $this->db->where("event_id", $this->session->userdata('event_id'));
        $this->db->order_by("id", "desc");
        return $this->db->get($this->tablePengumuman);
    }

    public function getDataPengumumanByID($id)
    {
        return $this->db->get_where($this->tablePengumuman,['id' => $id]);
    }
    
    public function getDataPengumuman($limit,$page)
    {
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $page);
        return $this->db->get($this->tablePengumuman);
    }

    public function insertDataPengumuman($data)
    {
        return $this->db->insert($this->tablePengumuman, $data);
    }

    public function updateDataPengumuman($data)
    {
        return $this->db->update($this->tablePengumuman, $data);
    }

    public function getDataJuriAll()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get_where($this->tableUser, ['role_id' => get_option('website_role_juri',TRUE)])->num_rows();
    }
    
    public function getDataJuri()
    {
        $result = [];
        $this->db->select("$this->tableJuri.*");
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableJuri.'.user_id');
        $this->db->where("$this->tableUser.event_id", $this->session->userdata('event_id'));
        $this->db->order_by("$this->tableUser.id", "desc");
        $data = $this->db->get($this->tableJuri);
        $data = ($data->num_rows() > 0) ? $data->result_array() : [];
        if ($data) {
            foreach ($data as $key => $value) {
                $this->db->select("
                    $this->tableKategori.id, 
                    $this->tableKategori.nama, 
                    (SELECT COUNT(id) FROM $this->tableTim WHERE $this->tableJuriKategori.kategori_id = $this->tableTim.kategori_id) AS total,
                    (
                        SELECT COUNT($this->tableNilai.id) FROM $this->tableTim 
                        JOIN $this->tableNilai ON $this->tableNilai.tim_id = $this->tableTim.user_id 
                        WHERE $this->tableTim.user_id = $this->tableNilai.tim_id 
                        AND $this->tableNilai.juri_id = $this->tableJuriKategori.user_id
                        AND $this->tableJuriKategori.kategori_id = $this->tableTim.kategori_id
                    ) 
                    AS total_dinilai,
                ");
               $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableJuriKategori.'.kategori_id');
               $this->db->group_by('kategori_id');
               $this->db->where("user_id", $value['user_id']);
               $kategori = $this->db->get($this->tableJuriKategori);
               $kategori = ($kategori->num_rows() > 0) ? $kategori->result_array() : [];
               $data[$key]['kategori'] = $kategori;
            }
        }
        return $data;
    }

    public function getDataJuriByID($id)
    {
        $result = [];
        $this->db->select("
            $this->tableJuri.*,
            $this->tableJuri.email AS email_publik,
            $this->tableUser.username,
            $this->tableUser.email
        ");
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableJuri.'.user_id');
        $this->db->where("$this->tableJuri.id", $id);
        $data = $this->db->get($this->tableJuri);
        $data = ($data->num_rows() > 0) ? $data->row_array() : [];
        if ($data) {
            $this->db->select("
                $this->tableKategori.id, 
                $this->tableKategori.nama, 
                (SELECT COUNT(id) FROM $this->tableTim WHERE $this->tableJuriKategori.kategori_id = $this->tableTim.kategori_id) AS total
            ");
            $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableJuriKategori.'.kategori_id');
            $this->db->group_by('kategori_id');
            $this->db->where("user_id", $data['user_id']);
            $kategori = $this->db->get($this->tableJuriKategori);
            $kategori = ($kategori->num_rows() > 0) ? $kategori->result_array() : [];
            $data['kategori'] = $kategori;
        }
        return $data;
    }

    public function getDataEvent($limit,$page)
    {
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $page);
        return $this->db->get($this->tableEvent);
    }

    public function getDataEventAll()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get($this->tableEvent)->num_rows();
    }

    public function getDataEventAlls()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get($this->tableEvent)->result_array();
    }

    public function getDataEventAvailable()
    {
        $this->db->order_by("id", "desc");
        return $this->db->get_where($this->tableEvent,['YEAR(tanggal_mulai) >=' => date('Y')])->result_array();
    }

    public function insertDataEvent($data)
    {
        return $this->db->insert($this->tableEvent, $data);
    }

    public function insertDataUser($data)
    {
        return $this->db->insert($this->tableUser, $data);
    }
    
    public function updateDataUser($data)
    {
        return $this->db->update($this->tableUser, $data);
    }

    public function insertDataJuri($data)
    {
        return $this->db->insert($this->tableJuri, $data);
    }
    
    public function updateDataJuri($data)
    {
        return $this->db->update($this->tableJuri, $data);
    }

    public function insertDataJuriKategori($data)
    {
        return $this->db->insert($this->tableJuriKategori, $data);
    }
    public function deleteDataJuriKategori()
    {
        return $this->db->delete($this->tableJuriKategori);
    }

    public function get_current_event_year($tahun)
    {
        return $this->db->get_where($this->tableEvent,['YEAR(tanggal_mulai)' => $tahun])->row_array();
    }
    
    public function getDataEventByID($id)
    {
        return $this->db->get_where($this->tableEvent,['id' => $id]);
    }

    public function updateDataEvent($data)
    {
        return $this->db->update($this->tableEvent, $data);
    }

    public function getDataTimAll()
    {
        $this->db->select("
            $this->tableTim.*,
            $this->tableKategori.nama AS kategori_nama, 
            $this->tableKategoriSub.nama AS kategori_sub_nama,
            (SELECT COUNT($this->tableAnggota.user_id) FROM $this->tableAnggota WHERE $this->tableAnggota.user_id = $this->tableTim.user_id) AS jumlah_tim
        ");
        $this->db->order_by($this->tableTim.".id", "desc");
        $this->db->where($this->tableUser.".event_id", $this->session->userdata('event_id'));
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableTim.'.user_id');
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        $this->db->join($this->tableKategoriSub, $this->tableKategoriSub.'.id = '.$this->tableTim.'.kategori_sub_id');
        return $this->db->get($this->tableTim);
    }

    public function getDataTimByIDAll($kategori_id)
    {
        $this->db->select("
            $this->tableTim.*,
            $this->tableKategori.nama AS kategori_nama, 
            $this->tableKategoriSub.nama AS kategori_sub_nama,
            (SELECT COUNT($this->tableAnggota.user_id) FROM $this->tableAnggota WHERE $this->tableAnggota.user_id = $this->tableTim.user_id) AS jumlah_tim
        ");
        $this->db->order_by($this->tableTim.".id", "desc");
        $this->db->where([
            $this->tableUser.".event_id" => $this->session->userdata('event_id'),
            $this->tableTim.".kategori_id" => $kategori_id
        ]);
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableTim.'.user_id');
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        $this->db->join($this->tableKategoriSub, $this->tableKategoriSub.'.id = '.$this->tableTim.'.kategori_sub_id');
        return $this->db->get($this->tableTim);
    }

    public function getDataRekapPenjurian()
    {
        $this->db->select("
            $this->tableTim.*,
            $this->tableKategori.nama AS kategori_nama, 
            (SELECT COUNT($this->tableAnggota.user_id) FROM $this->tableAnggota WHERE $this->tableAnggota.user_id = $this->tableTim.user_id) AS jumlah_tim,
            (
                IFNULL(
                    (
                        SELECT 
                            SUM(
                                ((20/100)*$this->tableNilai.ide) +
                                ((15/100)*$this->tableNilai.metode) +
                                ((40/100)*$this->tableNilai.manfaat)
                            )
                        FROM $this->tableNilai WHERE $this->tableNilai.tim_id = $this->tableTim.id
                    )
                    ,0
                )
            ) AS pm,
            (
                IFNULL(
                    (
                        SELECT 
                            SUM(
                                ((10/100)*$this->tableNilai.display) +
                                ((15/100)*$this->tableNilai.presentasi)
                            )
                        FROM $this->tableNilai WHERE $this->tableNilai.tim_id = $this->tableTim.id
                    )
                    ,0
                )
            ) AS gf,
        ");
        $this->db->order_by($this->tableTim.".id", "desc");
        $this->db->where([
            $this->tableUser.".event_id" => $this->session->userdata('event_id')
        ]);
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableTim.'.user_id');
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        return $this->db->get($this->tableTim)->result_array();
    }

    public function getDataRekapPenjurianByKategoriID($kategori_id)
    {
        $this->db->select("
            $this->tableTim.*,
            $this->tableKategori.nama AS kategori_nama, 
            (SELECT COUNT($this->tableAnggota.user_id) FROM $this->tableAnggota WHERE $this->tableAnggota.user_id = $this->tableTim.user_id) AS jumlah_tim,
            (
                IFNULL(
                    (
                        SELECT 
                            SUM(
                                ((20/100)*$this->tableNilai.ide) +
                                ((15/100)*$this->tableNilai.metode) +
                                ((40/100)*$this->tableNilai.manfaat)
                            )
                        FROM $this->tableNilai WHERE $this->tableNilai.tim_id = $this->tableTim.id
                    )
                    ,0
                )
            ) AS pm,
            (
                IFNULL(
                    (
                        SELECT 
                            SUM(
                                ((10/100)*$this->tableNilai.display) +
                                ((15/100)*$this->tableNilai.presentasi)
                            )
                        FROM $this->tableNilai WHERE $this->tableNilai.tim_id = $this->tableTim.id
                    )
                    ,0
                )
            ) AS gf,
        ");
        $this->db->order_by($this->tableTim.".id", "desc");
        $this->db->where([
            $this->tableUser.".event_id" => $this->session->userdata('event_id'),
            $this->tableTim.".kategori_id" => $kategori_id
        ]);
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableTim.'.user_id');
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        return $this->db->get($this->tableTim)->result_array();
    }

    public function getDataNilaiByTimID($tim_id)
    {
        $this->db->select("
            $this->tableJuri.name AS nama_juri,
            (
                IFNULL(
                    (
                        SELECT 
                            SUM(
                                ((20/100)*$this->tableNilai.ide) +
                                ((15/100)*$this->tableNilai.metode) +
                                ((40/100)*$this->tableNilai.manfaat)
                            )
                        FROM $this->tableNilai
                    )
                    ,0
                )
            ) AS pm,
            (
                IFNULL(
                    (
                        SELECT 
                            SUM(
                                ((10/100)*$this->tableNilai.display) +
                                ((15/100)*$this->tableNilai.presentasi)
                            )
                        FROM $this->tableNilai
                    )
                    ,0
                )
            ) AS gf,
        ");
        $this->db->join($this->tableJuri, $this->tableJuri.'.user_id = '.$this->tableNilai.'.juri_id');
        $this->db->order_by("$this->tableNilai.id", "desc");
        return $this->db->get_where($this->tableNilai, ['tim_id' => $tim_id])->result_array();
    }

    public function getDataTimByID($id)
    {
        $this->db->select("
            $this->tableTim.*,
            $this->tableKategori.nama AS kategori_nama, 
            $this->tableKategoriSub.nama AS kategori_sub_nama,
            (SELECT COUNT($this->tableAnggota.user_id) FROM $this->tableAnggota WHERE $this->tableAnggota.user_id = $this->tableTim.user_id) AS jumlah_tim
        ");
        $this->db->order_by($this->tableTim.".id", "desc");
        $this->db->where([
            $this->tableUser.".event_id" => $this->session->userdata('event_id'),
            $this->tableTim.".id" => $id
        ]);
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableTim.'.user_id');
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        $this->db->join($this->tableKategoriSub, $this->tableKategoriSub.'.id = '.$this->tableTim.'.kategori_sub_id');
        return $this->db->get($this->tableTim)->row_array();
    }

    public function getDataAnggotaByUserID($user_id)
    {
        $data = $this->db->query("
            SELECT * FROM $this->tableAnggota 
            JOIN $this->tableUser ON $this->tableUser.id = $this->tableAnggota.user_id 
            WHERE $this->tableUser.event_id = '".$this->session->userdata('event_id')."' 
                AND role_id = '$this->roleIdPeserta' 
                AND $this->tableAnggota.user_id = $user_id 
            ORDER BY FIELD(jabatan, 'ketua','pembimbing','anggota') 
        ");
        return $data->result_array();
    }

    public function getDataAnggota()
    {
        $data = $this->db->query("
            SELECT $this->tableAnggota.*, $this->tableTim.nama AS nama_tim,
                $this->tableKategori.nama AS nama_kategori, $this->tableKategoriSub.nama AS nama_kategori_sub 
            FROM $this->tableAnggota 
            JOIN $this->tableUser ON $this->tableUser.id = $this->tableAnggota.user_id 
            JOIN $this->tableTim ON $this->tableTim.user_id = $this->tableUser.id 
            JOIN $this->tableKategori ON $this->tableKategori.id = $this->tableTim.kategori_id 
            JOIN $this->tableKategoriSub ON $this->tableKategoriSub.id = $this->tableTim.kategori_sub_id 
            WHERE $this->tableUser.event_id = '".$this->session->userdata('event_id')."' 
                AND role_id = '$this->roleIdPeserta' 
            ORDER BY FIELD(jabatan, 'ketua','pembimbing','anggota') 
        ");
        return $data->result_array();
    }

    public function getDataAnggotaByKategoriID($kategori_id)
    {
        $data = $this->db->query("
            SELECT $this->tableAnggota.*, $this->tableTim.nama AS nama_tim,
                $this->tableKategori.nama AS nama_kategori, $this->tableKategoriSub.nama AS nama_kategori_sub 
            FROM $this->tableAnggota 
            JOIN $this->tableUser ON $this->tableUser.id = $this->tableAnggota.user_id 
            JOIN $this->tableTim ON $this->tableTim.user_id = $this->tableUser.id 
            JOIN $this->tableKategori ON $this->tableKategori.id = $this->tableTim.kategori_id 
            JOIN $this->tableKategoriSub ON $this->tableKategoriSub.id = $this->tableTim.kategori_sub_id 
            WHERE $this->tableUser.event_id = '".$this->session->userdata('event_id')."' 
                AND role_id = '$this->roleIdPeserta' 
                AND $this->tableKategori.id = $kategori_id
            ORDER BY FIELD(jabatan, 'ketua','pembimbing','anggota') 
        ");
        return $data->result_array();
    }

    public function getDataKategoriAll()
    {
        $this->db->order_by("id", "desc");
        $data = $this->db->get_where($this->tableKategori, ['event_id' => $this->session->userdata('event_id')]);
        $data = ($data->num_rows() > 0) ? $data->result_array() : [];
        return $data;
    }

    public function getDataKategoriByID($id)
    {
        return $this->db->get_where($this->tableKategori,['id' => $id]);
    }

    public function _uploadFileDinamis($nameFile='materi',$ext="docx|pdf|doc", $filename='', $path='./uploads/profile/')
    {
        if (!$filename) {
            $filename = get_current_user_id()."_".$nameFile;
        }
        $config = [];
        $config['upload_path']   = $path;
        $config['allowed_types'] = $ext;
        $config['file_name']     = $filename;
        $config['overwrite']     = true;
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($nameFile)){
            $data = array(
                'error' => TRUE,
                'data' => $this->upload->display_errors()
            );
        }else{
            $data = array(
                'error' => FALSE,
                'data' => $this->upload->data()
            );
        }
        return $data;
    }

    function check_valid_email_event($email)
    {
        return $this->db->get_where($this->tableUser, ['email' => $email, 'event_id' => $this->session->userdata('event_id')])->num_rows();
    }

    function check_valid_email_event_juri($email)
    {
        $this->db->join($this->tableUser, $this->tableUser.'.id = '.$this->tableJuri.'.user_id');
        $this->db->where(["$this->tableJuri.email" => $email, "$this->tableUser.event_id" => $this->session->userdata('event_id')]);
        return $this->db->get($this->tableJuri)->num_rows();
    }

    public function getTotalTimEachCategory()
    {
        $result = [];
        $this->db->select("
            $this->tableKategori.nama,
            (SELECT COUNT($this->tableTim.id) FROM $this->tableTim WHERE $this->tableTim.kategori_id = $this->tableKategori.id) AS jumlah_tim
        ");
        $this->db->order_by("nama", "asc");
        $this->db->where("event_id", $this->session->userdata('event_id'));
        $data = $this->db->get($this->tableKategori);
        $data = ($data->num_rows() > 1) ? $data->result_array() : [];
        foreach ($data as $key => $value) {
            $result['nama'][] = $value['nama'];
            $result['total'][] = $value['jumlah_tim'];
        }
        return $result;
    }

    public function getTotalTimEachSchool()
    {
        $result = [];
        $this->db->select("
            $this->tableKategoriSub.nama,
            $this->tableKategori.nama AS nama_kategori,
            (SELECT COUNT($this->tableTim.id) FROM $this->tableTim WHERE $this->tableTim.kategori_id = $this->tableKategori.id AND $this->tableTim.kategori_sub_id = $this->tableKategoriSub.id) AS jumlah_tim
        ");
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableKategoriSub.'.kategori_id');
        $this->db->order_by("$this->tableKategoriSub.nama", "asc");
        $this->db->where("$this->tableKategori.event_id", $this->session->userdata('event_id'));
        $data = $this->db->get($this->tableKategoriSub);
        $data = ($data->num_rows() > 1) ? $data->result_array() : [];
        return $data;
    }

    public function getTotalTimEachGender()
    {
        $result = [];
        $this->db->select("
            $this->tableKategori.nama,
            $this->tableAnggota.gender,
            SUM(case when $this->tableAnggota.gender = '1' then 1 else 0 end) as laki,
            SUM(case when $this->tableAnggota.gender = '2' then 1 else 0 end) as perempuan,
            count($this->tableAnggota.gender) as total
        ");
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableTim.'.kategori_id');
        $this->db->join($this->tableAnggota, $this->tableAnggota.'.user_id = '.$this->tableTim.'.user_id');
        $this->db->where("$this->tableKategori.event_id", $this->session->userdata('event_id'));
        $this->db->group_by([
            "$this->tableTim.kategori_id"
        ]);
        $this->db->order_by("$this->tableKategori.nama", "asc");
        $data = $this->db->get($this->tableTim)->result_array();
        return $data;
    }

    public function insertDataKategoriSub($data)
    {
        return $this->db->insert($this->tableKategoriSub, $data);
    }

    public function updateDataKategoriSub($data)
    {
        return $this->db->update($this->tableKategoriSub, $data);
    }

    public function insertDataKategori($data)
    {
        return $this->db->insert($this->tableKategori, $data);
    }

    public function updateDataKategori($data)
    {
        return $this->db->update($this->tableKategori, $data);
    }

    public function getDataKategoriSubByIDAll($id)
    {
        $this->db->select("$this->tableKategoriSub.*, $this->tableKategori.nama AS kategori_nama");
        $this->db->order_by("$this->tableKategoriSub.id", "desc");
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableKategoriSub.'.id');
        $data = $this->db->get_where($this->tableKategoriSub, ["$this->tableKategoriSub.kategori_id" => $id]);
        $data = ($data->num_rows() > 0) ? $data->result_array() : [];
        return $data;
    }

    public function getDataKategoriSubByID($id)
    {
        $this->db->select("$this->tableKategoriSub.*, $this->tableKategori.nama AS kategori_nama");
        $this->db->order_by("$this->tableKategoriSub.id", "desc");
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableKategoriSub.'.id');
        $data = $this->db->get_where($this->tableKategoriSub, ["$this->tableKategoriSub.id" => $id]);
        $data = ($data->num_rows() > 0) ? $data->row_array() : [];
        return $data;
    }

    public function getDataKategoriSubAll()
    {
        $this->db->select("$this->tableKategoriSub.*, $this->tableKategori.nama AS kategori_nama");
        $this->db->order_by("$this->tableKategoriSub.id", "desc");
        $this->db->join($this->tableKategori, $this->tableKategori.'.id = '.$this->tableKategoriSub.'.id');
        $data = $this->db->get_where($this->tableKategoriSub,['$this->tableKategori.event_id',$this->session->userdata('event_id')]);
        $data = ($data->num_rows() > 0) ? $data->result_array() : [];
        return $data;
    }
}
