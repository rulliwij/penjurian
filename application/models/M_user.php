<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model
{	
    function __construct(){
        parent::__construct();
        $this->tableUser = get_table('website_table_user');
        $this->tableRole = get_table('website_table_role');
    }

    // public function getDataUserAll($limit, $page)
    // {
    // 	$this->db->order_by("id", "desc");
    //     $this->db->limit($limit, $page);
    //     return $this->db->get($this->tableUser);
    // }

    public function getDataUserAll()
    {
    	$this->db->select("
            $this->tableUser.*,
            $this->tableRole.name AS role_name
        ");
        $this->db->order_by("$this->tableUser.id", "desc");
        $this->db->where(["$this->tableUser.id !=" => 1]);
        $this->db->join($this->tableRole, $this->tableUser.'.role_id = '.$this->tableRole.'.id');
        return $this->db->get($this->tableUser)->result_array();
    }

    public function getDataUserByID($id)
    {
        return $this->db->get_where($this->tableUser,['id' => $id])->row_array();
    }

    public function getDataRole()
    {
        return $this->db->get_where($this->tableRole,['id !=' => 1])->result_array();
    }
}