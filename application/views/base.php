<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title><?= ucwords($title) ?></title>
	
	<!-- Bootstrap -->
	<link href="<?php echo asset('vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo asset('vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo asset('vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
	<!-- jQueryUI -->
	<link href="<?php echo asset('build/css/jquery-ui.css') ?>" rel="stylesheet">
	<!-- SweetAlert -->
	<link href="<?php echo asset('build/css/sweetalert2.min.css') ?>" rel="stylesheet">
	
	<!-- Custom Theme Style -->
	<link href="<?php echo asset('build/css/custom.css') ?>" rel="stylesheet">
	<!-- <link href="<?php echo asset('build/css/custom.min.css') ?>" rel="stylesheet"> -->

	<!-- jQuery -->
	<script src="<?php echo asset('vendors/jquery/dist/jquery.min.js') ?>"></script>
	<!-- <script src="<?php //echo asset('build/js/jquery-1.12.4.js') ?>"></script> -->
	<!-- jQuery UI -->
	<script src="<?php echo asset('build/js/jquery-ui.js') ?>"></script>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<?php 
						$tableEvent = get_table('website_table_event');
						$event = getData($tableEvent, ['id' => $this->session->userdata('event_id')], TRUE);
						$website_title = ($this->session->userdata('event_id') == get_option('website_role_panitia',TRUE)) ? $event['judul'] : strtoupper(get_option('website_title')->option_value);
						?>
						<a href="#" class="site_title"><span class="d-block text-center"><?= $this->session->userdata('event_id') ? $website_title : 'Welcome' ?></span></a>
					</div>
					
					<div class="clearfix"></div>
					
					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="<?php echo asset('images/users.png') ?>" alt="Profile" class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Selamat Datang,</span>
							<h2><?= ucwords($this->session->userdata('nama')) ?></h2>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- /menu profile quick info -->
					
					<br />
					
					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<?php 
								$role_id = $this->session->userdata('role_id');
								?>
								<li><a href="<?= base_url(role_slug($role_id)) ?>"><i class="fa fa-home"></i> Dashboard</a></li>

								<!-- PESERTA -->
								<?php if ($role_id == get_option('website_role_peserta',TRUE)): ?>
									<li><a href="<?= base_url('peserta/profile') ?>"><i class="fa fa-check-circle"></i> Profile Tim</a></li>
									<li class="">
										<a>
											<i class="fa fa-users"></i> Anggota <span class="fa fa-chevron-down"></span>
										</a>
										<ul class="nav child_menu">
											<li><a href="<?= base_url('peserta/list_anggota') ?>">Lihat Semua Anggota</a></li>
											<li><a href="<?= base_url('peserta/create_anggota') ?>">Daftarkan Anggota</a></li>
										</ul>
									</li>
									<li><a href="<?= base_url('peserta/upload_materi_lomba') ?>"><i class="fa fa-upload"></i> Upload Materi Lomba</a></li>
									<li><a href="<?= base_url('peserta/list_pengumuman') ?>"><i class="fa fa-newspaper-o"></i> Pengumuman</a></li>
									<li><a href="<?= base_url('peserta/tutorial_lomba') ?>"><i class="fa fa-book"></i> Panduan</a></li>
									<li><a href="<?= base_url('peserta/survey') ?>"><i class="fa fa-book"></i> Survey</a></li>
								<?php endif ?>

								<!-- JURI -->
								<?php if ($role_id == get_option('website_role_juri',TRUE)): ?>
									<li><a href="<?= base_url('juri/edit_profile') ?>"><i class="fa fa-user"></i> Profile Juri</a></li>
									<li><a href="<?= base_url('juri/list_peserta_lomba') ?>"><i class="fa fa-users"></i> Peserta Lomba</a></li>
									<li><a href="<?= base_url('juri/survey') ?>"><i class="fa fa-book"></i> Survey</a></li>
								<?php endif ?>

								<!-- PANITIA -->
								<?php if ($role_id == get_option('website_role_panitia',TRUE)): ?>
									<li class="">
										<a>
											Event AIP <span class="fa fa-chevron-down"></span>
										</a>
										<ul class="nav child_menu">
											<li><a href="<?= base_url('panitia/list_event') ?>">Select Event</a></li>
											<li><a href="<?= base_url('panitia/create_event') ?>">Buat Event</a></li>
										</ul>
									</li>
									<li>
										<a href="<?= base_url('user/index') ?>">List User</a>
									</li>
									<!-- was selected event -->
									<?php if ($this->session->userdata('event_id')): ?>
										<li class="">
											<a>
												Data Peserta <span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu">
												<li><a href="<?= base_url('panitia/list_peserta') ?>">Lihat Tim</a></li>
												<li><a href="<?= base_url('panitia/statistik') ?>">Statistik</a></li>
												<li><a href="<?= base_url('panitia/sertifikat') ?>">Sertifikat</a></li>
											</ul>
										</li>
										<li class="">
											<a>
												Data Juri <span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu">
												<li><a href="<?= base_url('panitia/list_juri') ?>">Lihat Semua</a></li>
												<li><a href="<?= base_url('panitia/create_juri') ?>">Tambah Juri Baru</a></li>
											</ul>
										</li>
										<li><a href="<?= base_url('panitia/rekap_penjurian') ?>">Rekap Penjurian</a></li>
										<li class="">
											<a>
												Konfigurasi <span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu">
												<li>
													<a href="<?= base_url('panitia/set_time_frame') ?>">Set Time Frame</a>
												</li>
												<li class="">
													<a>
														Manajemen Kategori <span class="fa fa-chevron-down"></span>
													</a>
													<ul class="nav child_menu">
														<li><a href="<?= base_url('panitia/create_kategori') ?>">Buat Kategori</a></li>
														<li><a href="<?= base_url('panitia/kategori') ?>">Kategori</a></li>
														<li><a href="<?= base_url('panitia/sub_kategori') ?>">Instansi</a></li>
													</ul>
												</li>
												<li>
													<a href="<?= base_url('panitia/tutorial') ?>">Tutorial</a>
												</li>
												<li>
													<a href="<?= base_url('panitia/embed_survey_peserta') ?>">Embed Survey Peserta</a>
												</li>
												<li>
													<a href="<?= base_url('panitia/embed_survey_juri') ?>">Embed Survey Juri</a>
												</li>
												<li class="">
													<a>
														Pengumuman <span class="fa fa-chevron-down"></span>
													</a>
													<ul class="nav child_menu">
														<li><a href="<?= base_url('panitia/list_pengumuman') ?>">Lihat Semua Pengumuman</a></li>
														<li><a href="<?= base_url('panitia/create_pengumuman') ?>">Buat Pengumuman</a></li>
													</ul>
												</li>
											</ul>
										</li>
									<?php endif ?>
								<?php endif ?>

								<!-- ADMIN -->
								<?php if ($role_id == '1'): ?>
									<!-- <li><a><i class="fa fa-home"></i> Profile Juri</a></li>
									<li><a><i class="fa fa-users"></i> Lihat Semua Anggota</a></li>
									<li><a><i class="fa fa-graduation-cap"></i> Lihat Semua Juri</a></li>
									<li><a><i class="fa fa-user-plus"></i> Pendaftaran Juri</a></li>
									<li><a><i class="fa fa-newspaper-o"></i> Buat Pengumuman</a></li>
									<li><a><i class="fa fa-trophy"></i> Lihat Semua Karya</a></li> -->
								<?php endif ?>
								<li><a href="<?= base_url('umum/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
							</ul>
						</div>
						
					</div>
					<!-- /sidebar menu -->
					
				</div>
			</div>
			
			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<nav class="nav navbar-nav">
						<ul class="navbar-right">
							<li class="nav-item dropdown open" style="padding-left: 15px;">
								<a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
									<img src="<?php echo asset('images/users.png') ?>" alt=""><?= ucwords($this->session->userdata('nama')) ?>
								</a>
								<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
									<?php if ($role_id == get_option('website_role_juri',TRUE)): ?>
										<a class="dropdown-item" href="<?php echo base_url('juri/edit_profile') ?>"> Profile</a>
									<?php endif  ?>
									<a class="dropdown-item text-primary"  href="<?= ($this->session->userdata('role_id') == get_option('website_role_panitia',TRUE)) ? base_url('panitia/list_event') : 'javascript:;' ?>"><?= $event['judul'] ?? 'Pilih Event' ?></a>
									<a class="dropdown-item"  href="<?= base_url('umum/logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
								</div>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->
			
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<!-- <div class="page-title">
						<div class="title_left">
							<h3><?php echo $title ?? '' ?></h3>
						</div>
					</div> -->
					
					<div class="clearfix"></div>
					<?php 
					$this->load->view('dashboard/_partial/breadcrumb.php');
					echo $content;
					?>
				</div>
			</div>
			<!-- /page content -->
			
			<!-- footer content -->
			<footer>
				<div class="pull-right">
					@<?= ucwords(get_option('website_title')->option_value) ." ". date('Y') ?>
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>
	<!-- Bootstrap -->
	<script src="<?php echo asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
	<!-- FastClick -->
	<script src="<?php echo asset('vendors/fastclick/lib/fastclick.js') ?>"></script>
	<!-- NProgress -->
	<script src="<?php echo asset('vendors/nprogress/nprogress.js') ?>"></script>
	<!-- iCheck -->
	<script src="<?php echo asset('vendors/iCheck/icheck.min.js') ?>"></script>
	<!-- SweetAlert -->
	<script src="<?php echo asset('build/js/sweetalert2.min.js') ?>"></script>
	
	<!-- Custom Theme Scripts -->
	<script src="<?php echo asset('build/js/custom.min.js') ?>"></script>
	<script src="<?php echo asset('build/js/script.js') ?>"></script>
	<script type="text/javascript">
		$(function() {
			$(".datepickerJQuery").datepicker({ 
				dateFormat: 'yy-mm-dd',
				showButtonPanel: true,
			    changeMonth: true,
			    changeYear: true,
			    showOtherMonths: true,
			    selectOtherMonths: true,
			    yearRange: "-50:+5"
			});
		})
	</script>
</body>
</html>
