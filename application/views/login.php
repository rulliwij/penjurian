<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo ucwords(get_option('website_title')->option_value) ?> | <?php echo $title ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo asset('vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo asset('vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo asset('vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo asset('vendors/animate.css/animate.min.css') ?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo asset('build/css/custom.min.css') ?>" rel="stylesheet">
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form action="<?php echo base_url('login/aksi_login') ?>" method="POST">
                        <h1 class="text-uppercase">Login Form</h1>
                        <?php //echo validation_errors(); ?>
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                        <div>
                            <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo set_value('username'); ?>" />
                            <small><?php echo form_error('username'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" />
                            <small><?php echo form_error('password'); ?></small>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-primary" href="#"><i class="fa fa-check-circle"></i>&nbsp;Masuk</button>
                        </div>
                        <div class="separator">
                            <p class="change_link">Belum Mempunyai Akun ?
                                <a href="#signup" class="to_register"> Buat Akun </a>
                            </p>
                        </div>
                    </form>
                </section>
            </div>

            <div id="register" class="animate form registration_form">
                <section class="login_content">
                    <form method="POST" action="<?= base_url('login/register') ?>">
                        <?php if ($this->session->flashdata('error')): ?>
                            <div class="<?= $this->session->flashdata('status') ?>">
                                <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                &nbsp;<?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif ?>
                        <h1>Daftar sebagai peserta</h1>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username" required value="<?php echo set_value('username'); ?>" />
                            <small><?php echo form_error('username'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email" required value="<?php echo set_value('email'); ?>" />
                            <small><?php echo form_error('email'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required />
                            <small><?php echo form_error('password'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="conf_password" class="form-control" placeholder="Password" required />
                            <small><?php echo form_error('conf_password'); ?></small>
                        </div>
                        <div class="form-group text-left">
                            <button type="submit" class="btn btn-outline-success" href=""><i class="fa fa-check-circle"></i>&nbsp;Daftar</button>
                            <button type="reset" class="btn btn-outline-secondary" href="">Batal</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link">Sudah mempunyai akun ?
                                <a href="#signin" class="to_register"> Masuk </a>
                            </p>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>
</html>