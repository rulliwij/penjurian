<?php 
    $date = date('Y-m-d H:i:s');
    
    $submisi = $event['submisi'] ?? FALSE;
    $submisi = $submisi ? explode('_', $submisi) : [];
    $submisiStart = $submisi[0] ?? $date;
    $submisiEnd = $submisi[1] ?? $date;

    $bimbingan = $event['bimbingan'] ?? FALSE;
    $bimbingan = $bimbingan ? explode('_', $bimbingan) : [];
    $bimbinganStart = $bimbingan[0] ?? $date;
    $bimbinganEnd = $bimbingan[1] ?? $date;

    $penjurian_makalah = $event['penjurian_makalah'] ?? FALSE;
    $penjurian_makalah = $penjurian_makalah ? explode('_', $penjurian_makalah) : [];
    $penjurianMakalahStart = $penjurian_makalah[0] ?? $date;
    $penjurianMakalahEnd = $penjurian_makalah[1] ?? $date;

    $penjurian_grand_final = $event['penjurian_grand_final'] ?? FALSE;
    $penjurian_grand_final = $penjurian_grand_final ? explode('_', $penjurian_grand_final) : [];
    $penjurianGrandFinalStart = $penjurian_grand_final[0] ?? $date;
    $penjurianGrandFinalEnd = $penjurian_grand_final[1] ?? $date;
?>
<style type="text/css">
    .bootstrap-datetimepicker-widget {
        width: 60% !important;
    }
</style>
<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<script src="<?= asset('vendors/moment/moment.js') ?>"></script>
<script src="<?= asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>"></script>
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Pengaturan Set Time Frame Event </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form method="POST" action="<?= base_url('panitia/update_set_time_frame') ?>">
                    <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                    <?php if ($this->session->flashdata('status')): ?>
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif ?>
                    <div class="row">
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tbody>
                                        <tr>
                                            <td colspan="2" class="text-center">Informasi batasan untuk menentukan fase</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Mulai</td>
                                            <td>Tanggal Selesai</td>
                                        </tr>
                                        <tr>
                                            <td><?= $event['tanggal_mulai']." 00:00:00" ?? '' ?></td>
                                            <td><?= $event['tanggal_selesai']." 00:00:00" ?? '' ?></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-left">
                                <th>Fase</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td>Submisi</td>
                                    <td><input type="text" name="submisi_start" class="form-control datetimepicker" placeholder="tanggal dimulai" value="<?= set_value('submisi_start',$submisiStart) ?>" required></td>
                                    <td><input type="text" name="submisi_end" class="form-control datetimepicker" placeholder="tanggal selesai" value="<?= set_value('submisi_end',$submisiEnd) ?>" required></td>
                                </tr>
                                <tr>
                                    <td>Bimbingan</td>
                                    <td><input type="text" name="bimbingan_start" class="form-control datetimepicker" placeholder="tanggal dimulai" value="<?= set_value('bimbingan_start',$bimbinganStart) ?>" required></td>
                                    <td><input type="text" name="bimbingan_end" class="form-control datetimepicker" placeholder="tanggal selesai" value="<?= set_value('bimbingan_end',$bimbinganEnd) ?>" required></td>
                                </tr>
                                <tr>
                                    <td>Penjurian Makalah</td>
                                    <td><input type="text" name="penjurian_makalah_start" class="form-control datetimepicker" placeholder="tanggal dimulai" value="<?= set_value('penjurian_makalah_start',$penjurianMakalahStart) ?>" required></td>
                                    <td><input type="text" name="penjurian_makalah_end" class="form-control datetimepicker" placeholder="tanggal selesai" value="<?= set_value('penjurian_makalah_end',$penjurianMakalahEnd) ?>" required></td>
                                </tr>
                                <tr>
                                    <td>Penjurian Grand Final</td>
                                    <td><input type="text" name="penjurian_grand_final_start" class="form-control datetimepicker" placeholder="tanggal dimulai" value="<?= set_value('penjurian_grand_final_start',$penjurianGrandFinalStart) ?>" required></td>
                                    <td><input type="text" name="penjurian_grand_final_end" class="form-control datetimepicker" placeholder="tanggal selesai" value="<?= set_value('penjurian_grand_final_end',$penjurianGrandFinalEnd) ?>" required></td>
                                </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.datetimepicker').datetimepicker({
        defaultDate: new Date(),
        format: 'YYYY-MM-DD HH:mm:ss',
        sideBySide: true,
        widgetPositioning: {
              horizontal: 'auto',
              vertical: 'auto'
        },
    });
</script>