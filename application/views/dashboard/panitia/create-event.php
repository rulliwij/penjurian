<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Buat Event</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <a href="<?= base_url('panitia/list_event') ?>" class="btn btn-outline-secondary btn-sm"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
                    <hr>
                </div>
                <form method="POST" action="<?= base_url('panitia/store_event') ?>">
                    <div class="col-md-6 col-sm-6">
                        <?php if ($this->session->flashdata('status')): ?>
                            <div class="<?= $this->session->flashdata('status') ?>">
                                <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                &nbsp;<?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif ?>
                        <div class="form-group">
                            <label for="judul">Nama Event *</label>
                            <input type="text" name="judul" id="judul" class="form-control" autocomplete="off" placeholder="" required="required" title="judul" value="<?php echo set_value('judul'); ?>" />
                            <small><?php echo form_error('judul'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_mulai">Tanggal Mulai *</label>
                            <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control datepickerJQuery" autocomplete="off" placeholder="" required="required" title="Tanggal Mulai" value="<?php echo set_value('tanggal_mulai'); ?>" />
                            <small><?php echo form_error('tanggal_mulai'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_selesai">Tanggal Selesai *</label>
                            <input type="text" name="tanggal_selesai" id="tanggal_selesai" class="form-control datepickerJQuery" autocomplete="off" placeholder="" required="required" title="Tanggal Selesai" value="<?php echo set_value('tanggal_selesai'); ?>" />
                            <small><?php echo form_error('tanggal_selesai'); ?></small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="submit" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>