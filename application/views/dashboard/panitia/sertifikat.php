<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Anggota / Sertifikat</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-group">
                    <div class="col-md-4">
                        <select name="kategori" class="form-control select2" id="kategori">
                            <option value="">Pilih Kategori</option>
                            <?php foreach ($kategori as $key => $value): ?>
                                <option value="<?= $value['id'] ?>"><?= strtoupper($value['nama']) ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-outline-success" onclick="window.open('<?= base_url('panitia/export_sertifikat/') ?>'+$('#kategori').val())"><i class="fa fa-file-excel-o"></i>&nbsp;Export</button>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"></div>
                </div>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Nama Lengkap</th>
                            <th>Jabatan</th>
                            <th>Nama Tim</th>
                            <th>Sekolah/Institusi</th>
                            <th>Email</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($anggota): ?>
                            <?php foreach ($anggota as $key => $value): ?>
                                <tr class="text-left">
                                    <td class="text-center"><?= ($key+1) ?></td>
                                    <td><?= $value['name'] ?></td>
                                    <td><?= $value['jabatan'] ?></td>
                                    <td><?= $value['nama_tim'] ?></td>
                                    <td>
                                        <?= $value['nama_kategori'] ?><br>
                                        <?= $value['nama_kategori_sub'] ?>
                                    </td>
                                    <td><?= $value['email'] ?></td>
                                    <td><?= $value['phone'] ?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.select2').select2();
        $('.datatable').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    })
</script>