<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Edit Instansi</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form method="POST" action="<?= base_url('panitia/store_kategori') ?>">
                    <div class="col-md-12">
                        <a href="<?= base_url('panitia/kategori') ?>" class="btn btn-outline-secondary btn-sm"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
                        <hr>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <?php if ($this->session->flashdata('status')): ?>
                            <div class="<?= $this->session->flashdata('status') ?>">
                                <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                &nbsp;<?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif ?>
                        <div class="form-group">
                            <label for="nama">Nama Kategori *</label>
                            <input type="text" name="nama" id="nama" class="form-control" autocomplete="off" placeholder="" required="required" title="Nama" value="<?php echo set_value('nama'); ?>" />
                            <small><?php echo form_error('nama'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="kategori_id">Kategori</label>
                            <select name="kategori_id" class="form-control select2" id="kategori_id">
                                <option value="">Pilih Kategori</option>
                                <?php foreach ($kategori as $key => $value): ?>
                                    <option value="<?= $value['id'] ?>" <?php echo (set_value('kategori_id') == $value['id']) ? 'selected' : ''; ?>><?= strtoupper($value['nama']) ?></option>
                                <?php endforeach ?>
                            </select>
                            <small><?php echo form_error('kategori_id'); ?></small>
                            <small class="text-help text-danger">Kosongkan jika ingin membuat kategori, isi jika ingin membuat instansi</small>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi *</label>
                            <textarea name="deskripsi" id="deskripsi" class="form-control" autocomplete="off" placeholder="" title="Deskripsi" /><?php echo set_value('deskripsi'); ?></textarea>
                            <small><?php echo form_error('deskripsi'); ?></small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.select2').select2();
    })
</script>