<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Juri</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <a href="<?= base_url('panitia/create_juri') ?>" class="btn btn-outline-success btn-sm"><i class="fa fa-plus-circle"></i>&nbsp;Tambah</a>
                    <hr>
                </div>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <div class="col-md-12" id="response-message"></div>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Nama Juri</th>
                            <th>Kategori</th>
                            <th>Jumlah Submisi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($juri): ?>
                            <?php foreach ($juri as $key => $value): ?>
                            <?php $total = 0; $totalDinilai = 0; ?>
                                <tr class="text-left">
                                    <td><?= ($key+1) ?></td>
                                    <td><?= ucwords($value['name']) ?></td>
                                    <td>
                                        <?php foreach ($value['kategori'] as $key2 => $value2): ?>
                                            <?= ucwords($value2['nama'])."(".$value2['total_dinilai']."/".$value2['total'].")<br>" ?>
                                            <?php 
                                                $total += $value2['total'];
                                                $totalDinilai += $value2['total_dinilai'];
                                            ?>
                                        <?php endforeach ?>
                                    </td>
                                    <td><?= $totalDinilai."/".$total ?></td>
                                    <td>
                                        <a href="javascript:;" class="detailJuri" key="<?= $value['id'] ?>">Lihat |</a>
                                        <a href="<?= base_url('panitia/edit_juri/'.$value['id']) ?>">Edit |</a>
                                        <a href="#" onclick="return delete_juri(<?= $value['id'] ?>);">Hapus</a></a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <?php $this->load->view('dashboard/panitia/modal/detail-juri'); ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
    $(function() {
        $('.datatable').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
        $(".datatable").on('click','.detailJuri',function(event) {
            var id = $(this).attr('key');
            if (id) {
                $.ajax({
                    url : "<?php echo base_url('panitia/get_detail_juri'); ?>",
                    data:{id : id},
                    method:'GET',
                    dataType:'json',
                    success:function(response) {
                        if (response.error.code != 200) {
                            $('.modal-title').html(response.message);
                            $('.modal-body').html(response.error.message);
                            $('#modalDetailJuri').modal({backdrop: 'static', keyboard: true, show: true});
                        }else{
                            $('.modal-title').html(response.data.title);
                            $('.modal-body').html(response.data.content);
                            $('#modalDetailJuri').modal({backdrop: 'static', keyboard: true, show: true});
                        }
                    }
                });
            }
        });
    })

    function delete_juri(id) {
        if (confirm("Apakah anda akan menghapus data juri?")) {
            $.ajax({
                url : "<?php echo base_url('panitia/delete_juri'); ?>",
                data:{id : id},
                method:'POST',
                dataType:'json',
                success:function(response) {
                    if (response.error.code != 200) {
                        $('#response-message').addClass('alert alert-danger');
                        $('#response-message').html(response.error.message);
                    }else{
                        $('#response-message').addClass('alert alert-success');
                        $('#response-message').html(response.error.message);
                    }
                    setTimeout(function() {
                        location.reload();
                    }, 5000);
                }
            });
        }
    }
</script>