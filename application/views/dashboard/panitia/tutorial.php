<style>
.ck-editor__editable_inline {
    min-height: 400px;
}
</style>
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tutorial</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form method="POST" action="<?= base_url('panitia/update_tutorial') ?>">
                    <div class="col-md-12">
                        <?php if ($this->session->flashdata('status')): ?>
                            <div class="<?= $this->session->flashdata('status') ?>">
                                <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                &nbsp;<?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif ?>
                        <div class="form-group">
                            <label for="tutorial">Tutorial *</label>
                            <textarea name="tutorial" class="form-control"><?= $event['tutorial'] ?></textarea>
                            <small><?php echo form_error('tutorial'); ?></small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/ckeditor5/ckeditor.js') ?>"></script>
<script type="text/javascript">
    ClassicEditor
    .create( document.querySelector( 'textarea' ) )
    .catch( error => {
        console.error( error );
    } );
</script>