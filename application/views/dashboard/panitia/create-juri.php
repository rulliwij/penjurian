<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tambah Juri</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form method="POST" action="<?= base_url('panitia/store_juri') ?>" enctype="multipart/form-data">
                    <div class="col-md-6 col-sm-6">
                        <div class="x_panel">
                            <div class="x_content">
                                <?php if ($this->session->flashdata('status')): ?>
                                    <div class="<?= $this->session->flashdata('status') ?>">
                                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label for="username">Username *</label>
                                    <input type="text" name="username" id="username" class="form-control" autocomplete="off" placeholder="" required="required" title="Username" value="<?php echo set_value('username'); ?>" />
                                    <small><?php echo form_error('username'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="jss">JSS ID *</label>
                                    <input type="text" name="jss" id="jss" class="form-control" autocomplete="off" placeholder="" required="required" title="jss" value="<?php echo set_value('jss'); ?>" />
                                    <small><?php echo form_error('jss'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email *</label>
                                    <input type="text" name="email" id="email" class="form-control" autocomplete="off" placeholder="" required="required" title="Email" value="<?php echo set_value('email'); ?>" />
                                    <small><?php echo form_error('email'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password *</label>
                                    <input type="password" name="password" id="password" class="form-control" autocomplete="off" placeholder="" required="required" title="Password" value=""/>
                                    <small><?php echo form_error('password'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="conf_password">Konfirmasi Password *</label>
                                    <input type="password" name="conf_password" id="conf_password" class="form-control" autocomplete="off" placeholder="" required="required" title="Konfirmasi Password" value="" />
                                    <small><?php echo form_error('conf_password'); ?></small>
                                </div>
                                <label for="penelitian_mahasiswa">Kategori *</label>
                                <?php foreach ($kategori as $key => $value): ?>
                                    <div class="form-group">
                                        <input type="checkbox" name="kategori[]" id="kategori<?= $key ?>" autocomplete="off" title="<?= $value['nama'] ?>" value="<?= $value['id'] ?>" <?= in_array($value['id'], set_value('kategori',[])) ? 'checked' : '' ?> />
                                        <label for="kategori<?= $key ?>"><?= $value['nama'] ?></label>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="form-group">
                                    <label for="name">Nama Lengkap *</label>
                                    <input type="text" name="name" id="name" class="form-control" autocomplete="off" placeholder="" required="required" title="Nama Juri" value="<?php echo set_value('name'); ?>" />
                                    <small><?php echo form_error('name'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="jabatan">Jabatan *</label>
                                    <input type="text" name="jabatan" id="jabatan" class="form-control" autocomplete="off" placeholder="" required="required" title="Jabatan" value="<?php echo set_value('jabatan'); ?>" />
                                    <small><?php echo form_error('jabatan'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="organisasi">Organisasi *</label>
                                    <input type="text" name="organisasi" id="organisasi" class="form-control" autocomplete="off" placeholder="" required="required" title="Organisasi" value="<?php echo set_value('organisasi'); ?>" />
                                    <small><?php echo form_error('organisasi'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="no_rek">Nomor Rekening *</label>
                                    <input type="number" name="no_rek" id="no_rek" class="form-control" autocomplete="off" placeholder="" required="required" title="Nomor Rekening" value="<?php echo set_value('no_rek'); ?>" />
                                    <small><?php echo form_error('no_rek'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="bank">Bank *</label>
                                    <input type="text" name="bank" id="bank" class="form-control" autocomplete="off" placeholder="" required="required" title="Bank" value="<?php echo set_value('bank'); ?>" />
                                    <small><?php echo form_error('bank'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="npwp">NPWP *</label>
                                    <input type="number" name="npwp" id="npwp" class="form-control" autocomplete="off" placeholder="" required="required" title="NPWP" value="<?php echo set_value('npwp'); ?>" />
                                    <small><?php echo form_error('npwp'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="place_of_birth">Tempat Lahir *</label>
                                    <input type="text" name="place_of_birth" id="place_of_birth" class="form-control" autocomplete="off" placeholder="" required="required" title="Tempat Lahir" value="<?php echo set_value('place_of_birth'); ?>" />
                                    <small><?php echo form_error('place_of_birth'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="date_of_birth">Tanggal Lahir *</label>
                                    <input type="text" name="date_of_birth" id="date_of_birth" class="form-control datepickerJQuery" autocomplete="off" placeholder="" required="required" title="Tanggal Lahir" value="<?php echo set_value('date_of_birth'); ?>" />
                                    <small><?php echo form_error('date_of_birth'); ?></small>
                                </div>
                                <div class="form-group">
                                    <input type="radio" name="gender" id="male" value="1" checked="" required />
                                    <label for="male">Laki-laki</label>
                                    <div class="clearfix"></div>
                                    <input type="radio" name="gender" id="female" value="2" />
                                    <label for="female">Perempuan</label>
                                </div>
                                <div class="form-group">
                                    <label for="country">Negara *</label>
                                    <input type="text" name="country" id="country" class="form-control" autocomplete="off" placeholder="" required="required" title="Negara" value="<?php echo set_value('country','ID'); ?>" />
                                    <small><?php echo form_error('country'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Nomor HP *</label>
                                    <input type="text" name="phone" id="phone" class="form-control" autocomplete="off" placeholder="" required="required" title="Nomor HP" value="<?php echo set_value('phone'); ?>" />
                                    <small><?php echo form_error('phone'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="email_publik">Email Publik *</label>
                                    <input type="email" name="email_publik" id="email_publik" class="form-control" autocomplete="off" placeholder="" required="required" title="Email Publik" value="<?php echo set_value('email_publik'); ?>" />
                                    <small><?php echo form_error('email_publik'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="foto">Foto *</label>
                                    <input type="file" name="foto" id="foto" class="form-control-file <?php echo form_error('foto') ? 'is-invalid':'' ?>" accept="image/*" />
                                    <small><?php echo form_error('foto'); ?></small>
                                    <img id="showFoto" src="" class="img-responsive img-thumbnail w-25" style="display: 'none'; ?>;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="x_content text-right">
                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#showFoto').attr('src', e.target.result);
          $('#showFoto').show();
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#foto").change(function() {
        readURL(this);
    });
</script>