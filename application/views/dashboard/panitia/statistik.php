<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Statistik</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Sebaran Jumlah Tim Setiap Kategori
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <canvas id="chartCategory"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Statistik Jumlah Submisi per Sekolah
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <table class="table table-bordered datatable">
                                    <thead>
                                        <tr class="text-left">
                                            <th class="text-center">#</th>
                                            <th>Instansi</th>
                                            <th>Jumlah Tim</th>
                                            <th>Kategori</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($total_each_school): ?>
                                            <?php foreach ($total_each_school as $key => $value): ?>
                                                <tr>
                                                    <td><?= ($key+1) ?></td>
                                                    <td><?= strtoupper($value['nama']) ?></td>
                                                    <td><?= strtoupper($value['jumlah_tim']) ?></td>
                                                    <td><?= strtoupper($value['nama_kategori']) ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                            <tr>
                                                <td>Maaf, Tidak ada data instansi untuk ditampilkan</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Jenis kelamin (Laki VS Perempuan)
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <?php if ($total_each_gender): ?>
                                    <?php foreach ($total_each_gender as $key => $value): ?>
                                        <div class="col-md-3">
                                            <canvas id="gender<?= $key ?>" width="150"></canvas>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?php echo asset('vendors/Chart.js/dist/Chart.bundle.min.js') ?>"></script>
<script type="text/javascript">
    $(function() {
        $('.datatable').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    })

    // BAR
    let totalEachCategory = <?= json_encode([$total_each_category]) ?>;
    if (!$.isEmptyObject(totalEachCategory)) {
        var densityCanvas = document.getElementById("chartCategory");
        Chart.defaults.global.defaultFontFamily = "Lato";
        Chart.defaults.global.defaultFontSize = 18;
        var densityData = {
            label: 'Sebaran Jumlah Tim Setiap Kategori',
            data: [<?php echo '"'.implode('","', $total_each_category['total']).'"' ?>]
        };
        var chartOptions = {
            scales: {
                yAxes: [{
                    barPercentage: 0.5
                }]
            },
            elements: {
                rectangle: {
                    borderSkipped: 'left',
                }
            }
        };
        var barChart = new Chart(densityCanvas, {
            type: 'horizontalBar',
            data: {
                labels: [<?php echo '"'.implode('","', $total_each_category['nama']).'"' ?>],
                datasets: [densityData]
            },
            options: chartOptions
        });
    }

    // PIE
    let totalEachGender = <?= json_encode([$total_each_gender]) ?>;
    if (!$.isEmptyObject(totalEachGender)) {
        let oilCanvas
        let oilData
        let Laki,perempuan
        let dataGender = '<?= json_encode($total_each_gender) ?>'
        var JSONObject = JSON.parse(dataGender);

        for (var i = 0; i < JSONObject.length; i++) {
            oilCanvas = document.getElementById("gender"+i)
            laki = JSONObject[i].laki
            perempuan = JSONObject[i].perempuan
            Chart.defaults.global.defaultFontFamily = "Lato"
            Chart.defaults.global.defaultFontSize = 15
            oilData = {
                labels: [
                    `Laki-laki (${JSONObject[i].laki})`,
                    `Perempuan (${JSONObject[i].perempuan})`
                ],
                datasets: [
                    {
                        data: [laki, perempuan],
                        backgroundColor: [
                            "#FF6384",
                            "#63FF84"
                        ]
                    }]
            };
            var pieChart = new Chart(oilCanvas, {
              type: 'pie',
              data: oilData,
              options: {
                title: {
                    display: true,
                    text: JSONObject[i].nama
                },
                responsive: true,
                maintainAspectRatio: true
              }
            });
        }
    }
</script>