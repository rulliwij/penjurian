<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Event</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <a href="<?= base_url('panitia/create_event') ?>" class="btn btn-outline-success btn-sm"><i class="fa fa-plus-circle"></i>&nbsp;Tambah</a>
                    <hr/>
                </div>
                <div class="clearfix"></div>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="col-md-12">
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                    </div>
                <?php endif ?>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Nama Event</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($event): ?>
                            <?php foreach ($event as $key => $value): ?>
                                <tr>
                                    <td class="text-center"><?= ($key+1) ?></td>
                                    <td><?= $value['judul'] ?></td>
                                    <td><?= date('Y F d', strtotime($value['tanggal_mulai'])) ?></td>
                                    <td><?= date('Y F d', strtotime($value['tanggal_selesai'])) ?></td>
                                    <td>
                                        <a href="<?= base_url('panitia/choice_event/'.$value['id']); ?>"><u>Kelola</u></a> |
                                        <a href="<?= base_url('panitia/edit_event/'.$value['id']); ?>"><u>Edit</u></a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
    $('.datatable').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "order": [[ 2, "asc" ]]
    });
</script>