<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<style>
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
  }

  .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
  }

  .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
  }

  .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
  }

  input:checked + .slider {
      background-color: #2196F3;
  }

  input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
  }

  input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
  }

  /* Rounded sliders */
  .slider.round {
      border-radius: 34px;
  }

  .slider.round:before {
      border-radius: 50%;
  }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Buat Pengumuman</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <a href="<?= base_url('panitia/list_pengumuman') ?>" class="btn btn-outline-secondary btn-sm"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
                    <hr>
                </div>
                <form method="POST" action="<?= base_url('panitia/store_pengumuman') ?>">
                    <div class="col-md-6 col-sm-6">
                        <?php if ($this->session->flashdata('status')): ?>
                            <div class="<?= $this->session->flashdata('status') ?>">
                                <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                &nbsp;<?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif ?>
                        <div class="form-group">
                            <label for="title">Judul *</label>
                            <input type="text" name="title" id="title" class="form-control" autocomplete="off" placeholder="" required="required" title="Judul" value="<?php echo set_value('title'); ?>" />
                            <small><?php echo form_error('title'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="content">Isi *</label>
                            <textarea name="content" class="form-control" rows="5" required><?= set_value('content') ?></textarea>
                        </div>
                        <div class="form-group">
                            <label class="switch">
                                <input type="checkbox" name="status" id="status" autocomplete="off" placeholder="" title="Status" <?php echo set_value('status') ? 'checked' : ''; ?> />
                                <span class="slider round"></span>
                            </label>
                            <small><?php echo form_error('title'); ?></small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="submit" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>