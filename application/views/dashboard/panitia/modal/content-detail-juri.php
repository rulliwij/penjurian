<?php $total = 0; ?>
<table class="table table-bordered">
    <tr>
        <td class="w-25">Nama</td>
        <td><?= $juri['name'] ?></td>
    </tr>
    <tr>
        <td>Kategori</td>
        <td>
            <?php foreach ($juri['kategori'] as $key => $value): ?>
                <?= ucwords($value['nama'])."(".$value['total']."), " ?>
                <?php 
                    $total += $value['total'];
                ?>
            <?php endforeach ?>
        </td>
    </tr>
    <tr>
        <td>Jumlah Submisi</td>
        <td><?= $total ?></td>
    </tr>
    <tr>
        <td>Jabatan</td>
        <td><?= $juri['jabatan'] ?></td>
    </tr>
    <tr>
        <td>Organisasi</td>
        <td><?= $juri['organisasi'] ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?= $juri['email'] ?></td>
    </tr>
    <tr>
        <td>No HP</td>
        <td><?= $juri['phone'] ?></td>
    </tr>
    <tr>
        <td>Nomor Rekening</td>
        <td><?= $juri['no_rek'] ?></td>
    </tr>
    <tr>
        <td>Bank</td>
        <td><?= $juri['bank'] ?></td>
    </tr>
    <tr>
        <td>NPWP</td>
        <td><?= $juri['npwp'] ?></td>
    </tr>
</table>