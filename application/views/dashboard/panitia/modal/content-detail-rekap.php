<table class="table table-bordered">
    <thead>
        <tr class="text-left">
            <th>#</th>
            <th>Nama Juri</th>
            <th>Nilai PM (75%)</th>
            <th>Nilai GF (25%)</th>
            <th>Nilai Total (100%)</th>
        </tr>
    </thead>
    <tbody>
        <?php if ($nilai): ?>
            <?php foreach ($nilai as $key => $value): ?>
                <tr>
                    <td><?= ($key+1) ?></td>
                    <td><?= $value['nama_juri'] ?></td>
                    <td><?= $value['pm'] ?></td>
                    <td><?= $value['gf'] ?></td>
                    <td><?= ($value['pm']+$value['gf']) ?></td>
                </tr>
            <?php endforeach ?>
        <?php else: ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php endif ?>
    </tbody>
</table>