<div>
    <h4>Informasi Penelitian/Inovasi</h4>
    <table class="table table-bordered w-50">
        <tr>
            <td>Judul</td>
            <td><?= $tim['inovasi'] ?></td>
        </tr>
        <tr>
            <td>Abstract</td>
            <td><?= $tim['abstract'] ?></td>
        </tr>
    </table>
</div>
<div class="mb-3">
    <h4>Informasi Makalah/Lampiran</h4>
    <?php if ($team_makalah_3): ?>
        <a href="<?= get_permalink_materi($team_makalah_3) ?>" target="blank">[link download makalah]</a>
    <?php else: ?>
        <?php if ($team_makalah_2): ?>
            <a href="<?= get_permalink_materi($team_makalah_2) ?>" target="blank">[link download makalah]</a>
        <?php else: ?>
            <?php if ($team_makalah_1): ?>
                <a href="<?= get_permalink_materi($team_makalah_1) ?>" target="blank">[link download makalah]</a>
            <?php endif ?>
        <?php endif ?>
    <?php endif ?>
    <?php if ($team_lampiran_3): ?>
        <a href="<?= get_permalink_materi($team_lampiran_3) ?>" target="blank">[link download lampiran]</a>
    <?php else: ?>
        <?php if ($team_lampiran_2): ?>
            <a href="<?= get_permalink_materi($team_lampiran_2) ?>" target="blank">[link download lampiran]</a>
        <?php else: ?>
            <?php if ($team_lampiran_1): ?>
                <a href="<?= get_permalink_materi($team_lampiran_1) ?>" target="blank">[link download lampiran]</a>
            <?php endif ?>
        <?php endif ?>
    <?php endif ?>
</div>
<div>
    <h4>Informasi Tim</h4>
    <table class="table table-bordered w-50">
        <tr>
            <td>Nama</td>
            <td><?= $tim['nama'] ?></td>
        </tr>
        <tr>
            <td>Kategori</td>
            <td><?= $tim['kategori_nama'] ?></td>
        </tr>
        <tr>
            <td>Instansi</td>
            <td><?= $tim['kategori_sub_nama'] ?></td>
        </tr>
    </table>
</div>
<div>
    <h4>Informasi Anggota Tim</h4>
    <table class="table table-bordered w-50">
        <tr>
            <td>#</td>
            <td>Nama</td>
            <td>Jabatan</td>
        </tr>
        <?php if ($data_anggota): ?>
            <?php foreach ($data_anggota as $key => $value): ?>
                <tr>
                    <td><?= ($key+1) ?></td>
                    <td><?= $value['name'] ?></td>
                    <td><?= $value['jabatan'] ?></td>
                </tr>
            <?php endforeach ?>
        <?php else: ?>
            <tr>
                <td colspan="3">Maaf, Tidak ada anggota pada tim ini</td>
            </tr>
        <?php endif ?>
    </table>
</div>
<br><br>