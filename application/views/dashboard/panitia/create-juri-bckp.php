<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tambah Juri</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form method="POST" action="<?= base_url('peserta/store_anggota') ?>">
                    <div class="col-md-12 col-sm-12">
                        <div class="x_content text-right">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="submit" class="btn btn-default border-dark btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="x_panel">
                            <div class="x_content">
                                <?php if ($this->session->flashdata('status')): ?>
                                    <div class="<?= $this->session->flashdata('status') ?>">
                                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label for="username">Username *</label>
                                    <input type="text" name="username" id="username" class="form-control" autocomplete="off" placeholder="" required="required" title="Username" value="<?php echo set_value('username'); ?>" />
                                    <small><?php echo form_error('username'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email *</label>
                                    <input type="text" name="email" id="email" class="form-control" autocomplete="off" placeholder="" required="required" title="Email" value="<?php echo set_value('email'); ?>" />
                                    <small><?php echo form_error('email'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password *</label>
                                    <input type="password" name="password" id="password" class="form-control" autocomplete="off" placeholder="" required="required" title="Password" value=""/>
                                    <small><?php echo form_error('password'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="conf_password">Konfirmasi Password *</label>
                                    <input type="password" name="conf_password" id="conf_password" class="form-control" autocomplete="off" placeholder="" required="required" title="Konfirmasi Password" value="" />
                                    <small><?php echo form_error('conf_password'); ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="x_panel">
                            <div class="x_content">
                                <?php if ($this->session->flashdata('status')): ?>
                                    <div class="<?= $this->session->flashdata('status') ?>">
                                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label for="name">Nama *</label>
                                    <input type="text" name="name" id="name" class="form-control" autocomplete="off" placeholder="" required="required" title="Nama Anggota" value="<?php echo set_value('name'); ?>" />
                                    <small><?php echo form_error('name'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="place_of_birth">Tempat Lahir *</label>
                                    <input type="text" name="place_of_birth" id="place_of_birth" class="form-control" autocomplete="off" placeholder="" required="required" title="Tempat Lahir" value="<?php echo set_value('place_of_birth'); ?>" />
                                    <small><?php echo form_error('place_of_birth'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="date_of_birth">Tanggal Lahir *</label>
                                    <input type="text" name="date_of_birth" id="date_of_birth" class="form-control datepickerJQuery" autocomplete="off" placeholder="" required="required" title="Tanggal Lahir" value="<?php echo set_value('date_of_birth'); ?>" />
                                    <small><?php echo form_error('date_of_birth'); ?></small>
                                </div>
                                <div class="form-group">
                                    <input type="radio" name="gender" id="male" value="1" checked="" required />
                                    <label for="male">Laki-laki</label>
                                    <div class="clearfix"></div>
                                    <input type="radio" name="gender" id="female" value="2" />
                                    <label for="female">Perempuan</label>
                                </div>
                                <div class="form-group">
                                    <label for="country">Negara *</label>
                                    <input type="text" name="country" id="country" class="form-control" autocomplete="off" placeholder="" required="required" title="Negara" value="<?php echo set_value('country'); ?>" />
                                    <small><?php echo form_error('country'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Nomor HP *</label>
                                    <input type="text" name="phone" id="phone" class="form-control" autocomplete="off" placeholder="" required="required" title="Nomor HP" value="<?php echo set_value('phone'); ?>" />
                                    <small><?php echo form_error('phone'); ?></small>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email *</label>
                                    <input type="email" name="email" id="email" class="form-control" autocomplete="off" placeholder="" required="required" title="Email" value="<?php echo set_value('email'); ?>" />
                                    <small><?php echo form_error('email'); ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>