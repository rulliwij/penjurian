<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pengumuman</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <a href="<?= base_url('panitia/create_pengumuman') ?>" class="btn btn-outline-success btn-sm"><i class="fa fa-plus-circle"></i>&nbsp;Tambah</a>
                    <hr>
                </div>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th>Tanggal</th>
                            <th>Manage</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($pengumuman): ?>
                            <?php foreach ($pengumuman as $key => $value): ?>
                                <tr class="text-left">
                                    <td class="text-center"><?= ($key+1) ?></td>
                                    <td><?= $value['title'] ?></td>
                                    <td><?= $value['content'] ?></td>
                                    <td><?= $value['created_at'] ?></td>
                                    <td>
                                        <a href="<?= base_url('panitia/edit_pengumuman/'.$value['id']) ?>">Manage</a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
    $(function() {
        $('.datatable').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    })
</script>