<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Rekap Penjurian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <div class="form-group row">
                    <div class="col-md-4">
                        <select name="kategori" class="form-control select2" id="kategori" required>
                            <option value="">Pilih Kategori</option>
                            <?php foreach ($kategori as $key => $value): ?>
                                <option value="<?= $value['id'] ?>"><?= strtoupper($value['nama']) ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-outline-success" onclick="window.open('<?= base_url('panitia/export_rekap/') ?>'+$('#kategori').val())"><i class="fa fa-file-excel-o"></i>&nbsp;Export</button>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"></div>
                </div>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Kode Tim</th>
                            <th>Nama Tim</th>
                            <th>Kategori</th>
                            <th>Avg Nilai PM (75%)</th>
                            <th>Avg Nilai GF (25%)</th>
                            <th>Avg Nilai Total (100%)</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($tim): ?>
                            <?php foreach ($tim as $key => $value): ?>
                                <tr>
                                    <td><?= ($key+1) ?></td>
                                    <td><?= $value['kode'] ?></td>
                                    <td><?= $value['nama'] ?></td>
                                    <td><?= $value['kategori_nama'] ?></td>
                                    <td><?= $value['pm'] ?></td>
                                    <td><?= $value['gf'] ?></td>
                                    <td><?= ($value['pm']+$value['gf']) ?></td>
                                    <td>
                                        <a href="javascript:;" class="detailRekap" key="<?= $value['id'] ?>">Lihat</a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <?php $this->load->view('dashboard/panitia/modal/detail-rekap'); ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.select2').select2();
        $('.datatable').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
        $(".datatable").on('click','.detailRekap',function(event) {
            var id = $(this).attr('key');
            if (id) {
                $.ajax({
                    url : "<?php echo base_url('panitia/get_detail_rekap'); ?>",
                    data:{id : id},
                    method:'GET',
                    dataType:'json',
                    success:function(response) {
                        if (response.error.code != 200) {
                            $('.modal-title').html(response.message);
                            $('.modal-body').html(response.error.message);
                            $('#modalDetailRekap').modal({backdrop: 'static', keyboard: true, show: true});
                        }else{
                            $('.modal-title').html(response.data.title);
                            $('.modal-body').html(response.data.content);
                            $('#modalDetailRekap').modal({backdrop: 'static', keyboard: true, show: true});
                        }
                    }
                });
            }
        });
    })
</script>