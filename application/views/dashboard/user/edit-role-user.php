<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $title; ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            	<div class="col-md-6 col-sm-6">
            		<div class="x_panel">
            			<form method="POST" action="<?= base_url('user/update_role/'.$user['id']) ?>" enctype="multipart/form-data">
            				<?php if ($this->session->flashdata('status')): ?>
            					<div class="<?= $this->session->flashdata('status') ?>">
            						<i class="<?= $this->session->flashdata('icon') ?>"></i>
            						&nbsp;<?php echo $this->session->flashdata('message'); ?>
            					</div>
            				<?php endif ?>
            				<div class="form-group">
            					<label for="username" style="font-weight:bold;">Username </label>
            					<input type="text" name="username" id="username" class="form-control" autocomplete="off" placeholder="" required="required" title="user_name" value="<?php echo set_value('username',$user['username']); ?>" readonly />
            					<small><?php echo form_error('username'); ?></small>
            				</div>
            				<label for="role" style="font-weight:bold;">Role *</label>
                                <?php foreach ($role as $key => $value): 
                                	$checked = '';
                                	if ($user['role_id'] == $value['id']) {
                                		$checked = 'checked = checked';
                                	}
                                	?>
                                    <div class="form-group">
                                        <input type="radio" name="role[]" id="role<?= $key ?>" autocomplete="off" title="<?= $value['name'] ?>" value="<?= $value['id'] ?>" <?= $checked; ?>/>
                                        <label for="role<?= $key ?>"><?= $value['name'] ?></label>
                                    </div>
                                <?php endforeach ?>
                            <div class="col-md-12 col-sm-12">
		                        <div class="x_content text-right">
		                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
		                            <button type="reset" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
		                        </div>
		                    </div>
            			</form>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>
