<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Manajemen Role User</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="clearfix"></div>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="col-md-12">
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                    </div>
                <?php endif ?>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th>Nama</th>
                            <th>Role</th>
                            <th>Email</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($user): 
                            $no = 1;
                            ?>
                            <?php foreach ($user as $key => $value): ?>
                                <tr>
                                    <td><?= $value['username'] ?></td>
                                    <td><?= $value['role_name'] ?></td>
                                    <td><?= $value['email'] ?></td>
                                    <td>
                                        <a href="<?php echo base_url('user/choice_role/'.$value['id']); ?>" id="btn-kelola"><u>Kelola</u></a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
    $('.datatable').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "order": [[ 2, "asc" ]]
    });
</script>