<?php 
    $date = date('Y-m-d H:i:s');
    
    $submisi = $event['submisi'] ?? FALSE;
    $submisi = $submisi ? explode('_', $submisi) : [];
    $submisiStart = $submisi[0] ?? $date;
    $submisiEnd = $submisi[1] ?? $date;

    $bimbingan = $event['bimbingan'] ?? FALSE;
    $bimbingan = $bimbingan ? explode('_', $bimbingan) : [];
    $bimbinganStart = $bimbingan[0] ?? $date;
    $bimbinganEnd = $bimbingan[1] ?? $date;

    $penjurian_makalah = $event['penjurian_makalah'] ?? FALSE;
    $penjurian_makalah = $penjurian_makalah ? explode('_', $penjurian_makalah) : [];
    $penjurianMakalahStart = $penjurian_makalah[0] ?? $date;
    $penjurianMakalahEnd = $penjurian_makalah[1] ?? $date;

    $penjurian_grand_final = $event['penjurian_grand_final'] ?? FALSE;
    $penjurian_grand_final = $penjurian_grand_final ? explode('_', $penjurian_grand_final) : [];
    $penjurianGrandFinalStart = $penjurian_grand_final[0] ?? $date;
    $penjurianGrandFinalEnd = $penjurian_grand_final[1] ?? $date;

    $grandFinalTime = ($penjurianGrandFinalStart < $date) ? TRUE : FALSE;
    $makalahTime = ($penjurianMakalahStart <= $date && $penjurianMakalahEnd >= $date) ? TRUE : FALSE;
?>
<div>
    <h6>Informasi Penelitian/Inovasi</h6>
    <table class="table table-bordered w-50">
        <tbody><tr>
            <td>Judul</td>
            <td><?= $tim['inovasi'] ?? '' ?></td>
        </tr>
        <tr>
            <td>Abstract</td>
            <td><?= $tim['abstract'] ?? '' ?></td>
        </tr>
    </tbody></table>
</div>

<p>
    <?php if ($team_makalah_3): ?>
        <a href="<?= get_permalink_materi($team_makalah_3) ?>" target="blank">[link download makalah]</a>
    <?php else: ?>
        <?php if ($team_makalah_2): ?>
            <a href="<?= get_permalink_materi($team_makalah_2) ?>" target="blank">[link download makalah]</a>
        <?php else: ?>
            <?php if ($team_makalah_1): ?>
                <a href="<?= get_permalink_materi($team_makalah_1) ?>" target="blank">[link download makalah]</a>
            <?php endif ?>
        <?php endif ?>
    <?php endif ?>
    <?php if ($team_lampiran_3): ?>
        <a href="<?= get_permalink_materi($team_lampiran_3) ?>" target="blank">[link download lampiran]</a>
    <?php else: ?>
        <?php if ($team_lampiran_2): ?>
            <a href="<?= get_permalink_materi($team_lampiran_2) ?>" target="blank">[link download lampiran]</a>
        <?php else: ?>
            <?php if ($team_lampiran_1): ?>
                <a href="<?= get_permalink_materi($team_lampiran_1) ?>" target="blank">[link download lampiran]</a>
            <?php endif ?>
        <?php endif ?>
    <?php endif ?>
</p>

<h6>Formulir Penjurian</h6>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-uppercase" colspan="3">penjurian makalah</th>
            <th class="text-uppercase" colspan="2">penjurian grandfinal</th>
        </tr>
        <tr>
            <th class="text-uppercase">ide (20%)</th>
            <th class="text-uppercase">metode (15%)</th>
            <th class="text-uppercase">manfaat (40%)</th>
            <th class="text-uppercase">display (10%)</th>
            <th class="text-uppercase">presentasi (15%)</th>
        </tr>
        <tr>
            <td><small class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</small></td>
            <td><small class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</small></td>
            <td><small class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</small></td>
            <td><small class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</small></td>
            <td><small class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</small></td>
        </tr>
        <tr>
            <?php if ($penjurianMakalahStart < $date): ?>
                <?php if ($penjurianMakalahEnd >= $date): ?>
                    <td>
                        <input type="number" name="ide" id="ide" class="form-control" placeholder="0-100" autocomplete="off" min="0" max="100" value="<?= $team_nilai_ide ?>">
                    </td>
                    <td>
                        <input type="number" name="metode" id="metode" class="form-control" placeholder="0-100" autocomplete="off" min="0" max="100" value="<?= $team_nilai_metode ?>">
                    </td>
                    <td>
                        <input type="number" name="manfaat" id="manfaat" class="form-control" placeholder="0-100" autocomplete="off" min="0" max="100" value="<?= $team_nilai_manfaat ?>">
                    </td>
                <?php else: ?>
                    <td colspan="3">
                        Maaf, Penjurian makalah sudah ditutup pada tanggal <strong><?= $penjurianMakalahEnd ?></strong>
                    </td>
                <?php endif ?>
                <?php if ($penjurianGrandFinalStart < $date): ?>
                    <?php if ($penjurianGrandFinalEnd < $date): ?>
                        <td colspan="2">
                            Maaf, Penjurian grandfinal sudah ditutup pada tanggal <strong><?= $penjurianGrandFinalEnd ?></strong>
                        </td>
                    <?php else: ?>
                        <td>
                            <input type="number" name="display" id="display" class="form-control" placeholder="0-100" autocomplete="off" min="0" max="100" value="<?= $team_nilai_display ?>">
                        </td>
                        <td>
                            <input type="number" name="presentasi" id="presentasi" class="form-control" placeholder="0-100" autocomplete="off" min="0" max="100" value="<?= $team_nilai_presentasi ?>">
                        </td>
                    <?php endif ?>
                <?php else: ?>
                    <td colspan="2">
                        Maaf, Penjurian grandfinal belum dibuka, akan dibuka pada tanggal <strong><?= $penjurianGrandFinalStart ?></strong>
                    </td>
                <?php endif ?>
            <?php else: ?>
                <td colspan="5">
                    Maaf, Penjurian belum dibuka, penjurian makalah akan dibuka pada tanggal <strong><?= $penjurianMakalahStart ?></strong>
                    Sedangkan untuk penjurian grandfinal akan dibuka pada tanggal <strong><?= $penjurianGrandFinalStart ?></strong>
                </td>
            <?php endif ?>
        </tr>
        <?php if ($penjurianMakalahStart < $date): ?>
            <tr>
                <td colspan="5">
                    <div>
                        <button type="button" class="btn btn-success" id="simpanNilai" ><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                    </div>
                </td>
            </tr>
        <?php endif ?>
    </thead>
</table>

<div>
    <h6>Informasi Tim</h6>
    <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <td>Nama</td>
                <td><?= $tim['nama'] ?? '' ?></td>
            </tr>
            <tr>
                <td>Kategori</td>
                <td><?= $tim['kategori_nama'] ?? '' ?></td>
            </tr>
            <tr>
                <td>Instansi</td>
                <td><?= $tim['kategori_sub_nama'] ?? '' ?></td>
            </tr>
        </tbody>
    </table>
</div>

<div>
    <h6>Anggota</h6>
    <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <td>Nama</td>
                <td>Jabatan</td>
            </tr>
            <?php if ($data_anggota): ?>
                <?php foreach ($data_anggota as $key => $value): ?>
                    <tr>
                        <td><?= ucwords($value['name']) ?></td>
                        <td><?= ucwords($value['jabatan']) ?></td>
                    </tr>
                <?php endforeach ?>
            <?php else: ?>
                <tr>
                    <td colspan="2">Maaf, Tidak ada anggota</td>
                </tr>
            <?php endif ?>
        </tbody>
    </table>
</div>
<br><br>
<script type="text/javascript">
    $('#simpanNilai').click(function(event) {
        let id = "<?= $id ?>"
        let ide = $("#ide").val() || 0
        let metode = $("#metode").val() || 0
        let manfaat = $("#manfaat").val() || 0
        let display = $("#display").val() || 0
        let presentasi = $("#presentasi").val() || 0
        let grandFinalTime = "<?= $grandFinalTime ?>"
        let makalahTime = "<?= $makalahTime ?>"
        let error = false
        if (makalahTime) {
            if (!ide || (ide > 100)) {
                $("#ide").attr('style', 'border:1px solid red;')
                error = true
            }else{
                $("#ide").attr('style', 'border:1px solid green;');
            }

            if (!metode || (metode > 100)) {
                $("#metode").attr('style', 'border:1px solid red;')
                error = true
            }else{
                $("#metode").attr('style', 'border:1px solid green;');
            }

            if (!manfaat || (manfaat > 100)) {
                $("#manfaat").attr('style', 'border:1px solid red;')
                error = true
            }else{
                $("#manfaat").attr('style', 'border:1px solid green;');
            }
        }

        if (grandFinalTime) {
            if (!display || (display > 100)) {
                $("#display").attr('style', 'border:1px solid red;')
                error = true
            }else{
                $("#display").attr('style', 'border:1px solid green;');
            }
            if (!presentasi || (presentasi > 100)) {
                $("#presentasi").attr('style', 'border:1px solid red;')
                error = true
            }else{
                $("#presentasi").attr('style', 'border:1px solid green;');
            }
        }

        if (error) {
            return;
        }

        let data = {id, ide, metode, manfaat, display, presentasi}
        $.ajax({
            url : "<?php echo base_url('juri/simpan_nilai_tim'); ?>",
            data:data,
            method:'POST',
            dataType:'json',
            success:function(response) {
                if (response.error.code != 200) {
                    alert(response.error.message)
                    $('#modalDetailPeserta').modal('hide')
                }else{
                    alert(response.error.message)
                    window.location.reload();
                }
            }
        });
    });
</script>