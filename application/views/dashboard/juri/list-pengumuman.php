<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-users"></i>&nbsp;Daftar Pengumuman</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($pengumuman->num_rows() > 0): ?>
                            <?php foreach ($pengumuman->result() as $key => $value): ?>
                                <tr class="text-left">
                                    <td class="text-center"><?= $key+1 ?></td>
                                    <td><?= $value->title ?></td>
                                    <td><?= $value->content ?></td>
                                    <td><?= $value->created_at ?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php else: ?>
                            <tr class="text-center">
                                <td scope="row" colspan="9">Maaf, Tidak Ada Data Untuk Ditampilkan</td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col">
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>