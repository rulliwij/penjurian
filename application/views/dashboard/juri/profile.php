<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Profile Peserta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-6">
                    <form method="POST" action="<?= base_url('juri/update_profile') ?>" enctype="multipart/form-data">
                        <?php 
                        $data = $juri ?? "";
                        ?>
                        <?php if ($this->session->flashdata('status')): ?>
                            <div class="<?= $this->session->flashdata('status') ?>">
                                <i class="<?= $this->session->flashdata('icon') ?>"></i>
                                &nbsp;<?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif ?>
                        <div class="form-group">
                            <label for="name">Nama Lengkap *</label>
                            <input type="hidden" name="id" id="id" value="<?php echo set_value('id',$data['id'] ?? ""); ?>" />
                            <input type="text" name="name" id="name" class="form-control" autocomplete="off" placeholder="" required="required" title="Nama Anggota" value="<?php echo set_value('name',$data['name'] ?? ""); ?>" />
                            <small><?php echo form_error('name'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="jabatan">Jabatan *</label>
                            <input type="text" name="jabatan" id="jabatan" class="form-control" autocomplete="off" placeholder="" required="required" title="Jabatan" value="<?php echo set_value('jabatan',$data['jabatan'] ?? ""); ?>" />
                            <small><?php echo form_error('jabatan'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="organisasi">Organisasi *</label>
                            <input type="text" name="organisasi" id="organisasi" class="form-control" autocomplete="off" placeholder="" required="required" title="Organisasi" value="<?php echo set_value('ord(string)',$data['organisasi'] ?? ""); ?>" />
                            <small><?php echo form_error('organisasi'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="email">Email *</label>
                            <input type="email" name="email" id="email" class="form-control" autocomplete="off" placeholder="" required="required" title="Email" <?= ($data['email'] ?? "") ? "readonly" : "" ?> value="<?php echo set_value('email',$data['email'] ?? ""); ?>" />
                            <small><?php echo form_error('email'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="phone">Nomor HP *</label>
                            <input type="text" name="phone" id="phone" class="form-control" autocomplete="off" placeholder="" required="required" title="Nomor HP" value="<?php echo set_value('phone',$data['phone'] ?? ""); ?>" />
                            <small><?php echo form_error('phone'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="no_rek">Nomor Rekening *</label>
                            <input type="number" name="no_rek" id="no_rek" class="form-control" autocomplete="off" placeholder="" required="required" title="Nomor Rekening" value="<?php echo set_value('no_rek',$data['no_rek'] ?? ""); ?>" />
                            <small><?php echo form_error('no_rek'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="bank">Bank *</label>
                            <input type="text" name="bank" id="bank" class="form-control" autocomplete="off" placeholder="" required="required" title="Bank" value="<?php echo set_value('bank',$data['bank'] ?? "BCA"); ?>" />
                            <small><?php echo form_error('bank'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="place_of_birth">Tempat Lahir *</label>
                            <input type="text" name="place_of_birth" id="place_of_birth" class="form-control" autocomplete="off" placeholder="" required="required" title="Tempat Lahir" value="<?php echo set_value('place_of_birth',$data['place_of_birth'] ?? ""); ?>" />
                            <small><?php echo form_error('place_of_birth'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="date_of_birth">Tanggal Lahir *</label>
                            <input type="text" name="date_of_birth" id="date_of_birth" class="form-control datepickerJQuery" autocomplete="off" placeholder="" required="required" title="Tanggal Lahir" value="<?php echo set_value('date_of_birth',$data['date_of_birth'] ?? ""); ?>" />
                            <small><?php echo form_error('date_of_birth'); ?></small>
                        </div>
                        <div class="form-group">
                            <?php $gender = set_value('gender',$data['gender'] ?? ""); ?>
                            <input type="radio" name="gender" id="male" value="1" <?php echo ($gender == "1") ? "checked" : "" ?> />
                            <label for="male">Laki-laki</label>
                            <div class="clearfix"></div>
                            <input type="radio" name="gender" id="female" value="2" <?php echo ($gender == "2") ? "checked" : "" ?> />
                            <label for="female">Perempuan</label>
                        </div>
                        <div class="form-group">
                            <label for="country">Negara *</label>
                            <input type="text" name="country" id="country" class="form-control" autocomplete="off" placeholder="" required="required" title="Negara" value="<?php echo set_value('country',$data['country'] ?? ""); ?>" />
                            <small><?php echo form_error('country'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="npwp">NPWP *</label>
                            <input type="text" name="npwp" id="npwp" class="form-control" autocomplete="off" placeholder="" required="required" title="NPWP" value="<?php echo set_value('npwp',$data['npwp'] ?? ""); ?>" <?= ($data['npwp'] ?? false) ? 'readonly' : '' ?> />
                            <small><?php echo form_error('npwp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="foto">Foto</label>
                            <input type="file" name="foto" id="foto" class="form-control-file <?php echo form_error('foto') ? 'is-invalid':'' ?>" accept="image/*" />
                            <small><?php echo form_error('foto'); ?></small>
                            <img id="showFoto" src="<?= get_permalink_profile($data['foto'] ?? false) ?>" class="img-responsive img-thumbnail w-25" style="display: <?= ($data['foto'] ?? false) ? 'block' : 'none'; ?>;" />
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            <button type="submit" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr style="background-color: rgba(52,73,94,0.94);color: #fff;">
                            <td colspan="2">Info Data</td>
                        </tr>
                        <tr>
                            <td class="w-25">Kategori</td>
                            <td>
                                <?php if(isset($data['kategori'])): ?>
                                    <?php foreach ($data['kategori'] as $key => $value): ?>
                                        <?= ucwords($value['nama'])."(".$value['total'].")<br> " ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="w-25">Nama</td>
                            <td><?= $data['name'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Jabatan</td>
                            <td><?= $data['jabatan'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Organisasi</td>
                            <td><?= $data['organisasi'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Nomor Rekening</td>
                            <td><?= $data['no_rek'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Bank</td>
                            <td><?= $data['bank'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Tempat Lahir</td>
                            <td><?= $data['place_of_birth'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Tanggal Lahir</td>
                            <td><?= $data['date_of_birth'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Jenis Kelamin</td>
                            <?php if (empty($data['gender'] ?? "")): ?>
                                <td><i>(Belum Ada Data)</i></td>
                            <?php else: ?>
                                <td><?= get_gender_label($data['gender'] ?? "") ?></td>
                            <?php endif ?>
                        </tr>
                        <tr>
                            <td class="w-25">Negara</td>
                            <td><?= $data['country'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Nomor HP</td>
                            <td><?= $data['phone'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Email</td>
                            <td><?= $data['email'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">NPWP</td>
                            <td><?= $data['npwp'] ?? "<i>(Belum Ada Data)</i>" ?></td>
                        </tr>
                        <tr>
                            <td class="w-25">Foto</td>
                            <td>
                                <img src="<?= get_permalink_profile($data['foto'] ??  false) ?>" class="img-responsive img-thumbnail w-25" style="display: <?= ($data['foto'] ?? false) ? 'block' : 'none'; ?>;" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#showFoto').attr('src', e.target.result);
          $('#showFoto').show();
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#foto").change(function() {
        readURL(this);
    });
</script>