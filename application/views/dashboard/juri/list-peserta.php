<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Peserta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Nama Tim</th>
                            <th>Judul & Abstract</th>
                            <th>Nilai Penjurian Makalah</th>
                            <th>Nilai Grand Final</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($peserta): ?>
                            <?php foreach ($peserta as $key => $value): ?>
                                <tr class="text-left">
                                    <td class="text-center"><?= $key+1 ?></td>
                                    <td><?= strip_tags($value->tim['nama']) ?></td>
                                    <td>
                                        <strong><?= strip_tags($value->tim['inovasi']) ?></strong><br>
                                        <p><?= substr(strip_tags($value->tim['abstract']), 0,200) ?>...</p>
                                    </td>
                                    <td><?= $value->nilai['makalah'] ?>/75</td>
                                    <td><?= $value->nilai['grand_final'] ?>/25</td>
                                    <td>
                                        <u><a href="javascript:;" class="detailPeserta" key="<?= $value->id ?>">LIHAT</a></u>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('dashboard/juri/modal/detail-peserta'); ?>

<script src="<?php echo asset('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
    $('.datatable').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "order": [[ 3, "asc" ],[ 4, "asc" ]]
    });
    $(".datatable").on('click','.detailPeserta',function(event) {
        var id = $(this).attr('key');
        if (id) {
            $.ajax({
                url : "<?php echo base_url('juri/get_detail_peserta'); ?>",
                data:{id : id},
                method:'GET',
                dataType:'json',
                success:function(response) {
                    if (response.error.code != 200) {
                        $('.modal-title').html(response.message);
                        $('.modal-body').html(response.error.message);
                        $('#modalDetailPeserta').modal({backdrop: 'static', keyboard: true, show: true});
                    }else{
                        $('.modal-title').html(response.data.title);
                        $('.modal-body').html(response.data.content);
                        $('#modalDetailPeserta').modal({backdrop: 'static', keyboard: true, show: true});
                    }
                }
            });
        }
    });
</script>