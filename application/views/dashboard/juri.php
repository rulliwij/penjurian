<?php 
    $date = date('Y-m-d H:i:s');
    
    $submisi = $event['submisi'] ?? FALSE;
    $submisi = $submisi ? explode('_', $submisi) : [];
    $submisiStart = $submisi[0] ?? $date;
    $submisiEnd = $submisi[1] ?? $date;

    $bimbingan = $event['bimbingan'] ?? FALSE;
    $bimbingan = $bimbingan ? explode('_', $bimbingan) : [];
    $bimbinganStart = $bimbingan[0] ?? $date;
    $bimbinganEnd = $bimbingan[1] ?? $date;

    $penjurian_makalah = $event['penjurian_makalah'] ?? FALSE;
    $penjurian_makalah = $penjurian_makalah ? explode('_', $penjurian_makalah) : [];
    $penjurianMakalahStart = $penjurian_makalah[0] ?? $date;
    $penjurianMakalahEnd = $penjurian_makalah[1] ?? $date;

    $penjurian_grand_final = $event['penjurian_grand_final'] ?? FALSE;
    $penjurian_grand_final = $penjurian_grand_final ? explode('_', $penjurian_grand_final) : [];
    $penjurianGrandFinalStart = $penjurian_grand_final[0] ?? $date;
    $penjurianGrandFinalEnd = $penjurian_grand_final[1] ?? $date;
?>
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Profile Juri</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php $total = 0; $totalDinilai = 0; ?>
                <table class="table table-bordered">
                    <tbody>
                            <tr>
                                <td>Nama Juri</td>
                                <td><?= $this->session->userdata('nama') ?></td>
                            </tr>
                            <tr>
                                <td>Kategori</td>
                                <td>
                                    <?php if($juri['kategori'] ?? false): ?>
                                        <?php foreach ($juri['kategori'] as $key => $value): ?>
                                            <?= ucwords($value['nama'])."(".$value['total_dinilai']."/".$value['total'].")<br>" ?>
                                            <?php 
                                                $total += $value['total'];
                                                $totalDinilai += $value['total_dinilai'];
                                            ?>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah Submisi</td>
                                <td>
                                    <?= $totalDinilai ?> dari <?= $total ?> tim telah dinilai pada tahap Penjurian Makalah
                                </td>
                            </tr>
                            <tr>
                                <td>Tahap Penjurian</td>
                                <td>
                                    <ul>
                                        <li>
                                            <?php if ($penjurianMakalahStart < $date): ?>
                                                Masa penjurian dibuka sejak tanggal <strong><?= $penjurianMakalahStart ?></strong> dan akan ditutup pada tanggal <strong><?= $penjurianGrandFinalEnd ?></strong>
                                            <?php else: ?>
                                                Penjurian belum dibuka, akan dibuka pada tanggal <strong><?= $penjurianMakalahStart ?></strong>
                                            <?php endif ?>
                                        </li>
                                        <li>
                                            Penjurian Makalah pada <strong><?= $penjurianMakalahStart." - ".$penjurianMakalahEnd ?></strong>
                                            <?php if ($penjurianMakalahEnd < $date): ?>
                                                (sudah ditutup)
                                            <?php endif ?>
                                        </li>
                                        <li>
                                            Penjurian Grand Final pada <strong><?= $penjurianGrandFinalStart." - ".$penjurianGrandFinalEnd ?></strong>
                                            <?php if ($penjurianGrandFinalEnd < $date): ?>
                                                (sudah ditutup)
                                            <?php endif ?>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>