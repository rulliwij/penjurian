<link href="<?php echo asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Edit Anggota</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <a href="<?= base_url('peserta/list_anggota') ?>" class="btn btn-outline-secondary btn-sm mb-3"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
                <form method="POST" action="<?= base_url('peserta/update_anggota/'.$anggota->id) ?>" enctype="multipart/form-data">
                    <?php if ($this->session->flashdata('status')): ?>
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif ?>
                    <div class="form-group">
                        <label for="name">Nama Lengkap *</label>
                        <input type="text" name="name" id="name" class="form-control w-50" autocomplete="off" placeholder="" required="required" title="Nama Lengkap" value="<?php echo set_value('name', $anggota->name); ?>" />
                        <small><?php echo form_error('name'); ?></small>
                    </div>
                    <?php if ($anggota->jabatan == 'ketua'): ?>
                        <input type="hidden" name="jabatan" value="ketua">
                    <?php else: ?>
                        <div class="form-group">
                            <label for="jabatan">Jabatan dalam Tim *</label><br>
                            <select name="jabatan" id="jabatan" class="form-control w-50 select2" title="Jabatan dalam Tim" required>
                                <option value="" <?php echo (set_value('jabatan',$anggota->jabatan) == '') ? 'selected' : '' ?>>-- Pilih Jabatan --</option>
                                <option value="anggota" <?php echo (set_value('jabatan',$anggota->jabatan) == 'anggota') ? 'selected' : '' ?>>Anggota</option>
                                <!-- <option value="ketua" <?php echo (set_value('jabatan',$anggota->jabatan) == 'ketua') ? 'selected' : '' ?>>Ketua</option> -->
                                <option value="pembimbing" <?php echo (set_value('jabatan',$anggota->jabatan) == 'pembimbing') ? 'selected' : '' ?>>Guru Pembimbing</option>
                            </select>
                            <small><?php echo form_error('jabatan'); ?></small>
                        </div>
                    <?php endif ?>
                    <div class="form-group">
                        <label for="peran">Peranan dalam Tim *</label>
                        <textarea name="peran" id="peran" class="form-control w-50" title="Peranan dalam Tim" rows="4" required><?= set_value('peran',$anggota->peran) ?></textarea>
                        <small><?php echo form_error('peran'); ?></small>
                    </div>
                    <div class="form-group">
                        <input type="radio" name="gender" id="male" value="1" <?= ($anggota->gender == '1') ? "checked" : '' ?> />
                        <label for="male">Laki-laki</label>
                        <div class="clearfix"></div>
                        <input type="radio" name="gender" id="female" value="2" <?= ($anggota->gender == '2') ? "checked" : '' ?> />
                        <label for="female">Perempuan</label>
                    </div>
                    <div class="form-group">
                        <label for="agama">Agama *</label><br>
                        <select name="agama" id="agama" class="form-control w-50 select2" title="Agama" required>
                            <option value="" <?php echo (set_value('agama',$anggota->agama) == '') ? 'selected' : '' ?>>-- Pilih Agama --</option>
                            <option value="islam" <?php echo (set_value('agama',$anggota->agama) == 'islam') ? 'selected' : '' ?>>Islam</option>
                            <option value="kristen" <?php echo (set_value('agama',$anggota->agama) == 'kristen') ? 'selected' : '' ?>>Kristen</option>
                            <option value="katolik" <?php echo (set_value('agama',$anggota->agama) == 'katolik') ? 'selected' : '' ?>>Katolik</option>
                            <option value="hindu" <?php echo (set_value('agama',$anggota->agama) == 'hindu') ? 'selected' : '' ?>>Hindu</option>
                            <option value="budha" <?php echo (set_value('agama',$anggota->agama) == 'budha') ? 'selected' : '' ?>>Budha</option>
                            <option value="konghucu" <?php echo (set_value('agama',$anggota->agama) == 'konghucu') ? 'selected' : '' ?>>Konghucu</option>
                        </select>
                        <small><?php echo form_error('jabatan'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="place_of_birth">Tempat Lahir *</label>
                        <input type="text" name="place_of_birth" id="place_of_birth" class="form-control w-50" autocomplete="off" placeholder="" required="required" title="Tempat Lahir" value="<?php echo set_value('place_of_birth',$anggota->place_of_birth); ?>" />
                        <small><?php echo form_error('place_of_birth'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="date_of_birth">Tanggal Lahir *</label>
                        <input type="text" name="date_of_birth" id="date_of_birth" class="form-control w-50 datepickerJQuery" autocomplete="off" placeholder="" required="required" title="Tanggal Lahir" value="<?php echo set_value('date_of_birth',$anggota->date_of_birth); ?>" />
                        <small><?php echo form_error('date_of_birth'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK  *</label>
                        <input type="number" name="nik" id="nik" class="form-control w-50" autocomplete="off" placeholder="" required="required" title="NIK" value="<?php echo set_value('nik',$anggota->nik); ?>" />
                        <small><?php echo form_error('nik'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat sesuai KTP *</label>
                        <textarea name="alamat" id="alamat" class="form-control w-50" title="Alamat sesuai KTP" rows="4" required><?= set_value('alamat',$anggota->alamat) ?></textarea>
                        <small><?php echo form_error('alamat'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="provinsi">Provinsi *</label><br>
                        <select name="provinsi" id="provinsi" class="form-control w-50 select2" title="Provinsi" required>
                            <option value="" <?php echo (set_value('provinsi',$anggota->provinsi) == '') ? 'selected' : '' ?>>-- Pilih Provinsi --</option>
                            <?php foreach ($provinsi as $key => $value): ?>
                                <option value="<?= $value['id'] ?>" <?php echo (set_value('provinsi',$anggota->provinsi) == $value['id']) ? 'selected' : '' ?>><?= ucwords(strtolower($value['name'])) ?></option>
                            <?php endforeach ?>
                        </select>
                        <small><?php echo form_error('provinsi'); ?></small>
                        <h2 id="loading" style="display: none;">Loading...</h2>
                    </div>
                    <div class="form-group">
                        <label for="kabupaten">Kabupaten *</label><br>
                        <select name="kabupaten" id="kabupaten" class="form-control w-50 select2" title="Kabupaten" required>
                            <option value="" <?php (set_value('kabupaten',$anggota->kabupaten) == '') ? 'selected' : '' ?>>-- Pilih Kabupaten --</option>
                        </select>
                        <small><?php echo form_error('kabupaten'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="phone">Nomor HP *</label>
                        <input type="text" name="phone" id="phone" class="form-control w-50" autocomplete="off" placeholder="" required="required" title="Nomor HP" value="<?php echo set_value('phone',$anggota->phone); ?>" />
                        <small><?php echo form_error('phone'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="email">Email *</label>
                        <input type="email" name="email" id="email" class="form-control w-50" autocomplete="off" placeholder="" required="required" title="Email" readonly value="<?php echo set_value('email',$anggota->email); ?>" />
                        <small><?php echo form_error('email'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="country">Negara *</label>
                        <input type="text" name="country" id="country" class="form-control w-50" autocomplete="off" placeholder="" required="required" title="Negara" value="<?php echo set_value('country',$anggota->country); ?>" />
                        <small><?php echo form_error('country'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="foto">Foto *</label>
                        <input type="file" name="foto" id="foto" class="form-control-file <?php echo form_error('foto') ? 'is-invalid':'' ?>" accept="image/*" />
                        <small><?php echo form_error('foto'); ?></small>
                        <img id="showFoto" src="<?= get_permalink_profile($anggota->foto) ?>" class="img-responsive img-thumbnail w-25" style="display: <?= $anggota->foto ? 'block' : 'none'; ?>;" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                        <button type="submit" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript">
    window.addEventListener('load', function () {
        $("#provinsi").trigger('change')
    })
    $(function() {
        $('.select2').select2();
        $("#provinsi").change(function(){
            $("#loading").show();
            $("#kabupaten").prop('disabled', true);
            var id_provinsi = $(this).val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?= base_url('peserta/get_kabupaten') ?>",
                data: {id_provinsi:id_provinsi},
                success: function(msg){
                    $("#kabupaten").html(msg);
                    $("#kabupaten").prop('disabled', false);
                    $("#loading").hide();
                    let selected = "<?= set_value('kabupaten',$anggota->kabupaten) ?>"
                    if (selected) {
                        $("#kabupaten").val(selected)
                    }
                }
            });                    
        });
    })

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#showFoto').attr('src', e.target.result);
          $('#showFoto').show();
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#foto").change(function() {
        readURL(this);
    });
</script>