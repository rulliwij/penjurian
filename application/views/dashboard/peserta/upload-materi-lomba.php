<?php 
    $date = date('Y-m-d H:i:s');
    
    $submisi = $event['submisi'] ?? FALSE;
    $submisi = $submisi ? explode('_', $submisi) : [];
    $submisiStart = $submisi[0] ?? $date;
    $submisiEnd = $submisi[1] ?? $date;

    $bimbingan = $event['bimbingan'] ?? FALSE;
    $bimbingan = $bimbingan ? explode('_', $bimbingan) : [];
    $bimbinganStart = $bimbingan[0] ?? $date;
    $bimbinganEnd = $bimbingan[1] ?? $date;

    $penjurian_makalah = $event['penjurian_makalah'] ?? FALSE;
    $penjurian_makalah = $penjurian_makalah ? explode('_', $penjurian_makalah) : [];
    $penjurianMakalahStart = $penjurian_makalah[0] ?? $date;
    $penjurianMakalahEnd = $penjurian_makalah[1] ?? $date;

    $penjurian_grand_final = $event['penjurian_grand_final'] ?? FALSE;
    $penjurian_grand_final = $penjurian_grand_final ? explode('_', $penjurian_grand_final) : [];
    $penjurianGrandFinalStart = $penjurian_grand_final[0] ?? $date;
    $penjurianGrandFinalEnd = $penjurian_grand_final[1] ?? $date;
?>
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= ucwords($title) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php 
                $titleInovation = $tim['inovasi'] ?? "Tidak ada inovasi/penelitian";
                $abstractInovation = $tim['abstract'] ?? "Tidak ada abstract";

                $count = get_user_meta($this->session->userdata('id'),'team_upload_makalah_count')->meta_value ?? 1;
                $title = ["Upload Perdana","Upload Revisi 1","Upload Revisi 2"];
                
                $lampiran1st = get_user_meta($this->session->userdata('id'),'team_lampiran_1') ?? "";
                $lampiran1 = $lampiran1st->meta_value ?? "";
                $lampiran2nd = get_user_meta($this->session->userdata('id'),'team_lampiran_2') ?? "";
                $lampiran2 = $lampiran2nd->meta_value ?? "";
                $lampiran3rd = get_user_meta($this->session->userdata('id'),'team_lampiran_3') ?? "";
                $lampiran3 = $lampiran3rd->meta_value ?? "";
                $makalah1st = get_user_meta($this->session->userdata('id'),'team_makalah_1') ?? "";
                $makalah1 = $makalah1st->meta_value ?? "";
                $makalah2nd = get_user_meta($this->session->userdata('id'),'team_makalah_2') ?? "";
                $makalah2 = $makalah2nd->meta_value ?? "";
                $makalah3rd = get_user_meta($this->session->userdata('id'),'team_makalah_3') ?? "";
                $makalah3 = $makalah3rd->meta_value ?? "";
                ?>
                <form method="POST" action="<?= base_url('peserta/update_upload_materi_lomba') ?>" enctype="multipart/form-data">
                    <?php if ($this->session->flashdata('status')): ?>
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif ?>
                    
                    <div class="row mb-3">
                        <?php if ($submisiStart <= $date && $submisiEnd >= $date): ?>
                            <div class="col-md-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-exclamation-circle"></i>&nbsp;<strong>PERHATIAN</strong> : Hanya dapat direvisi 2x
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="col-md-12">
                                <p>
                                    Batas waktu submisi Makalah pada tanggal <?= $submisiStart ?> sampai <?= $submisiEnd ?> 
                                    <?php if ($submisiEnd <= $date): ?>
                                        <strong>SUDAH DITUTUP</strong>
                                    <?php endif ?>
                                </p>
                            </div>
                        <?php endif ?>
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Judul Inovasi/Penelitian</td>
                                        <td><?= ucwords($titleInovation) ?></td>
                                    </tr>
                                    <tr>
                                        <td>Abstract Inovasi/Penelitian</td>
                                        <td><?= ucwords($abstractInovation) ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <?php if ($makalah1 && $lampiran1): ?>
                        <p>1.&nbsp;
                            <ul style="padding-left: 30px;margin-top: -35px;">
                                <li>
                                    <i class="fa fa-check-circle text-success"></i> <?= $title[0] ?> (<a href="<?= get_permalink_materi($makalah1) ?>" target="blank">Download</a>)
                                    <small class="help-block"><?= $makalah1st->created_at ?></small>
                                </li>
                                <li>
                                    <i class="fa fa-check-circle text-success"></i> <?= $title[0] ?> (<a href="<?= get_permalink_materi($lampiran1) ?>" target="blank">Download</a>)
                                    <small class="help-block"><?= $lampiran1st->created_at ?></small>
                                </li>
                            </ul>
                        </p>
                    <?php endif ?>
                    <?php if ($makalah2 && $lampiran2): ?>
                        <p>2.&nbsp;
                            <ul style="padding-left: 30px;margin-top: -35px;">
                                <li>
                                    <i class="fa fa-check-circle text-success"></i> <?= $title[1] ?> (<a href="<?= get_permalink_materi($makalah2) ?>" target="blank">Download</a>)
                                    <small class="help-block"><?= $makalah2nd->created_at ?></small>
                                </li>
                                <li>
                                    <i class="fa fa-check-circle text-success"></i> <?= $title[1] ?> (<a href="<?= get_permalink_materi($lampiran2) ?>" target="blank">Download</a>)
                                    <small class="help-block"><?= $lampiran2nd->created_at ?></small>
                                </li>
                            </ul>
                        </p>
                    <?php endif ?>
                    <?php if ($makalah3 && $lampiran3): ?>
                        <p>3.&nbsp;
                            <ul style="padding-left: 30px;margin-top: -35px;">
                                <li>
                                    <i class="fa fa-check-circle text-success"></i> <?= $title[2] ?> (<a href="<?= get_permalink_materi($makalah3) ?>" target="blank">Download</a>)
                                    <small class="help-block"><?= $makalah3rd->created_at ?></small>
                                </li>
                                <li>
                                    <i class="fa fa-check-circle text-success"></i> <?= $title[2] ?> (<a href="<?= get_permalink_materi($lampiran3) ?>" target="blank">Download</a>)
                                    <small class="help-block"><?= $lampiran3rd->created_at ?></small>
                                </li>
                            </ul>
                        </p>
                    <?php endif ?>

                    <?php if ($count < 4): ?>
                        <?php if ($submisiStart <= $date && $submisiEnd >= $date): ?>
                            <!-- MAKALAH -->
                            <h4><?= $count ?>.&nbsp;<?= $title[$count-1] ?></h4>
                            <div class="form-group">
                                <label for="makalah<?= $count ?>">Makalah</label>
                                <input type="file" name="makalah<?= $count ?>" id="makalah<?= $count ?>" class="form-control-file <?php echo form_error('makalah'.$count) ? 'is-invalid':'' ?>" accept="application/pdf" />
                                <div class="invalid-feedback">
                                    <?php echo form_error('makalah'.$count) ?>
                                </div>
                                <small class="help-block">pdf</small>
                            </div>

                            <!-- LAMPIRAN -->
                            <div class="form-group">
                                <label for="lampiran<?= $count ?>">Lampiran</label>
                                <input type="file" name="lampiran<?= $count ?>" id="lampiran<?= $count ?>" class="form-control-file <?php echo form_error('lampiran'.$count) ? 'is-invalid':'' ?>" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/pdf, image/*" />
                                <div class="invalid-feedback">
                                    <?php echo form_error('lampiran'.$count) ?>
                                </div>
                                <small class="help-block">pdf, jpg, png, docx, xlsx, pptx</small>
                            </div>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if ($makalah1 && $makalah2 && $makalah3 && $lampiran1 && $lampiran2 && $lampiran3): ?>
                        <div class="form-group">
                            <button type="button" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Selesai, Terimakasih</button>
                        </div>
                    <?php else: ?>
                        <?php if ($submisiStart <= $date && $submisiEnd >= $date): ?>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                </form>
            </div>
        </div>
    </div>
</div>