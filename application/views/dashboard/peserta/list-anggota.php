<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Anggota</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <a href="<?= base_url('peserta/create_anggota') ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus-circle"></i>&nbsp;Tambah</a>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr class="text-left">
                            <th class="text-center">#</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Jenis Kelamin</th>
                            <th>Negara</th>
                            <th>Nomor HP</th>
                            <th>Email</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($anggota->num_rows() > 0): ?>
                            <?php foreach ($anggota->result() as $key => $value): ?>
                                <tr class="text-left">
                                    <td class="text-center"><?= $key+1 ?></td>
                                    <td>
                                        <?= $value->name ?>
                                        <div class="clearfix"></div>
                                        <small class="help-block">
                                            <!-- <a href="">Lihat</a> | -->
                                            <a href="<?= base_url('peserta/edit_anggota/'.$value->id) ?>">Edit</a>
                                            <?php if ($value->jabatan != 'ketua'): ?>
                                                | <a href="<?= base_url('peserta/delete_anggota/'.$value->id) ?>" onclick="confirmation(event)">Hapus</a>
                                            <?php endif ?>
                                        </small>        
                                    </td>
                                    <td><span class="<?= ($value->jabatan == 'ketua' ? 'badge badge-primary' : '') ?> p-2 text-uppercase"><?= ($value->jabatan == 'ketua' ? '<i class="fa fa-check-circle"></i>&nbsp;' : '') ?><?= $value->jabatan ?></span></td>
                                    <td><?= $value->place_of_birth ?></td>
                                    <td><?= date('d/m/Y', strtotime($value->date_of_birth)) ?></td>
                                    <td><?= get_gender_label($value->gender) ?></td>
                                    <td><?= $value->country ?></td>
                                    <td><?= $value->phone ?></td>
                                    <td><?= $value->email ?></td>
                                    <td><?= $value->created_at ?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php else: ?>
                            <tr class="text-center">
                                <td scope="row" colspan="9">Maaf, Tidak Ada Data Untuk Ditampilkan</td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>