<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Profile Peserta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php 
                    $nama = $tim['nama'] ?? '';
                    $provinsi_id = $tim['provinsi_id'] ?? '';
                    $inovasi = $tim['inovasi'] ?? '';
                    $abstract = $tim['abstract'] ?? '';
                ?>
                <form method="POST" action="<?= base_url('peserta/update_profile') ?>">
                    <?php if ($this->session->flashdata('status')): ?>
                        <div class="<?= $this->session->flashdata('status') ?>">
                            <i class="<?= $this->session->flashdata('icon') ?>"></i>
                            &nbsp;<?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif ?>
                    <div class="form-group row">
                        <label for="nama" class="col-md-3">Nama Tim *</label>
                        <div class="col-md-9">
                            <input type="text" name="nama" id="nama" class="form-control" autocomplete="off" placeholder="" required="required" title="Nama Tim" value="<?php echo set_value('nama',$nama); ?>" />
                            <small><?php echo form_error('nama'); ?></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="provinsi_id" class="col-md-3">Asal Tim *</label>
                        <div class="col-md-9">
                            <select name="provinsi_id" id="provinsi_id" class="form-control select2" required title="Asal Tim" >
                                <option value="">-- Pilih --</option>
                                <?php foreach ($provinsi as $key => $value): ?>
                                    <option value="<?= $value['id'] ?>" <?php echo (set_value('provinsi_id',$provinsi_id) == $value['id']) ? 'selected' : ''; ?>><?= ucwords($value['name']) ?></option>
                                <?php endforeach ?>
                            </select>
                            <small><?php echo form_error('provinsi_id'); ?></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inovasi" class="col-md-3">Judul Inovasi/Penelitian *</label>
                        <div class="col-md-9">
                            <input type="text" name="inovasi" id="inovasi" class="form-control" autocomplete="off" placeholder="" required="required" title="Judul Inovasi/Penelitian" value="<?php echo set_value('inovasi',$inovasi); ?>" />
                            <small><?php echo form_error('inovasi'); ?></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="abstract" class="col-md-3">Abstract Inovasi/Penelitian *</label>
                        <div class="col-md-9">
                            <textarea name="abstract" id="abstract" class="form-control" autocomplete="off" placeholder="" required="required" title="Deskripsi Inovasi/Penelitian" rows="5"><?php echo set_value('abstract',$abstract); ?></textarea>
                            <small><?php echo form_error('abstract'); ?></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                        <button type="reset" class="btn btn-outline-secondary btn-sm"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.select2').select2();
    })
</script>