<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Panduan Lomba</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= nl2br($event['tutorial']) ?? '' ?>
            </div>
        </div>
    </div>
</div>