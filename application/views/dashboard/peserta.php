<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
<?php                 
    $team_name = $tim['nama'] ?? '';
    $team_category = $tim['kategori_id'] ?? '';
    $team_school = $tim['kategori_sub_id'] ?? '';
?>
<div class="row">
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Dashboard Peserta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                Ini adalah Dashboard milik Peserta dengan Nama <strong><?= ucwords($this->session->userdata('nama')) ?></strong>
                <hr>
                <?php if ($this->session->flashdata('status')): ?>
                    <div class="<?= $this->session->flashdata('status') ?>">
                        <i class="<?= $this->session->flashdata('icon') ?>"></i>
                        &nbsp;<?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('peserta/save_dashboard') ?>" method="POST">
                    <input type="hidden" name="id" id="id" class="form-control" value="<?php echo set_value('id',($ketua->id ?? '')); ?>" />
                    <div class="form-group row">
                        <label for="kategori" class="col-md-3">Pilih Kategori *</label>
                        <div class="col-md-9">
                            <select name="kategori" class="form-control select2" id="kategori" required>
                                <option value="" <?php echo (set_value('kategori', $team_category) == '') ? 'selected' : '' ?>>Pilih Kategori</option>
                                <?php foreach ($kategori as $key => $value): ?>
                                    <option value="<?= $value['id'] ?>" <?php echo (set_value('kategori', $team_category) == $value['id']) ? 'selected' : '' ?>><?= ucwords($value['nama']) ?></option>
                                <?php endforeach ?>
                            </select>
                            <small><?php echo form_error('kategori'); ?></small>
                            <h2 id="loading" style="display: none;">Loading...</h2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kategori_sub" class="col-md-3">Nama Instansi *</label>
                        <div class="col-md-9">
                            <select name="kategori_sub" class="form-control select2" id="kategori_sub" required>
                                <option value="">Pilih</option>
                            </select>
                            <small><?php echo form_error('kategori_sub'); ?></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_tim" class="col-md-3">Nama Tim *</label>
                        <div class="col-md-9">
                            <input type="text" name="nama_tim" id="nama_tim" class="form-control" autocomplete="off" placeholder="" required title="Nama Tim" value="<?php echo set_value('nama_tim',$team_name); ?>" />
                        </div>
                        <small><?php echo form_error('nama_tim'); ?></small>
                    </div><hr>
                    <div class="form-group row">
                        <label for="nama_ketua_tim" class="col-md-3">Nama Ketua Tim *</label>
                        <div class="col-md-9">
                            <input type="text" name="nama_ketua_tim" id="nama_ketua_tim" class="form-control" autocomplete="off" placeholder="" required title="Nama Ketua Tim" value="<?php echo set_value('nama_ketua_tim',($ketua->name ?? '')); ?>" />
                        </div>
                        <small><?php echo form_error('team_name'); ?></small>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-3">Email *</label>
                        <div class="col-md-9">
                            <input type="email" name="email" id="email" class="form-control" autocomplete="off" placeholder="" required title="Email" <?= $ketua ? "disabled" : '' ?> value="<?php echo set_value('email',($ketua->email ?? '')); ?>" />
                        </div>
                        <small><?php echo form_error('email'); ?></small>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-md-3">Nomor Telepon *</label>
                        <div class="col-md-9">
                            <input type="text" name="phone" id="phone" class="form-control" autocomplete="off" placeholder="" required title="Nomor Telepon" value="<?php echo set_value('phone',($ketua->phone ?? '')); ?>" />
                        </div>
                        <small><?php echo form_error('phone'); ?></small>
                    </div>
                    <?php if ($ketua): ?>
                        <strong><p class="text-info">Lengkapi data ketua <u><a href="<?= base_url('peserta/edit_anggota/'.$ketua->id) ?>">disini</a></u></p></strong>
                    <?php endif ?>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Simpan</button>
                        <button type="reset" class="btn btn-outline-secondary btn-sm">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript">
    window.addEventListener('load', function () {
        $("#kategori").trigger('change')
    })
    $(function() {
        $('.select2').select2();
        $("#kategori").change(function(){
            $("#loading").show();
            $("#kategori_sub").prop('disabled', true);
            var value = $(this).val();
            if(value){
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    data: {kategori_id: value},
                    url: "<?= base_url('peserta/get_instansi_ajax') ?>",
                    success: function(msg){
                        $("#kategori_sub").html(msg);
                        $("#kategori_sub").prop('disabled', false);
                        $("#loading").hide();
                        let selected = "<?= set_value('kategori_sub', $team_school) ?>"
                        if (selected) {
                            $("#kategori_sub").val(selected)
                        }
                    }
                });
            }else{
                $("#kategori_sub").html('<option value="">Pilih</option>');
                $("#loading").hide();
            }
        });
    })
</script>