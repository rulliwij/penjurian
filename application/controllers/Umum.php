<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umum extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect(base_url());
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
