<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require('./vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Panitia extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect(base_url());
		}
		if ($this->session->userdata('role_id') != get_option('website_role_panitia')->option_value) {
			redirect(base_url(role_slug($this->session->userdata('role_id'))));
		}
		$this->prefixTitle = "Panitia";
		$this->load->model('M_panitia');
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->tableJuri = get_table('website_table_juri');
		$this->tablePeserta = get_table('website_table_tim');
	}

	public function index()
	{
		$data['event'] = $this->M_panitia->getDataEventAvailable();
		$data['title'] = 'Dashboard - '.$this->prefixTitle;
		set_view('dashboard/panitia', $data);
	}

	public function list_event()
	{
		// konfigurasi pagination
		// $config['base_url']    = site_url('panitia/list_event');
		// $config['total_rows']  = $this->M_panitia->getDataEventAll();
		// $config['per_page']    = get_option('limit')->option_value ?? 10;
		// $config["uri_segment"] = 3;
  //       $choice = $config["total_rows"] / $config["per_page"];
  //       $config["num_links"] = floor($choice);

        // style pagination bootstrap v4
		// $config['first_link']       = 'First';
		// $config['last_link']        = 'Last';
		// $config['next_link']        = 'Next';
		// $config['prev_link']        = 'Prev';
		// $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		// $config['full_tag_close']   = '</ul></nav></div>';
		// $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		// $config['num_tag_close']    = '</span></li>';
		// $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		// $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		// $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		// $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		// $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		// $config['prev_tagl_close']  = '</span>Next</li>';
		// $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		// $config['first_tagl_close'] = '</span></li>';
		// $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		// $config['last_tagl_close']  = '</span></li>';
		// $this->pagination->initialize($config);

		// $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		// $data['event'] = $this->M_panitia->getDataEvent($config["per_page"], $data['page']);
		// $data['pagination'] = $this->pagination->create_links();
		$data['event'] = $this->M_panitia->getDataEventAlls();
		$data['title'] = 'Dashboard - '.$this->prefixTitle;
		set_view('dashboard/panitia/list-event', $data);
	}

	public function create_event()
	{
		set_view('dashboard/panitia/create-event', [
			'title' => 'Dashboard - '.$this->prefixTitle,
		]);
	}

	public function store_event()
	{
		$this->form_validation->set_rules(
			'judul', 
			'Judul Event', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'tanggal_mulai', 
			'Tanggal Mulai', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'tanggal_selesai', 
			'Tanggal Selesai', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);

		if ($this->M_panitia->get_current_event_year(date('Y',strtotime($this->input->post('tanggal_mulai'))))) {
			$this->form_validation->set_rules(
				'judul', 
				'Kategori', 
				'trim|valid_email',
				array('valid_email' => "Maaf, Event pada tahun ".date('Y',strtotime($this->input->post('tanggal_mulai')))." sudah ada")
			);
		}
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->create_event();
        }

        $this->db->trans_begin();
		$data = array(
			'judul' => ucwords($this->input->post('judul')),
			'tanggal_mulai' => $this->input->post('tanggal_mulai'),
			'tanggal_selesai' => $this->input->post('tanggal_selesai'),
			'created_at' => date('Y-m-d H:i:s')
		);
		$save =  $this->M_panitia->insertDataEvent($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->create_event();
		}
		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('panitia/list_event'));
	}

	public function edit_event($id)
	{
		$event = $this->M_panitia->getDataEventByID($id);
		if ($event->num_rows() == 0) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Data Tidak Ditemukan");
			redirect(base_url('peserta/list_event'));
		}
		$data['title'] = 'Dashboard - '.$this->prefixTitle;
		$data['event'] = $event->row_array();
		set_view('dashboard/panitia/edit-event', $data);
	}

	public function update_event($id)
	{
		$this->form_validation->set_rules(
			'judul', 
			'Judul Event', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'tanggal_mulai', 
			'Tanggal Mulai', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'tanggal_selesai', 
			'Tanggal Selesai', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);

		$dataEvent = $this->M_panitia->getDataEventByID($id)->row_array();
		if (!$dataEvent) {
			$this->form_validation->set_rules(
				'judul', 
				'Kategori', 
				'trim|valid_email',
				array('valid_email' => "Maaf, Data event tidak ditemukan")
			);
		}
		$tanggalMulai = date('Y',strtotime($dataEvent['tanggal_mulai']));
		$tanggalSelesai = date('Y',strtotime($dataEvent['tanggal_selesai']));
		$tanggalMulaiNew = date('Y',strtotime($this->input->post('tanggal_mulai')));
		$tanggalSelesaiNew = date('Y',strtotime($this->input->post('tanggal_selesai')));

		if (($tanggalMulai != $tanggalMulaiNew) || ($tanggalSelesai != $tanggalSelesaiNew)) {
			$this->form_validation->set_rules(
				'judul', 
				'Kategori', 
				'trim|valid_email',
				array('valid_email' => "Maaf, Event ini tidak dapat diganti tahunnya")
			);
		}
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->create_event();
        }

        $this->db->trans_begin();
		$data = array(
			'judul' => ucwords($this->input->post('judul')),
			'tanggal_mulai' => $this->input->post('tanggal_mulai'),
			'tanggal_selesai' => $this->input->post('tanggal_selesai'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('id', $id);
		$save =  $this->M_panitia->updateDataEvent($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->create_event();
		}
		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('panitia/list_event'));
	}

	public function statistik()
	{
		$data['total_each_category'] = $this->M_panitia->getTotalTimEachCategory();
		$data['total_each_school'] = $this->M_panitia->getTotalTimEachSchool();
		$data['total_each_gender'] = $this->M_panitia->getTotalTimEachGender();
		// dd($data['total_each_gender']);
		$data['title'] = $this->prefixTitle.' - Statistik';
		set_view('dashboard/panitia/statistik', $data);
	}

	public function sertifikat()
	{
		$data['anggota'] = $this->M_panitia->getDataAnggota();
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		// dd($data['anggota']);
		$data['title'] = $this->prefixTitle.' - Sertifikat';
		set_view('dashboard/panitia/sertifikat', $data);
	}

	public function export_sertifikat($kategori_id=NULL)
	{
		if ($kategori_id) {
			$tim = $this->M_panitia->getDataAnggotaByKategoriID($kategori_id);
		}else{
			$tim = $this->M_panitia->getDataAnggota();
		}
		$spreadsheet = new Spreadsheet;
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'No')
			->setCellValue('B1', 'Nama Lengkap')
			->setCellValue('C1', 'Jabatan')
			->setCellValue('D1', 'Nama Tim')
			->setCellValue('E1', 'Sekolah/Institusi')
			->setCellValue('F1', 'Email')
			->setCellValue('G1', 'Phone');

		$kolom = 2;
		foreach($tim as $key => $value) {
			$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A' . $kolom, ($key+1))
			->setCellValue('B' . $kolom, $value['name'])
			->setCellValue('C' . $kolom, $value['jabatan'])
			->setCellValue('D' . $kolom, $value['nama_tim'])
			->setCellValue('E' . $kolom, $value['nama_kategori']." / ".$value['nama_kategori_sub'])
			->setCellValue('F' . $kolom, $value['email'])
			->setCellValue('G' . $kolom, $value['phone']);
			$kolom++;
		}

		$styleArray = [
		    'borders' => [
		        'outline' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK
		        ],
		    ],
		];
		// $spreadsheet->getDefaultStyle('A1:H'.count($tim))->applyFromArray($styleArray);

		$writer = new Xlsx($spreadsheet);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="list_anggota_sertifikat_'.date('Y-m-d-His').'.xlsx"');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}

	public function rekap_penjurian()
	{
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		$data['tim'] = $this->M_panitia->getDataRekapPenjurian();
		$data['title'] = $this->prefixTitle.' - Rekap Penjurian';
		set_view('dashboard/panitia/rekap-penjurian', $data);
	}

	public function export_rekap($kategori_id=NULL)
	{
		if ($kategori_id) {
			$tim = $this->M_panitia->getDataRekapPenjurianByKategoriID($kategori_id);
		}else{
			$tim = $this->M_panitia->getDataRekapPenjurian();
		}
		$spreadsheet = new Spreadsheet;
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'No')
			->setCellValue('B1', 'Kode Tim')
			->setCellValue('C1', 'Nama Tim')
			->setCellValue('D1', 'Kategori')
			->setCellValue('E1', 'Avg Nilai PM (75%)')
			->setCellValue('F1', 'Avg Nilai GF (25%)')
			->setCellValue('G1', 'Avg Nilai Total (100%)');

		$kolom = 2;
		foreach($tim as $key => $value) {
			$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A' . $kolom, ($key+1))
			->setCellValue('B' . $kolom, $value['kode'])
			->setCellValue('C' . $kolom, $value['nama'])
			->setCellValue('D' . $kolom, $value['kategori_nama'])
			->setCellValue('E' . $kolom, $value['pm'])
			->setCellValue('F' . $kolom, $value['gf'])
			->setCellValue('G' . $kolom, ($value['pm'] + $value['gf']));
			$kolom++;
		}

		$styleArray = [
		    'borders' => [
		        'outline' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK
		        ],
		    ],
		];
		// $spreadsheet->getDefaultStyle('A1:H'.count($tim))->applyFromArray($styleArray);

		$writer = new Xlsx($spreadsheet);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="list_rekap_'.date('Y-m-d-His').'.xlsx"');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}

	function get_detail_rekap()
	{
		$resp = [];
		$id = $this->input->get('id') ?? '';
		if (!$id) {
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Maaf, Parameter tidak valid';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}

		$dataTim = $this->M_panitia->getDataTimByID($id);
		$dataNilai = $this->M_panitia->getDataNilaiByTimID($id);
		$data['nilai'] = $dataNilai;

		$resp['error']['code'] = 200;
		$resp['error']['message'] = '';
		$resp['message'] = 'success';
		$resp['data']['title'] = $dataTim['nama'];
		$resp['data']['content'] = $this->load->view('dashboard/panitia/modal/content-detail-rekap',$data,TRUE);
		echo json_encode($resp); 
		exit();
	}

	public function set_time_frame()
	{
		$data['title'] = $this->prefixTitle.' - Set Time Frame';
		$data['event'] = $this->M_panitia->getDataEventByID($this->session->userdata('event_id'))->row_array();
		set_view('dashboard/panitia/set-time-frame', $data);
	}

	public function update_set_time_frame()
	{
		$this->form_validation->set_rules(
			'submisi_start', 
			'Tanggal Mulai Submisi', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'submisi_end', 
			'Tanggal Selesai Submisi', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'bimbingan_start', 
			'Tanggal Mulai Bimbingan', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'bimbingan_end', 
			'Tanggal Selesai Bimbingan', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'penjurian_makalah_start', 
			'Tanggal Mulai Penjurian Makalah', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'penjurian_makalah_end', 
			'Tanggal Selesai Penjurian Makalah', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'penjurian_grand_final_start', 
			'Tanggal Mulai Grand Final', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'penjurian_grand_final_end', 
			'Tanggal Selesai Grand Final', 
			'trim|required|min_length[19]|max_length[19]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->set_time_frame();
        }

        $data = array(
        	'submisi' => $this->input->post('submisi_start').'_'.$this->input->post('submisi_end'),
        	'bimbingan' => $this->input->post('bimbingan_start').'_'.$this->input->post('bimbingan_end'),
        	'penjurian_makalah' => $this->input->post('penjurian_makalah_start').'_'.$this->input->post('penjurian_makalah_end'),
        	'penjurian_grand_final' => $this->input->post('penjurian_grand_final_start').'_'.$this->input->post('penjurian_grand_final_end'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('id', $this->session->userdata('event_id'));
        $save =  $this->M_panitia->updateDataEvent($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->set_time_frame();
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/set_time_frame'));
	}

	public function create_kategori()
	{
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		$data['title'] = $this->prefixTitle.' - Buat Kategori';
		set_view('dashboard/panitia/create_kategori', $data);
	}

	public function edit_kategori($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		$data['kategori'] = $this->M_panitia->getDataKategoriByID($id)->row_array();
		// dd($data['kategori']);
		$data['title'] = $this->prefixTitle.' - Edit Kategori';
		set_view('dashboard/panitia/edit-kategori', $data);
	}

	public function edit_kategori_sub($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		$data['kategorisub'] = $this->M_panitia->getDataKategoriSubByID($id);
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		// dd($data['kategori']);
		$data['title'] = $this->prefixTitle.' - Edit Instansi';
		set_view('dashboard/panitia/edit-kategori-sub', $data);
	}

	public function update_kategori_sub($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		
		$this->form_validation->set_rules(
			'nama', 
			'Nama', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);

		$this->form_validation->set_rules(
			'kategori_id', 
			'Kategori', 
			'callback__available_kategori'
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->edit_kategori_sub($id);
        }

        $data = array(
        	'nama' => $this->input->post('nama'),
        	'deskripsi' => $this->input->post('deskripsi'),
        	'kategori_id' => $this->input->post('kategori_id'),
        	'updated_at' => date('Y-m-d H:i:s')
        );

        $this->db->where('id',$id);
    	$save =  $this->M_panitia->updateDataKategoriSub($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->edit_kategori_sub($id);
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/edit_kategori_sub/'.$id));
	}

	public function _available_kategori($id){
		$cek = $this->M_panitia->getDataKategoriByID($id);
    	if ($cek->num_rows() < 1) {
    		$this->form_validation->set_message('_available_kategori', '{field} tidak ada');
    		return FALSE;
    	}else{
    		return TRUE;
    	}
    }
	
	public function store_kategori()
	{
		$this->form_validation->set_rules(
			'nama', 
			'Nama', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		if ($this->input->post('kategori_id')) {
			$this->form_validation->set_rules(
				'kategori_id', 
				'Kategori', 
				'callback__available_kategori'
			);
		}
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->create_kategori();
        }

        $data = array(
        	'nama' => $this->input->post('nama'),
        	'deskripsi' => $this->input->post('deskripsi'),
        	'created_at' => date('Y-m-d H:i:s')
        );

        if ($this->input->post('kategori_id')) {
        	// CREATE INSTANSI
        	$data['kategori_id'] = $this->input->post('kategori_id');
        	$save =  $this->M_panitia->insertDataKategoriSub($data);
        }else{
        	// CREATE KATEGORI
        	$data['event_id'] = $this->session->userdata('event_id');
        	$save =  $this->M_panitia->insertDataKategori($data);
        }
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->create_kategori();
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/kategori'));
	}

	public function update_kategori($id=NULL)
	{
		$this->form_validation->set_rules(
			'nama', 
			'Nama', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->edit_kategori($id);
        }

        $data = array(
        	'nama' => $this->input->post('nama'),
        	'deskripsi' => $this->input->post('deskripsi'),
        	'event_id' => $this->session->userdata('event_id'),
        	'updated_at' => date('Y-m-d H:i:s')
        );

    	$this->db->where('id',$id);
    	$save =  $this->M_panitia->updateDataKategori($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->edit_kategori($id);
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/edit_kategori/'.$id));
	}

	public function kategori()
	{
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		$data['title'] = $this->prefixTitle.' - List Kategori';
		set_view('dashboard/panitia/kategori', $data);
	}

	public function sub_kategori($id=NULL)
	{
		if (!$id) {
			$data['kategori'] = $this->M_panitia->getDataKategoriSubAll($id);
			$data['title'] = $this->prefixTitle.' - List Instansi';
			set_view('dashboard/panitia/sub-kategori', $data);
		}else{
			$data['kategori'] = $this->M_panitia->getDataKategoriSubByIDAll($id);
			// dd($data['kategori']);
			$data['title'] = $this->prefixTitle.' - List Instansi';
			set_view('dashboard/panitia/sub-kategori', $data);
		}
	}

	public function tutorial()
	{
		$data['title'] = $this->prefixTitle.' - Tutorial';
		$data['event'] = $this->M_panitia->getDataEventByID($this->session->userdata('event_id'))->row_array();
		set_view('dashboard/panitia/tutorial', $data);
	}

	public function update_tutorial()
	{
		$this->form_validation->set_rules(
			'tutorial', 
			'Tutorial', 
			'trim|required|min_length[1]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->tutorial();
        }

        $data = array(
        	'tutorial' => $this->input->post('tutorial'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('id', $this->session->userdata('event_id'));
        $save =  $this->M_panitia->updateDataEvent($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->tutorial();
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/tutorial'));
	}

	public function embed_survey_juri()
	{
		$data['title'] = $this->prefixTitle.' - Emebed Survey Juri';
		$data['event'] = $this->M_panitia->getDataEventByID($this->session->userdata('event_id'))->row_array();
		set_view('dashboard/panitia/embed-survey-juri', $data);
	}

	public function update_embed_survey_juri()
	{
		$this->form_validation->set_rules(
			'embed', 
			'Embed', 
			'trim|required|min_length[1]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->embed_survey_juri();
        }

        $data = array(
        	'embed_survey_juri' => $this->input->post('embed'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('id', $this->session->userdata('event_id'));
        $save =  $this->M_panitia->updateDataEvent($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->embed_survey_juri();
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/embed_survey_juri'));
	}

	public function embed_survey_peserta()
	{
		$data['title'] = $this->prefixTitle.' - Emebed Survey Juri';
		$data['event'] = $this->M_panitia->getDataEventByID($this->session->userdata('event_id'))->row_array();
		set_view('dashboard/panitia/embed-survey-peserta', $data);
	}

	public function update_embed_survey_peserta()
	{
		$this->form_validation->set_rules(
			'embed', 
			'Embed', 
			'trim|required|min_length[1]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->embed_survey_peserta();
        }

        $data = array(
        	'embed_survey_peserta' => $this->input->post('embed'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('id', $this->session->userdata('event_id'));
        $save =  $this->M_panitia->updateDataEvent($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->embed_survey_peserta();
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/embed_survey_peserta'));
	}

	public function list_peserta()
	{
		$data = [];
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		$data['tim'] = $this->M_panitia->getDataTimAll();
		$data['title'] = $this->prefixTitle.' - List Peserta';
		set_view('dashboard/panitia/list-peserta', $data);
	}

	public function delete_peserta()
	{
		$id = $this->input->post('id');
		if (empty($id) || $id < 1) {
			die(get_option('website_function_not_found', TRUE));
		}

		$this->db->trans_begin();
		$this->db->where('kode', $id);
        $this->db->delete($this->tablePeserta);
		if ($this->db->affected_rows() < 1) {
			$this->db->trans_rollback();
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Hapus data peserta gagal.';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}
		$this->db->trans_commit();
		// SUCCESS
		$resp['error']['code'] = 200;
		$resp['error']['message'] = 'Hapus data peserta berhasil.';
		$resp['message'] = 'failed';
		$resp['data']['title'] = '';
		$resp['data']['content'] = '';
		echo json_encode($resp); 
		exit();
	}

	public function export_peserta($kategori_id=NULL)
	{
		if ($kategori_id) {
			$tim = $this->M_panitia->getDataTimByIDAll($kategori_id)->result_array();
		}else{
			$tim = $this->M_panitia->getDataTimAll()->result_array();
		}
		// dd($tim);
		$spreadsheet = new Spreadsheet;
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'No')
			->setCellValue('B1', 'Kode')
			->setCellValue('C1', 'Nama')
			->setCellValue('D1', 'Inovasi')
			->setCellValue('E1', 'Abstact')
			->setCellValue('F1', 'Nama Kategori')
			->setCellValue('G1', 'Nama Instansi')
			->setCellValue('H1', 'Tanggal Dibuat');

		$kolom = 2;
		foreach($tim as $key => $value) {
			$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A' . $kolom, ($key+1))
			->setCellValue('B' . $kolom, $value['kode'])
			->setCellValue('C' . $kolom, $value['nama'])
			->setCellValue('D' . $kolom, $value['inovasi'])
			->setCellValue('E' . $kolom, $value['abstract'])
			->setCellValue('F' . $kolom, $value['kategori_nama'])
			->setCellValue('G' . $kolom, $value['kategori_sub_nama'])
			->setCellValue('H' . $kolom, date('j F Y', strtotime($value['created_at'])));
			$kolom++;
		}

		$styleArray = [
		    'borders' => [
		        'outline' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK
		        ],
		    ],
		];
		// $spreadsheet->getDefaultStyle('A1:H'.count($tim))->applyFromArray($styleArray);

		$writer = new Xlsx($spreadsheet);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="list_peserta_'.date('Y-m-d-His').'.xlsx"');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}

	public function list_juri()
	{
		$data = [];
		$data['juri']  = $this->M_panitia->getDataJuri();
		$data['title'] = $this->prefixTitle.' - List Juri';
		set_view('dashboard/panitia/list-juri', $data);
	}

	public function create_juri()
	{
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		$data['title'] = $this->prefixTitle.' - Tambah Juri';
		set_view('dashboard/panitia/create-juri', $data);
	}

	public function edit_juri($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		$dataJuri = $this->M_panitia->getDataJuriByID($id);
		if (!$dataJuri) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-info");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Data tidak ditemukan");
			redirect(base_url('panitia/list_juri'));
		}
		$data['kategori'] = $this->M_panitia->getDataKategoriAll();
		$data['title'] = $this->prefixTitle.' - Edit Juri';
		$data['juri'] = $dataJuri;
		// dd($dataJuri);
		set_view('dashboard/panitia/edit-juri', $data);
	}

	public function update_juri($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		// VALIDATION USER
		if ($this->input->post('password')) {
			$this->form_validation->set_rules(
				'password', 
				'Password', 
				'trim|required|min_length[3]',
				array('required' => 'Maaf, %s Tidak Boleh Kosong')
			);
			$this->form_validation->set_rules(
				'conf_password', 
				'Konfirmasi Password', 
				'trim|required|min_length[3]|matches[password]',
				array('required' => 'Maaf, %s Tidak Boleh Kosong')
			);
		}

		// VALIDATION JURI
		$this->form_validation->set_rules(
			'name', 
			'Nama Anggota', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'place_of_birth', 
			'Tempat Lahir', 
			'trim|required|min_length[3]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'date_of_birth', 
			'Tanggal Lahir', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'gender', 
			'Jenis Kelamin', 
			'required|numeric|in_list[1,2]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'country', 
			'Negara', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'phone', 
			'Nomor HP', 
			'required|min_length[9]|max_length[15]|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, %s Minimal Panjangnya 9',
				'max_length' => 'Maaf, %s Maksimal Panjangnya 15'
			)
		);
		$this->form_validation->set_rules(
			'jabatan', 
			'Jabatan', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'organisasi', 
			'Organisasi', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'no_rek', 
			'Nomor Rekening', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'bank', 
			'Bank', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);

		// VALIDATION KATEGORI
		if (empty($_POST['kategori'])) {
		    $this->form_validation->set_rules(
		    	'kategori[]', 
		    	'Kategori', 
		    	'trim|required|min_length[3]',
		    	array('required' => 'Maaf, %s Tidak Boleh Kosong')
		    );
		}

		// VALIDATION RUN
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->edit_juri($id);
        }

        // GET DATA JURI
        $dataJuri = $this->M_panitia->getDataJuriByID($id);
        if (!$dataJuri) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Data juri tidak ditemukan");
        	return $this->list_juri();
        }

        $this->db->trans_begin();

        // UPDATE USER
		if ($this->input->post('password')) {
			$data = array(
				'password' => md5($this->input->post('password')),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$this->db->where('id',$dataJuri['user_id']);
			$save = $this->M_panitia->updateDataUser($data);
			if (!$save) {
				$this->db->trans_rollback();
				$this->session->set_flashdata('error', TRUE);
				$this->session->set_flashdata('status', "alert alert-danger");
				$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
				$this->session->set_flashdata('message', "Maaf, Terjadi kesalahan saat menyimpan data user");
				return $this->edit_juri($id);
			}
		}

        // UPLOAD FOTO
    	$filename = '';
    	if (!empty($_FILES['foto']['name'])) {
            $resp = $this->M_panitia->_uploadFileDinamis('foto', "jpg|jpeg|png", 'foto-juri-'.$id, './uploads/profile/');
            if ($resp['error']) {
            	$this->session->set_flashdata('error', TRUE);
            	$this->session->set_flashdata('status', "alert alert-danger");
            	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
            	$this->session->set_flashdata('message', $resp['data']);
            	return $this->edit_juri($id);
            }
            $filename = $resp['data']['file_name'];
    	}

    	// UPDATE JURI
    	$data = array(
    		'name'           => ucwords($this->input->post('name')),
			'place_of_birth' => ucwords($this->input->post('place_of_birth')),
			'date_of_birth'  => $this->input->post('date_of_birth'),
			'gender'         => $this->input->post('gender'),
			'country'        => ucwords($this->input->post('country')),
			'phone'          => $this->input->post('phone'),
			'jabatan'        => ucwords($this->input->post('jabatan')),
			'organisasi'     => ucwords($this->input->post('organisasi')),
			'no_rek'         => $this->input->post('no_rek'),
			'bank'           => ucwords($this->input->post('bank')),
			'updated_at'     => date('Y-m-d H:i:s')
    	);
    	if ($filename) {
    		$data['foto'] = $filename;
    	}
    	$this->db->where("id", $id);
    	$save =  $this->M_panitia->updateDataJuri($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi kesalahan saat menyimpan data");
			return $this->edit_juri($id);
		}
		$this->db->trans_commit();

		// UPDATE KATEGORI JURI
		$this->db->where('user_id',$dataJuri['user_id']);
		$delete = $this->M_panitia->deleteDataJuriKategori($data);
		if (!$delete) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi kesalahan saat menghapus data kategori juri");
			return $this->edit_juri($id);
		}
		foreach ($this->input->post('kategori') as $key => $value) {
			$data = array(
				'user_id' => $dataJuri['user_id'],
				'kategori_id' => $value,
				'created_at' => date('Y-m-d H:i:s')
			);
			$save = $this->M_panitia->insertDataJuriKategori($data);
			if (!$save) {
				$this->db->trans_rollback();
				$this->session->set_flashdata('error', TRUE);
				$this->session->set_flashdata('status', "alert alert-danger");
				$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
				$this->session->set_flashdata('message', "Maaf, Terjadi kesalahan saat menyimpan data kategori juri");
				return $this->edit_juri($id);
			}
		}

		// SUCCESS
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data berhasil disimpan");
		redirect(base_url('panitia/edit_juri/'.$id));
	}

	public function delete_juri()
	{
		$id = $this->input->post('id');
		if (empty($id) || $id < 1) {
			die(get_option('website_function_not_found', TRUE));
		}
		$user_id = $this->db->select("user_id")->where(["id" => $id])->get($this->tableJuri)->row("user_id");
		if (empty($user_id) || $user_id < 1) {
			die(get_option('website_function_not_found', TRUE));
		}
		$this->db->trans_begin();
		$this->db->where('id', $id);
        $this->db->delete($this->tableJuri);
		if ($this->db->affected_rows() < 1) {
			$this->db->trans_rollback();
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Hapus data juri gagal.';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}
		$this->db->where('user_id', $user_id);
        $this->db->delete("juri_kategori");
		if ($this->db->affected_rows() < 1) {
			$this->db->trans_rollback();
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Hapus data juri gagal.';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}
		$this->db->where('id', $user_id);
        $this->db->delete("user");
		if ($this->db->affected_rows() < 1) {
			$this->db->trans_rollback();
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Hapus data juri gagal.';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}
		print_r("expression");die;
		$this->db->trans_commit();
		// SUCCESS
		$resp['error']['code'] = 200;
		$resp['error']['message'] = 'Hapus data juri berhasil.';
		$resp['message'] = 'failed';
		$resp['data']['title'] = '';
		$resp['data']['content'] = '';
		echo json_encode($resp); 
		exit();
	}

	public function list_pengumuman()
	{
		$data['pengumuman'] = $this->M_panitia->getDataPengumumanAll()->result_array();
		// dd($data['pengumuman']);
		$data['title']      = $this->prefixTitle.' - List Pengumuman';
		set_view('dashboard/panitia/list-pengumuman', $data);
	}

	public function create_pengumuman()
	{
		$data['title'] = $this->prefixTitle.' - Buat Pengumuman';
		set_view('dashboard/panitia/create-pengumuman', $data);
	}

	public function edit_pengumuman($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		$data['pengumuman'] = $this->M_panitia->getDataPengumumanByID($id)->row_array();
		// dd($data['pengumuman']);
		$data['title'] = $this->prefixTitle.' - Buat Pengumuman';
		set_view('dashboard/panitia/edit-pengumuman', $data);
	}

	public function store_pengumuman()
	{
		$this->form_validation->set_rules(
			'title', 
			'Judul', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'content', 
			'Isi', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->create_pengumuman();
        }

        $data = array(
        	'title' => $this->input->post('title'),
        	'content' => $this->input->post('content'),
        	'event_id' => $this->session->userdata('event_id'),
        	'status' => $this->input->post('status') ? 1 : 0,
        	'created_at' => date('Y-m-d H:i:s')
        );
    	$save =  $this->M_panitia->insertDataPengumuman($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->create_pengumuman();
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/list_pengumuman'));
	}

	public function update_pengumuman($id=NULL)
	{
		if (!$id) {
			die(get_option('website_function_not_found', TRUE));
		}
		$this->form_validation->set_rules(
			'title', 
			'Judul', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'content', 
			'Isi', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->edit_pengumuman($id);
        }

        $data = array(
        	'title' => $this->input->post('title'),
        	'content' => $this->input->post('content'),
        	'status' => $this->input->post('status') ? 1 : 0,
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('id',$id);
    	$save =  $this->M_panitia->updateDataPengumuman($data);
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
        	return $this->edit_pengumuman($id);
        }

        // success
        $this->session->set_flashdata('error', FALSE);
        $this->session->set_flashdata('status', "alert alert-success");
        $this->session->set_flashdata('icon', "fa fa-check-circle");
        $this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
        redirect(base_url('panitia/edit_pengumuman/'.$id));
	}

	public function choice_event($id='')
	{
		$data = $this->M_panitia->getDataEventByID($id);
		if ($data->num_rows() < 1) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Event tidak ditemukan");
			return $this->list_event();
		}

		$this->session->set_userdata('event_id', $id);
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil");
		redirect(base_url('panitia/list_event'));
	}

	function get_detail_peserta()
	{
		$resp = [];
		$id = $this->input->get('id') ?? '';
		if (!$id) {
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Maaf, Parameter tidak valid';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}

		$dataTim = $this->M_panitia->getDataTimByID($id);
		// dd($dataTim);
		// $nilai = $this->M_juri->getNilai($id);

		$data['id'] = $id;
		$data['team_lampiran_1'] = get_user_meta($dataTim['user_id'],'team_lampiran_1', TRUE);
		$data['team_lampiran_2'] = get_user_meta($dataTim['user_id'],'team_lampiran_2', TRUE);
		$data['team_lampiran_3'] = get_user_meta($dataTim['user_id'],'team_lampiran_3', TRUE);
		$data['team_makalah_1'] = get_user_meta($dataTim['user_id'],'team_makalah_1', TRUE);
		$data['team_makalah_2'] = get_user_meta($dataTim['user_id'],'team_makalah_2', TRUE);
		$data['team_makalah_3'] = get_user_meta($dataTim['user_id'],'team_makalah_3', TRUE);
		$data['tim'] = $dataTim;
		$data['data_anggota'] = $this->M_panitia->getDataAnggotaByUserID($dataTim['user_id']) ?? [];
		$resp['error']['code'] = 200;
		$resp['error']['message'] = '';
		$resp['message'] = 'success';
		$resp['data']['title'] = $dataTim['nama'];
		$resp['data']['content'] = $this->load->view('dashboard/panitia/modal/content-detail-peserta',$data,TRUE);
		echo json_encode($resp); 
		exit();
	}

	function get_detail_juri()
	{
		$resp = [];
		$id = $this->input->get('id') ?? '';
		if (!$id) {
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Maaf, Parameter tidak valid';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}

		$dataJuri = $this->M_panitia->getDataJuriByID($id);
		$data['juri'] = $dataJuri;

		$resp['error']['code'] = 200;
		$resp['error']['message'] = '';
		$resp['message'] = 'success';
		$resp['data']['title'] = $dataJuri['name'];
		$resp['data']['content'] = $this->load->view('dashboard/panitia/modal/content-detail-juri',$data,TRUE);
		echo json_encode($resp); 
		exit();
	}

	public function _is_unique_email_event($email){
		$cek = $this->M_panitia->check_valid_email_event($email);
    	if ($cek) {
    		$this->form_validation->set_message('_is_unique_email_event', '{field} sudah ada yang menggunakan pada event ini');
    		return FALSE;
    	}else{
    		return TRUE;
    	}
    }

	public function _is_unique_email_event_juri($email){
		$cek = $this->M_panitia->check_valid_email_event_juri($email);
    	if ($cek) {
    		$this->form_validation->set_message('_is_unique_email_event_juri', '{field} sudah ada yang menggunakan pada event ini');
    		return FALSE;
    	}else{
    		return TRUE;
    	}
    }

	public function store_juri()
	{
		// VALIDATION USER
		$this->form_validation->set_rules(
			'username', 
			'Username', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'jss', 
			'JSS ID', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'email', 
			'Email', 
			'required|valid_email|callback__is_unique_email_event',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'valid_email' => 'Maaf, Email %s Tidal Valid',
				'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
			)
		);
		$this->form_validation->set_rules(
			'password', 
			'Password', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'conf_password', 
			'Konfirmasi Password', 
			'trim|required|min_length[3]|matches[password]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);

		// VALIDATION JURI
		$this->form_validation->set_rules(
			'name', 
			'Nama Juri', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'place_of_birth', 
			'Tempat Lahir', 
			'trim|required|min_length[3]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'date_of_birth', 
			'Tanggal Lahir', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'gender', 
			'Jenis Kelamin', 
			'required|numeric|in_list[1,2]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'country', 
			'Negara', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'phone', 
			'Nomor HP', 
			'required|min_length[9]|max_length[15]|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, %s Minimal Panjangnya 9',
				'max_length' => 'Maaf, %s Maksimal Panjangnya 15'
			)
		);
		$this->form_validation->set_rules(
			'email_publik', 
			'Email Publik', 
			'required|valid_email|callback__is_unique_email_event_juri',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'valid_email' => 'Maaf, Email %s Tidal Valid',
				'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
			)
		);
		$this->form_validation->set_rules(
			'jabatan', 
			'Jabatan', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'organisasi', 
			'Organisasi', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'no_rek', 
			'Nomor Rekening', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'bank', 
			'Bank', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'npwp', 
			'NPWP', 
			'trim|required|is_unique[juri.npwp]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'is_unique' => 'Maaf, %s sudah ada yang memakai'
			)
		);
		if (empty($_FILES['foto']['name'])){
		    $this->form_validation->set_rules('foto', 'Foto', 'required');
		}

		// VALIDATION KATEGORI
		if (empty($_POST['kategori'])) {
		    $this->form_validation->set_rules(
		    	'kategori[]', 
		    	'Kategori', 
		    	'trim|required|min_length[3]',
		    	array('required' => 'Maaf, %s Tidak Boleh Kosong')
		    );
		}
		
		// VALIDATION RUN
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->create_juri();
        }

        $this->db->trans_begin();

        // INSERT USER
		$data = array(
			'jss_id' => $this->input->post('jss'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'role_id' => get_option('website_role_juri',TRUE),
			'event_id' => $this->session->userdata('event_id'),
			'created_at' => date('Y-m-d H:i:s')
		);
		$save = $this->M_panitia->insertDataUser($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data User");
			return $this->create_juri();
		}
		$user_id = $this->db->insert_id();

		// INSERT JURI
		$filename = '';
		if (!empty($_FILES['foto']['name'])) {
	        $resp = $this->M_panitia->_uploadFileDinamis('foto', "jpg|jpeg|png", 'foto-juri-'.$user_id, './uploads/profile/');
	        if ($resp['error']) {
	        	$this->db->trans_rollback();
	        	$this->session->set_flashdata('error', TRUE);
	        	$this->session->set_flashdata('status', "alert alert-danger");
	        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
	        	$this->session->set_flashdata('message', $resp['data']);
	        	return $this->create_juri();
	        }
	        $filename = $resp['data']['file_name'];
		}
		$data = array(
			'user_id' => $user_id,
			'name' => $this->input->post('name'),
			'jabatan' => $this->input->post('jabatan'),
			'organisasi' => $this->input->post('organisasi'),
			'no_rek' => $this->input->post('no_rek'),
			'bank' => $this->input->post('bank'),
			'place_of_birth' => $this->input->post('place_of_birth'),
			'date_of_birth' => $this->input->post('date_of_birth'),
			'gender' => $this->input->post('gender'),
			'country' => $this->input->post('country'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email_publik'),
			'npwp' => $this->input->post('npwp'),
			'foto' => $filename,
			'created_at' => date('Y-m-d H:i:s')
		);
		$save = $this->M_panitia->insertDataJuri($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data Juri");
			return $this->create_juri();
		}

		// INSERT KATEGORI JURI
		foreach ($this->input->post('kategori') as $key => $value) {
			$data = array(
				'user_id' => $user_id,
				'kategori_id' => $value,
				'created_at' => date('Y-m-d H:i:s')
			);
			$save = $this->M_panitia->insertDataJuriKategori($data);
			if (!$save) {
				$this->db->trans_rollback();
				$this->session->set_flashdata('error', TRUE);
				$this->session->set_flashdata('status', "alert alert-danger");
				$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
				$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data Kategori Juri");
				return $this->create_juri();
			}
		}

		$this->db->trans_commit();

		// SUCCESS
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('panitia/list_juri'));
	}
}
