<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Juri extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect(base_url());
		}
		if ($this->session->userdata('role_id') != get_option('website_role_juri')->option_value) {
			redirect(base_url(role_slug($this->session->userdata('role_id'))));
		}
		$this->prefixTitle = "Juri";
		$this->load->model('M_juri');
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->table = get_table('website_table_juri');
		$this->tableNilai = get_table('website_table_nilai');
		if (!$this->table || !$this->tableNilai) {
		    die(get_option('website_comment_table_not_found', TRUE));
		}
	}

	public function index()
	{
		$data['title'] = 'dashboard - '.$this->prefixTitle;
		$data['juri'] = $this->M_juri->getDataJuri();
		$data['event'] = $this->M_juri->getDataEventByID();
		// dd($data['event']);
		set_view('dashboard/juri', $data);
	}

	public function edit_profile()
	{
		$data['title'] = $this->prefixTitle.' - Profile';
		$data['juri'] = $this->M_juri->getDataProfile();
		// dd($data['juri']);
		set_view('dashboard/juri/profile', $data);
	}

	public function update_profile()
	{
		$this->form_validation->set_rules(
			'name', 
			'Nama Anggota', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'place_of_birth', 
			'Tempat Lahir', 
			'trim|required|min_length[3]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'date_of_birth', 
			'Tanggal Lahir', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'gender', 
			'Jenis Kelamin', 
			'required|numeric|in_list[1,2]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'country', 
			'Negara', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'phone', 
			'Nomor HP', 
			'required|min_length[9]|max_length[15]|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, %s Minimal Panjangnya 9',
				'max_length' => 'Maaf, %s Maksimal Panjangnya 15'
			)
		);
		if (!$this->M_juri->getDataProfile()) {
			$this->form_validation->set_rules(
				'email', 
				'Email', 
				'required|valid_email|is_unique[juri.email]',
				array(
					'required' => 'Maaf, %s Tidak Boleh Kosong',
					'valid_email' => 'Maaf, Email %s Tidal Valid',
					'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
				)
			);
		}
		$this->form_validation->set_rules(
			'jabatan', 
			'Jabatan', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'organisasi', 
			'Organisasi', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'no_rek', 
			'Nomor Rekening', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'bank', 
			'Bank', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		if (!$this->M_juri->getDataProfile()) {
			$this->form_validation->set_rules(
				'npwp', 
				'NPWP', 
				'trim|required|is_unique[juri.npwp]',
				array(
					'required' => 'Maaf, %s Tidak Boleh Kosong',
					'is_unique' => 'Maaf, %s sudah ada yang memakai'
				)
			);
		}
		// if (empty($_FILES['foto']['name'])){
		//     $this->form_validation->set_rules('foto', 'Foto', 'required');
		// }

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->edit_profile();
        }

        // UPLOAD FOTO
    	$filename = '';
    	if (!empty($_FILES['foto']['name'])) {
            $resp = $this->M_juri->_uploadFileDinamis('foto', "jpg|jpeg|png", 'foto-juri-'.get_current_user_id(), './uploads/profile/');
            if ($resp['error']) {
            	$this->session->set_flashdata('error', TRUE);
            	$this->session->set_flashdata('status', "alert alert-danger");
            	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
            	$this->session->set_flashdata('message', $resp['data']);
            	return $this->create_anggota();
            }
            $filename = $resp['data']['file_name'];
    	}
    	
        $this->db->trans_begin();
        if ($this->M_juri->getDataProfile()) {
        	$data = array(
        		'user_id'        => get_current_user_id(),
        		'name'           => ucwords($this->input->post('name')),
				'place_of_birth' => ucwords($this->input->post('place_of_birth')),
				'date_of_birth'  => $this->input->post('date_of_birth'),
				'gender'         => $this->input->post('gender'),
				'country'        => ucwords($this->input->post('country')),
				'phone'          => $this->input->post('phone'),
				'jabatan'        => ucwords($this->input->post('jabatan')),
				'organisasi'     => ucwords($this->input->post('organisasi')),
				'no_rek'         => $this->input->post('no_rek'),
				'bank'           => ucwords($this->input->post('bank')),
				'npwp'           => $this->input->post('npwp'),
				'updated_at'     => date('Y-m-d H:i:s')
        	);
        	if ($filename) {
        		$data['foto'] = $filename;
        	}
        	$this->db->where("id", $this->input->post('id'));
        	$save =  $this->db->update($this->table, $data);
        }else{
        	$data = array(
        		'user_id'        => get_current_user_id(),
        		'name'           => ucwords($this->input->post('name')),
        		'place_of_birth' => ucwords($this->input->post('place_of_birth')),
        		'date_of_birth'  => $this->input->post('date_of_birth'),
        		'gender'         => $this->input->post('gender'),
        		'country'        => ucwords($this->input->post('country')),
        		'phone'          => $this->input->post('phone'),
        		'jabatan'        => ucwords($this->input->post('jabatan')),
        		'organisasi'     => ucwords($this->input->post('organisasi')),
        		'no_rek'         => $this->input->post('no_rek'),
        		'bank'           => ucwords($this->input->post('bank')),
        		'email'          => $this->input->post('email'),
        		// 'npwp'           => $this->input->post('npwp'),
        		'created_at'     => date('Y-m-d H:i:s')
        	);
        	if ($filename) {
        		$data['foto'] = $filename;
        	}
        	$save =  $this->db->insert($this->table, $data);
        }
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->edit_profile();
		}
		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('juri/edit_profile'));
	}

	public function list_pengumuman()
	{
		// konfigurasi pagination
		$config['base_url']    = site_url('juri/list_pengumuman');
		$config['total_rows']  = $this->M_juri->getDataPengumumanAll();
		$config['per_page']    = get_option('limit')->option_value ?? 10;
		$config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // style pagination bootstrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$this->pagination->initialize($config);

		$data = [];
		$data['page']       = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['pengumuman'] = $this->M_juri->getDataPengumuman($config["per_page"], $data['page']);
		$data['title']      = 'Juri - List Pengumuman';
		$data['pagination'] = $this->pagination->create_links();
		set_view('dashboard/juri/list-pengumuman', $data);
	}

	public function list_peserta_lomba()
	{
		$data['peserta']    = $this->M_juri->getDataPeserta();
		// dd($data['peserta']);
		$data['title']      = $this->prefixTitle.' - List Peserta';
		set_view('dashboard/juri/list-peserta', $data);
	}

	public function tutorial_pengisian_nilai()
	{
		$data['event'] = $this->M_juri->getDataEventByID();
		// dd($data['event']);
		$data['title'] = 'Juri - Tutorial Pengisian Nilai';
		set_view('dashboard/juri/tutorial-pengisian-nilai', $data);
	}

	public function survey()
	{
		$data['event'] = $this->M_juri->getDataEventByID();
		// dd($data['event']);
		$data['title'] = 'Juri - Survey';
		set_view('dashboard/juri/survey', $data);
	}

	function get_detail_peserta()
	{
		$resp = [];
		$id = $this->input->get('id') ?? '';
		if (!$id) {
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Maaf, Parameter tidak valid';
			$resp['message'] = 'failed';
			$resp['data']['title'] = '';
			$resp['data']['content'] = '';
			echo json_encode($resp); 
			exit();
		}

		$nilai = $this->M_juri->getNilai($id);
		$tim = $this->M_juri->getDataTimByUserID($id);

		$data['id'] = $id;
		$data['tim'] = $tim;
		$data['team_lampiran_1'] = get_user_meta($id,'team_lampiran_1', TRUE);
		$data['team_lampiran_2'] = get_user_meta($id,'team_lampiran_2', TRUE);
		$data['team_lampiran_3'] = get_user_meta($id,'team_lampiran_3', TRUE);
		$data['team_makalah_1'] = get_user_meta($id,'team_makalah_1', TRUE);
		$data['team_makalah_2'] = get_user_meta($id,'team_makalah_2', TRUE);
		$data['team_makalah_3'] = get_user_meta($id,'team_makalah_3', TRUE);
		$data['team_nilai_ide'] = $nilai['ide'] ?? 0;
		$data['team_nilai_metode'] = $nilai['metode'] ?? 0;
		$data['team_nilai_manfaat'] = $nilai['manfaat'] ?? 0;
		$data['team_nilai_display'] = $nilai['display'] ?? 0;
		$data['team_nilai_presentasi'] = $nilai['presentasi'] ?? 0;
		$data['event'] = $this->M_juri->getDataEventByID();
		$data['data_anggota'] = $this->M_juri->getDataAnggotaByUserID($id) ?? '';
		// dd($data['data_anggota']);

		$resp['error']['code'] = 200;
		$resp['error']['message'] = '';
		$resp['message'] = 'success';
		$resp['data']['title'] = $tim['nama'] ?? '';
		$resp['data']['content'] = $this->load->view('dashboard/juri/modal/content-detail-peserta',$data,TRUE);
		echo json_encode($resp); 
		exit();
	}

	function simpan_nilai_tim()
	{
		$id = $this->input->post('id');

		$event = $this->M_juri->getDataEventByID();
		$date = date('Y-m-d H:i:s');
		
		$submisi = $event['submisi'] ?? FALSE;
		$submisi = $submisi ? explode('_', $submisi) : [];
		$submisiStart = $submisi[0] ?? $date;
		$submisiEnd = $submisi[1] ?? $date;

		$bimbingan = $event['bimbingan'] ?? FALSE;
		$bimbingan = $bimbingan ? explode('_', $bimbingan) : [];
		$bimbinganStart = $bimbingan[0] ?? $date;
		$bimbinganEnd = $bimbingan[1] ?? $date;

		$penjurian_makalah = $event['penjurian_makalah'] ?? FALSE;
		$penjurian_makalah = $penjurian_makalah ? explode('_', $penjurian_makalah) : [];
		$penjurianMakalahStart = $penjurian_makalah[0] ?? $date;
		$penjurianMakalahEnd = $penjurian_makalah[1] ?? $date;

		$penjurian_grand_final = $event['penjurian_grand_final'] ?? FALSE;
		$penjurian_grand_final = $penjurian_grand_final ? explode('_', $penjurian_grand_final) : [];
		$penjurianGrandFinalStart = $penjurian_grand_final[0] ?? $date;
		$penjurianGrandFinalEnd = $penjurian_grand_final[1] ?? $date;

		$data = [];
		if ($penjurianMakalahStart <= $date && $penjurianMakalahEnd >= $date) {
			$data = [
				'ide' => $this->input->post('ide'),
				'metode' => $this->input->post('metode'),
				'manfaat' => $this->input->post('manfaat'),
			];	
		}
		if ($penjurianGrandFinalStart <= $date && $penjurianGrandFinalEnd >= $date) {
			$data['display'] = $this->input->post('display');
			$data['presentasi'] = $this->input->post('presentasi');
		}

		$nilai = $this->M_juri->getNilai($id);

		if ($nilai) {
			$data['updated_at'] = date('Y-m-d H:i:s');
			$this->db->where("id", $nilai['id']);
			$save =  $this->db->update($this->tableNilai, $data);
		}else{
			$data['tim_id'] = $id;
			$data['juri_id'] = get_current_user_id();
			$data['created_at'] = date('Y-m-d H:i:s');
			$save =  $this->db->insert($this->tableNilai, $data);
		}
		if (!$save) {
			$resp['error']['code'] = 400;
			$resp['error']['message'] = 'Maaf, Terjadi kesalahan saat menyimpan data';
			$resp['message'] = 'failed';
			$resp['data'] = '';
			echo json_encode($resp);
		}

		$resp['error']['code'] = 200;
		$resp['error']['message'] = 'Data berhasil disimpan';
		$resp['message'] = 'success';
		$resp['data'] = '';
		echo json_encode($resp);
		exit();
	}
}
