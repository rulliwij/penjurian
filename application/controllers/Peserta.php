<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect(base_url());
		}
		if ($this->session->userdata('role_id') != get_option('website_role_peserta')->option_value) {
			redirect(base_url(role_slug($this->session->userdata('role_id'))));
		}
		$this->prefixTitle = "Peserta";
		$this->load->model('M_peserta');
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		$this->load->library('pagination');
	}

	public function index()
	{
		$data['title'] = 'Dashboard - '.$this->prefixTitle;
		$data['ketua'] = $this->M_peserta->getDataKetua();
		$data['kategori'] = $this->M_peserta->getDataKategori();
		$data['tim'] = $this->M_peserta->getDataTim();
		set_view('dashboard/peserta', $data);
	}

	public function profile()
	{
		$data = [];
		$data['tim'] = $this->M_peserta->getDataTim();
		$data['provinsi'] = $this->M_peserta->getDataProvinsi();
		$data['title'] = $this->prefixTitle.' - Profile Tim';
		set_view('dashboard/peserta/profile', $data);
	}

	public function update_profile()
	{
		$this->form_validation->set_rules(
			'nama', 
			'Nama Tim', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'provinsi_id', 
			'Asal Tim', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'inovasi', 
			'Inovasi Tim', 
			'trim|required',
        	array('required' => 'Maaf, %s Tidak Boleh Kosong')
        );
		$this->form_validation->set_rules(
			'abstract', 
			'Abstract Inovasi Tim', 
			'trim|required',
        	array('required' => 'Maaf, %s Tidak Boleh Kosong')
        );
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->profile();
        }

        $this->db->trans_begin();
        $data = [
        	'nama' => ucwords($this->input->post('nama')),
        	'provinsi_id' => $this->input->post('provinsi_id'),
        	'inovasi' => $this->input->post('inovasi'),
        	'abstract' => $this->input->post('abstract')
        ];
        $save = $this->M_peserta->saveTim($data);
		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('peserta/profile'));
	}

	public function list_anggota()
	{
		$data = [];
		$data['anggota'] = $this->M_peserta->getDataAnggota();
		$data['title'] = $this->prefixTitle.' - List Anggota';
		set_view('dashboard/peserta/list-anggota', $data);
	}

	public function create_anggota()
	{
		$data = [];
		$data['title'] = $this->prefixTitle.' - Tambah Anggota';
		$data['provinsi'] = $this->M_peserta->getDataProvinsi();
		set_view('dashboard/peserta/create', $data);
	}

	public function store_anggota()
	{
		$this->form_validation->set_rules(
			'name', 
			'Nama Anggota', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'place_of_birth', 
			'Tempat Lahir', 
			'trim|required|min_length[3]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'date_of_birth', 
			'Tanggal Lahir', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'gender', 
			'Jenis Kelamin', 
			'required|numeric|in_list[1,2]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'country', 
			'Negara', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'phone', 
			'Nomor HP', 
			'required|min_length[9]|max_length[15]|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, %s Minimal Panjangnya 9',
				'max_length' => 'Maaf, %s Maksimal Panjangnya 15'
			)
		);
		$this->form_validation->set_rules(
			'email', 
			'Email', 
			'required|valid_email|is_unique[anggota.email]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'valid_email' => 'Maaf, Email %s Tidal Valid',
				'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
			)
		);
		$this->form_validation->set_rules(
			'jabatan', 
			'Jabatan', 
			'required|in_list[ketua,anggota,pembimbing]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'peran', 
			'Peranan dalam Tim', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'agama', 
			'Agama', 
			'required|in_list[islam,kristen,katolik,hindu,budha,konghucu]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'alamat', 
			'Alamat', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'nik',
			'NIK', 
			'required|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number'
			)
		);
		$this->form_validation->set_rules(
			'provinsi', 
			'Provinsi', 
			'required|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number'
			)
		);
		$this->form_validation->set_rules(
			'kabupaten', 
			'Kabupaten', 
			'required|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number'
			)
		);

		if (empty($_FILES['foto']['name'])) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Foto tidak boleh kosong");
			return $this->create_anggota();
		}

		$nextID = $this->M_peserta->getNextPrimaryKey();
        $resp = $this->M_peserta->_uploadFileDinamis('foto', "jpg|jpeg|png", 'foto-'.($nextID+1), './uploads/profile/');
        if ($resp['error']) {
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', $resp['data']);
        	return $this->create_anggota();
        }
        $filename = $resp['data']['file_name'] ?? '';
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->create_anggota();
        }

        $this->db->trans_begin();
		$data = array(
			'user_id'        => get_current_user_id(),
			'name'           => ucwords($this->input->post('name')),
			'place_of_birth' => ucwords($this->input->post('place_of_birth')),
			'date_of_birth'  => $this->input->post('date_of_birth'),
			'gender'         => $this->input->post('gender'),
			'country'        => ucwords($this->input->post('country')),
			'phone'          => $this->input->post('phone'),
			'email'          => $this->input->post('email'),
			'jabatan'        => $this->input->post('jabatan'),
			'peran'          => $this->input->post('peran'),
			'agama'          => $this->input->post('agama'),
			'nik'          => $this->input->post('nik'),
			'alamat'         => $this->input->post('alamat'),
			'provinsi'       => $this->input->post('provinsi'),
			'kabupaten'      => $this->input->post('kabupaten'),
			'foto'           => $filename,
			'created_at'     => date('Y-m-d H:i:s')
		);
		$save =  $this->M_peserta->insertDataAnggota($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->create_anggota();
		}
		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('peserta/list_anggota'));
	}

	public function edit_anggota($id)
	{
		$anggota = $this->M_peserta->getDataAnggotaByID($id);
		if ($anggota->num_rows() == 0) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Data Tidak Ditemukan");
			redirect(base_url('peserta/list_anggota'));
		}
		$data = [];
		$data['title'] = $this->prefixTitle.' - Edit Anggota';
		$data['provinsi'] = $this->M_peserta->getDataProvinsi();
		$data['anggota'] = $anggota->row();
		set_view('dashboard/peserta/edit', $data);
	}

	public function update_anggota($id)
	{
		$this->form_validation->set_rules(
			'name', 
			'Nama Anggota', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'place_of_birth', 
			'Tempat Lahir', 
			'trim|required|min_length[3]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'date_of_birth', 
			'Tanggal Lahir', 
			'trim|required|min_length[10]|max_length[10]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, Masukkan Data %s Dengan Benar',
				'max_length' => 'Maaf, Masukkan Data %s Dengan Benar'
			)
		);
		$this->form_validation->set_rules(
			'gender', 
			'Jenis Kelamin', 
			'required|numeric|in_list[1,2]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'country', 
			'Negara', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'phone', 
			'Nomor HP', 
			'required|min_length[9]|max_length[15]|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, %s Minimal Panjangnya 9',
				'max_length' => 'Maaf, %s Maksimal Panjangnya 15'
			)
		);
		// $this->form_validation->set_rules(
		// 	'email', 
		// 	'Email', 
		// 	'required|valid_email|is_unique[anggota.email]',
		// 	array(
		// 		'required' => 'Maaf, %s Tidak Boleh Kosong',
		// 		'valid_email' => 'Maaf, Email %s Tidal Valid',
		// 		'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
		// 	)
		// );
		$this->form_validation->set_rules(
			'jabatan', 
			'Jabatan', 
			'required|in_list[ketua,anggota,pembimbing]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'peran', 
			'Peranan dalam Tim', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'agama', 
			'Agama', 
			'required|in_list[islam,kristen,katolik,hindu,budha,konghucu]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'in_list' => 'Maaf, %s Tidak Ada Didalam Option'
			)
		);
		$this->form_validation->set_rules(
			'alamat', 
			'Alamat', 
			'required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'nik',
			'NIK', 
			'required|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number'
			)
		);
		$this->form_validation->set_rules(
			'provinsi', 
			'Provinsi', 
			'required|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number'
			)
		);
		$this->form_validation->set_rules(
			'kabupaten', 
			'Kabupaten', 
			'required|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'numeric' => 'Maaf, %s Bukan Number'
			)
		);
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->edit_anggota($id);
        }

        $this->db->trans_begin();
		$data = array(
			'user_id'        => get_current_user_id(),
			'name'           => ucwords($this->input->post('name')),
			'place_of_birth' => ucwords($this->input->post('place_of_birth')),
			'date_of_birth'  => $this->input->post('date_of_birth'),
			'gender'         => $this->input->post('gender'),
			'country'        => ucwords($this->input->post('country')),
			'phone'          => $this->input->post('phone'),
			// 'email'          => $this->input->post('email'),
			'jabatan'        => $this->input->post('jabatan'),
			'peran'          => $this->input->post('peran'),
			'agama'          => $this->input->post('agama'),
			'nik'            => $this->input->post('nik'),
			'alamat'         => $this->input->post('alamat'),
			'provinsi'       => $this->input->post('provinsi'),
			'foto'       	 => '',
			'kabupaten'      => $this->input->post('kabupaten'),
			'updated_at'     => date('Y-m-d H:i:s')
		);
		if (!empty($_FILES['foto']['name'])) {
			$resp = $this->M_peserta->_uploadFileDinamis('foto', "jpg|jpeg|png", 'foto-'.$id);
			if ($resp['error']) {
				$this->session->set_flashdata('error', TRUE);
				$this->session->set_flashdata('status', "alert alert-danger");
				$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
				$this->session->set_flashdata('message', $resp['data']);
				return $this->edit_anggota($id);
			}
			$data['foto'] = $resp['data']['file_name'] ?? '';
		}

		$this->db->where('id', $id);
		$update =  $this->M_peserta->updateDataAnggota($data);
		if (!$update) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->edit_anggota($id);
		}
		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('peserta/edit_anggota/'.$id));
	}

	public function delete_anggota($id)
	{
		$delete =  $this->M_peserta->deleteDataAnggota($id);
		if (!$delete) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menghapus Data");
			return $this->list_anggota();
		}

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Dihapus");
		redirect(base_url('peserta/list_anggota'));
	}

	public function upload_materi_lomba()
	{
		$data = [];
		$data['tim'] = $this->M_peserta->getDataTim();
		$data['title'] = $this->prefixTitle.' - Upload Materi Lomba';
		$data['event'] = $this->M_peserta->getDataEventByID();
		set_view('dashboard/peserta/upload-materi-lomba', $data);
	}

	public function update_upload_materi_lomba()
	{
		$count = get_user_meta($this->session->userdata('id'),'team_upload_makalah_count')->meta_value ?? 1;

		// CHECK ALL
		if (empty($_FILES['makalah'.$count]['name']) || empty($_FILES['lampiran'.$count]['name'])) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Tidak ada data untuk diupload");
			return $this->upload_materi_lomba();
		}

		$this->db->trans_begin();

		// MAKALAH
        $resp = $this->M_peserta->_uploadFile('makalah'.$count, "pdf");
        if ($resp['error']) {
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', $resp['data']);
        	return $this->upload_materi_lomba();
        }
        $filename = $resp['data']['file_name'];
		$has = get_user_meta(get_current_user_id(), 'team_makalah_'.$count);
		if ($has) {
			$save = update_user_meta(get_current_user_id(),'team_makalah_'.$count,$filename);
		}else{
			$save = add_user_meta(get_current_user_id(),'team_makalah_'.$count,$filename);
		}
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->upload_materi_lomba();
		}

		// LAMPIRAN
        $resp = $this->M_peserta->_uploadFile('lampiran'.$count, "pdf|jpg|jpeg|png|docx|doc|xlsx|xls|pptx", FALSE);
        if ($resp['error']) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', strip_tags($resp['data']));
        	return $this->upload_materi_lomba();
        }
        $filename = $resp['data']['file_name'];
		$has = get_user_meta(get_current_user_id(), 'team_lampiran_'.$count);
		if ($has) {
			$save = update_user_meta(get_current_user_id(),'team_lampiran_'.$count,$filename);
		}else{
			$save = add_user_meta(get_current_user_id(),'team_lampiran_'.$count,$filename);
		}
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->upload_materi_lomba();
		}

		// UPLOAD COUNT
		$has = get_user_meta(get_current_user_id(), 'team_upload_makalah_count');
		if ($has) {
			$save = update_user_meta(get_current_user_id(),'team_upload_makalah_count',($count+1));
		}else{
			$save = add_user_meta(get_current_user_id(),'team_upload_makalah_count',($count+1));
		}
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			return $this->upload_materi_lomba();
		}

		// success
		$this->db->trans_commit();
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('peserta/upload_materi_lomba'));
	}

	public function list_pengumuman()
	{
		// konfigurasi pagination
		$config['base_url']    = site_url('peserta/list_pengumuman');
		$config['total_rows']  = $this->M_peserta->getDataAll();
		$config['per_page']    = get_option('limit')->option_value ?? 10;
		$config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // style pagination bootstrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$this->pagination->initialize($config);

		$data = [];
		$data['page']       = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['pengumuman'] = $this->M_peserta->getData($config["per_page"], $data['page']);
		$data['title']      = $this->prefixTitle.' - List Pengumuman';
		$data['pagination'] = $this->pagination->create_links();
		set_view('dashboard/peserta/list-pengumuman', $data);
	}

	public function tutorial_lomba()
	{
		$data = [];
		$data['event'] = $this->M_peserta->getDataEventByID();
		// dd($data['event']);
		$data['title'] = $this->prefixTitle.' - Panduan Lomba';
		set_view('dashboard/peserta/tutorial-lomba', $data);
	}

	public function survey()
	{
		$data = [];
		$data['event'] = $this->M_peserta->getDataEventByID();
		// dd($data['event']);
		$data['title'] = $this->prefixTitle.' - Survey';
		set_view('dashboard/peserta/survey', $data);
	}

	function get_kabupaten()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		if(!$id_provinsi){
			echo false;
			exit();
		}
		$html = '';
		$getKabupaten = $this->M_peserta->getDataKabupaten($id_provinsi);
		foreach ($getKabupaten as $key => $value) {
			$html .= '<option value="'.$value['id'].'">'.ucwords(strtolower($value['name'])).'</option>';
		}
		echo $html;
		exit();
	}

	function get_instansi_ajax()
	{
		$kategori_id = $this->input->post('kategori_id');
		if(!$kategori_id){
			echo false;
			exit();
		}
		$html = '';
		$data = $this->M_peserta->getDataInstansiByID($kategori_id);
		foreach ($data as $key => $value) {
			$html .= '<option value="'.$value['id'].'">'.$value['nama'].'</option>';
		}
		echo $html;
		exit();
	}

	public function save_dashboard()
	{
		$this->form_validation->set_rules(
			'kategori', 
			'Kategori', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'kategori_sub', 
			'Instansi', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'nama_tim', 
			'Nama Tim', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		$this->form_validation->set_rules(
			'nama_ketua_tim', 
			'Nama Ketua Tim', 
			'trim|required',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong'
			)
		);
		if (!$this->input->post('id')) {
			$this->form_validation->set_rules(
				'email', 
				'Email', 
				'required|valid_email|is_unique[anggota.email]',
				array(
					'required' => 'Maaf, %s Tidak Boleh Kosong',
					'valid_email' => 'Maaf, Email %s Tidal Valid',
					'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
				)
			);
		}
		$this->form_validation->set_rules(
			'phone', 
			'Nomor HP', 
			'required|min_length[9]|max_length[15]|numeric',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'min_length' => 'Maaf, %s Minimal Panjangnya 9',
				'max_length' => 'Maaf, %s Maksimal Panjangnya 15'
			)
		);
		
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->index();
        }

        $this->db->trans_begin();

        // KETUA
        if ($this->input->post('id')) {
        	$data = array(
				'name'  => ucwords($this->input->post('nama_ketua_tim')),
				'phone' => $this->input->post('phone')
        	);
        	$this->db->where('id', $this->input->post('id'));
        	$save =  $this->M_peserta->updateDataAnggota($data);
        }else{
        	$data = array(
				'user_id' => get_current_user_id(),
				'name'    => ucwords($this->input->post('nama_ketua_tim')),
				'jabatan' => 'ketua',
				'email'   => $this->input->post('email'),
				'phone'   => $this->input->post('phone')
        	);
        	$save =  $this->M_peserta->insertDataAnggota($data);
        }
        if (!$save) {
        	$this->db->trans_rollback();
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', "Maaf, Terjadi kesalahan saat menyimpan data");
        	return $this->index();
        }

        // TIM
        $data = [
        	'nama' => ucwords($this->input->post('nama_tim')),
        	'kategori_id' => $this->input->post('kategori'),
        	'kategori_sub_id' => $this->input->post('kategori_sub')
        ];
        $save = $this->M_peserta->saveTim($data);

		$this->db->trans_commit();

		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data berhasil disimpan");
		redirect(base_url('peserta/index'));
	}
}
