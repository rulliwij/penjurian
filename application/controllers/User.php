<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect(base_url());
		}
		if ($this->session->userdata('role_id') != get_option('website_role_panitia')->option_value) {
			redirect(base_url(role_slug($this->session->userdata('role_id'))));
		}
		$this->prefixTitle = "User";
		$this->load->model('M_user');
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		$this->load->library('pagination');
	}

	public function index()
	{
		$data['user'] = $this->M_user->getDataUserAll();
		$data['title'] = 'Dashboard - '.$this->prefixTitle;
		set_view('dashboard/user/list-user', $data);
	}

	public function choice_role($id)
	{
		$data = [];
		$data = $this->M_user->getDataUserByID($id);
		if (empty($data)) {
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Data peserta tidak ditemukan");
			redirect(base_url('user'));
		} else {
			$data['user'] = $data;
			$data['title'] = $this->prefixTitle.' - Edit User';
			$data['role'] = $this->M_user->getDataRole();
			set_view('dashboard/user/edit-role-user', $data);
		}
	}

	public function update_role($id)
	{
		$this->form_validation->set_rules(
			'user_id', 
			'ID User', 
			'trim',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'role_id', 
			'Role', 
			'trim',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			redirect(base_url('user/choice_role'));
        }
        $this->db->trans_begin();
        $arr_role_id = $this->input->post('role');
        $role_id = 0;
        if (!empty($arr_role_id)) {
        	foreach ($arr_role_id as $key => $value) {
        		$role_id = $value;
        	}
        }
        $save = FALSE;
        if ($role_id != 0) {
	        $data['role_id'] = $role_id;
	        $this->db->where("id", $id);
	        $save =  $this->db->update('user', $data);
        }
        if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data");
			redirect(base_url('user/choice_role/'.$this->input->post('user_id')));
		}
		$this->db->trans_commit();
		// success
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url('user'));
	}
}