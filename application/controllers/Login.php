<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		if($this->session->userdata('is_login')){
			$role_id = $this->session->userdata('role_id');
			if ($role_id == "1") {
				redirect(base_url("panitia"));
			}else if ($role_id == "2") {
				redirect(base_url("panitia"));
			}else if ($role_id == "3") {
				redirect(base_url("juri"));
			}else if ($role_id == "4") {
				redirect(base_url("peserta"));
			}
		}
		// $this->load->library('encrypt');
		$this->load->model('M_login');
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
	}

	function index()
	{
		// redirect(base_url("login/login"));
		// if (!$this->input->cookie('jss_session', false)) {
		// 	redirect('https://jss.jogjakota.go.id/homepage?login=true&redirectlink='.urlencode(base_url()));
		// }
		$data['title'] = "Login Dashboard";
		$this->load->view('login',$data);
	}

	function register()
	{
		$this->form_validation->set_rules(
			'username', 
			'Username', 
			'trim|required|min_length[3]|is_unique[user.username]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'is_unique' => 'Maaf, %s sudah terdaftar pada event'
			)
		);
		$this->form_validation->set_rules(
			'email', 
			'Email', 
			'required|valid_email|is_unique[user.email]',
			array(
				'required' => 'Maaf, %s Tidak Boleh Kosong',
				'valid_email' => 'Maaf, Email %s Tidal Valid',
				'is_unique' => 'Maaf, %s Sudah Terdaftar di Tim Lain'
			)
		);
		$this->form_validation->set_rules(
			'password', 
			'Password', 
			'trim|required|min_length[3]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'conf_password', 
			'Konfirmasi Password', 
			'trim|required|min_length[3]|matches[password]',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);

		// VALIDATION RUN
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', validation_errors());
			redirect(base_url('#signup'));
        }

        $event = $this->M_login->get_current_event_year();
        if (!$event) {
        	$this->session->set_flashdata('error', TRUE);
        	$this->session->set_flashdata('status', "alert alert-danger");
        	$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
        	$this->session->set_flashdata('message', 'Maaf, Tidak ada event pada tahun ini');
        	redirect(base_url('#signup'));
        }

        // INSERT DATA
		$data = array(
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'role_id' => get_option('website_role_peserta',TRUE),
			'event_id' => $event['id'],
			'created_at' => date('Y-m-d H:i:s')
		);
		$save = $this->M_login->insertDataUser($data);
		if (!$save) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Terjadi Kesalahan Saat Menyimpan Data User");
			redirect(base_url('#signup'));
		}

		// SUCCESS
		$this->session->set_flashdata('error', FALSE);
		$this->session->set_flashdata('status', "alert alert-success");
		$this->session->set_flashdata('icon', "fa fa-check-circle");
		$this->session->set_flashdata('message', "Berhasil, Data Berhasil Disimpan");
		redirect(base_url());
	}

	function aksi_login(){
		$this->form_validation->set_rules(
			'username', 
			'Username', 
			'trim|required',
			array('required' => 'Maaf, %s Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules(
			'password', 
			'Password', 
			'trim|required',
        	array('required' => 'Maaf, %s Tidak Boleh Kosong')
        );
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		if ($this->form_validation->run() == FALSE){
			return $this->index();
        }

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = $this->M_login->cek_login($username, $password);
		$check = $data->num_rows();
		if($check < 1){
			$this->session->set_flashdata('error', TRUE);
			$this->session->set_flashdata('status', "alert alert-danger");
			$this->session->set_flashdata('icon', "fa fa-exclamation-circle");
			$this->session->set_flashdata('message', "Maaf, Username atau Password Salah");
			return $this->index();
		}
		$data = $data->row();

		if ($data->role_id == get_option('website_role_panitia',TRUE)) {
			$nama = $data->username;
		}else if ($data->role_id == get_option('website_role_juri',TRUE)) {
			$nama = $this->M_login->getDataJuriByUserID($data->id)['name'];
		}else if ($data->role_id == get_option('website_role_peserta',TRUE)) {
			$nama = $this->M_login->getDataPesertaByUserID($data->id)['nama'];
		}

		$data_session = array(
			'id'       => $data->id,
			'nama'     => $nama,
			'email'    => $data->email,
			'role_id'  => $data->role_id,
			'is_login' => TRUE
		);
		if ($data->role_id == get_option('website_role_panitia',TRUE)) {
			$data_session['event_id'] = '';
		}else{
			$data_session['event_id'] = $data->event_id;
		}
		$this->session->set_userdata($data_session);

		if ($data->role_id == 1) {
			redirect(base_url("panitia"));
		}else if ($data->role_id == get_option('website_role_panitia',TRUE)) {
			redirect(base_url("panitia"));
		}else if ($data->role_id == get_option('website_role_juri',TRUE)) {
			redirect(base_url("juri"));
		}else if ($data->role_id == get_option('website_role_peserta',TRUE)) {
			redirect(base_url("peserta"));
		}
	}

	function login() {
		// $object = new stdClass();
   		// $object->id_upik = 'JSS-1';
   		// $object->username = 'tio';
   		// $object->email = 'tiotamara@gmail.com';
		// $data = $this->M_login->registerUserJSS($object);
		// $data = $this->M_login->getUserByJssID($object->id_upik);
		// dd($data->row());
		$nama = null;

		// check jss cookie
		if (!$this->input->cookie('jss_session', false)) {
			redirect('https://jss.jogjakota.go.id/homepage?login=true&redirectlink='.urlencode(base_url('login/login')));
		}
		
		// decrypt cookie
		$decrypt = $this->encrypt->decode($this->input->cookie('jss_session', false), "diskominfo");
		$jsonuser = json_decode($decrypt);
		if(!$jsonuser) {
			die("Error, Cookies JSS tidak ditemukan");
		}
		// var_dump($jsonuser);
		// echo "<pre>";
		// print_r($jsonuser);
		// echo "</pre>";
		// die;

		if (empty($jsonuser->id_upik)) {
			die("Anda tidak berhak masuk ke aplikasi ini!");
		}
        
        // check available user with jss ID
		$data = $this->M_login->getUserByJssID($jsonuser->id_upik ?? null);
		$check = $data ? $data->num_rows() : false;
		if(!$check) {
			// create user
			$data = $this->M_login->registerUserJSS($jsonuser);
			$data = $this->M_login->getUserByJssID($jsonuser->id_upik ?? null);
		}
		$data = $data->row();

		// get detail user
		if ($data->role_id == get_option('website_role_panitia',TRUE)) {
			$nama = $data->username ?? '';
		} else if ($data->role_id == get_option('website_role_juri',TRUE)) {
			$nama = $this->M_login->getDataJuriByUserID($data->id)['name'] ?? '';
		} else if ($data->role_id == get_option('website_role_peserta',TRUE)) {
			$nama = $this->M_login->getDataPesertaByUserID($data->id)['nama'] ?? '';
		}

		// create session
		$data_session = array(
			'id'       => $data->id,
			'nama'     => $nama,
			'email'    => $data->email,
			'role_id'  => $data->role_id,
			'is_login' => TRUE
		);
		if ($data->role_id == get_option('website_role_panitia',TRUE)) {
			$data_session['event_id'] = '';
		}else{
			$data_session['event_id'] = $data->event_id;
		}
		$this->session->set_userdata($data_session);

		// redirect with role type
		// berhasil
		if ($data->role_id == 1) {
			redirect(base_url("panitia"));
		}else if ($data->role_id == get_option('website_role_panitia',TRUE)) {
			redirect(base_url("panitia"));
		}else if ($data->role_id == get_option('website_role_juri',TRUE)) {
			redirect(base_url("juri"));
		}else if ($data->role_id == get_option('website_role_peserta',TRUE)) {
			redirect(base_url("peserta"));
		}
    }
}
