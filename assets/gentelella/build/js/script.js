const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

function confirmation(ev) {
    ev.preventDefault();
    var urlToRedirect = ev.currentTarget.getAttribute('href');
    Swal.fire({
        title: "Apakah Anda Yakin ?",
        text: "Setelah Dihapus, Kamu Tidak akan bisa mengembalikan data ini kembali",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!',
        cancelButtonText: 'Batal'
    }).then((willDelete) => {
        if (willDelete.value) {
            Toast.fire({
              icon: 'success',
              title: 'Data akan segera dihapus'
            }).then((result) => {
                window.location.href = urlToRedirect;
            });
        }
    });
}